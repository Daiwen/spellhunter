/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

public class Background extends Part {
	int shouldContinue;
	0 => int isSilence;

	Gain volume => ADSR env;
	Piano piano;
	RhythmGenerator rgen;

	Gain chordVolume => ADSR chordEnv => volume;
	TriOsc osc1 => chordVolume;
	TriOsc osc2 => chordVolume;
	TriOsc osc3 => chordVolume;

	Conductor conductor;

	fun Background(Gain mv, Conductor c) {
		env => mv;
		0.7 => volume.gain;
		c @=> conductor;
		0.02 => chordVolume.gain;
		0 :: ms => env.duration;
		1 => env.keyOn;
		rgen.beat => env.duration;
		chordEnv.set(rgen.bar()/2, rgen.bar()/3, 0, 0::ms);
	}

	fun void playNote(Instrument i, int midi) {
		rgen.beat / 2 => dur beat;

		SndBuf buf => volume;
		i.midiToName(midi) => buf.read;
		beat => now;
	}

	fun void melody () {
		while (shouldContinue) {
			for (0 => int i; i < 4; i++) {
				playNote(piano, conductor.current_chord()[0]-24);
				playNote(piano, conductor.current_chord()[1]-24);
				playNote(piano, conductor.current_chord()[0]-24);
				playNote(piano, conductor.current_chord()[2]-24);
			}
		}
	}

	fun void chords () {
		while (shouldContinue) {
			Std.mtof(conductor.current_chord()[0]-12) => osc1.freq;
			Std.mtof(conductor.current_chord()[1]-12) => osc2.freq;
			Std.mtof(conductor.current_chord()[2]-12) => osc3.freq;
			1 => chordEnv.keyOn;
			rgen.bar() => now;
		}
	}

	Shred @ shreds[2];
	fun void start() {
		1 => shouldContinue;
		spork ~ melody() @=> shreds[0];
		spork ~ chords() @=> shreds[1];
	}

	fun void stop() {
		0 => shouldContinue;
	}

	fun void exit() {
		0 => shouldContinue;
		for (Shred @ s : shreds) {
			if (null == s) {
				continue;
			}
			s.exit();
		}
	}

	fun silence() {
		if (isSilence)
			return;
		1 => isSilence;
		1 => env.keyOff;
	}

	fun unsilence() {
		if (!isSilence)
			return;
		0 => isSilence;
		1 => env.keyOn;
	}
}
