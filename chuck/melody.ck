/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

global int seed;

public class Melody extends Part {
	int shouldContinue;
	0 => int isSilence;

	NRev rev => Gain volume => Envelope env;
	Glockenspiel glockenspiel;

	Conductor conductor;

	MelodyGenerator phrases[];

	RhythmGenerator rgen;

	fun Melody(Gain mv, Conductor c) {
		env => mv;
		Math.srandom(seed);

		0.05 => rev.mix;
		0.3 => volume.gain;
		c @=> conductor;

		0 :: ms => env.duration;
		1 => env.keyOn;
		rgen.beat => env.duration;

		MelodyGenerator nphrases[c.progression.cap()];
		for(0 => int i; i < c.progression.cap(); i++) {
			MelodyGraph g;
			g.clique(c.progression[i]);
			MelodyGenerator m @=> nphrases[i];
			m.init(g);
		}

		nphrases @=> phrases;
	}

	fun void playNote(Instrument i, Note n) {
		SndBuf buf => rev;
		i.midiToName(n.midi) => buf.read;
		n.length => now;
	}

	fun melody() {
		while (shouldContinue) {
			phrases[conductor.current].notes @=> Note notes[];
			for (0 => int j; j < notes.cap(); j++) {
				playNote(glockenspiel, notes[j]);
			}
		}
	}

	Shred @ melodyShred;
	fun start() {
		1 => shouldContinue;
		spork ~ melody() @=> melodyShred;
	}

	fun stop() {
		0 => shouldContinue;
	}
	
	fun exit() {
		0 => shouldContinue;
		if (null == melodyShred) {
			return;
		}
		melodyShred.exit();
	}

	fun silence() {
		if (isSilence)
			return;
		1 => isSilence;
		1 => env.keyOff;
	}

	fun unsilence() {
		if (!isSilence)
			return;
		0 => isSilence;
		1 => env.keyOn;
	}
}
