/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

public class Piano extends Instrument {
	fun Piano() {
		add(me.dir() + "piano/Piano.mf.C3.wav", 60);
		add(me.dir() + "piano/Piano.mf.Db3.wav", 61);
		add(me.dir() + "piano/Piano.mf.D3.wav", 62);
		add(me.dir() + "piano/Piano.mf.Eb3.wav", 63);
		add(me.dir() + "piano/Piano.mf.E3.wav", 64);
		add(me.dir() + "piano/Piano.mf.F3.wav", 65);
		add(me.dir() + "piano/Piano.mf.Gb3.wav", 66);
		add(me.dir() + "piano/Piano.mf.G3.wav", 67);
		add(me.dir() + "piano/Piano.mf.Ab3.wav", 68);
		add(me.dir() + "piano/Piano.mf.A3.wav", 69);
		add(me.dir() + "piano/Piano.mf.Bb3.wav", 70);
		add(me.dir() + "piano/Piano.mf.B3.wav", 71);
		add(me.dir() + "piano/Piano.mf.C4.wav", 72);
		add(me.dir() + "piano/Piano.mf.Db4.wav", 73);
		add(me.dir() + "piano/Piano.mf.D4.wav", 74);
		add(me.dir() + "piano/Piano.mf.Eb4.wav", 75);
		add(me.dir() + "piano/Piano.mf.E4.wav", 76);
		add(me.dir() + "piano/Piano.mf.F4.wav", 77);
		add(me.dir() + "piano/Piano.mf.Gb4.wav", 78);
		add(me.dir() + "piano/Piano.mf.G4.wav", 79);
		add(me.dir() + "piano/Piano.mf.Ab4.wav", 80);
		add(me.dir() + "piano/Piano.mf.A4.wav", 81);
		add(me.dir() + "piano/Piano.mf.Bb4.wav", 82);
		add(me.dir() + "piano/Piano.mf.B4.wav", 83);
	}
}
