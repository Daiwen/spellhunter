/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

public class RhythmGenerator {
	second => static dur beat;
	4 => static int beatsPerBar;

	fun dur bar() {
		return beat * beatsPerBar;
	}

	dur beats[];

	fun void init() {
		Math.random2(0, beatsPerBar) => int slowBeats;
		2 * (beatsPerBar - slowBeats) => int fastBeats;
		dur newBeats[fastBeats + slowBeats];	
		for (0 => int i; i < newBeats.cap(); i++) {
			if (slowBeats == 0) {
				0.5::beat => newBeats[i];
				fastBeats--;
			} else if (fastBeats == 0) {
				beat => newBeats[i];
				slowBeats--;
			} else if (Math.randomf() > 0.5) {
				beat => newBeats[i];
				slowBeats--;
			} else {
				0.5::beat => newBeats[i];
				fastBeats--;
			}
		}
		newBeats @=> beats;
	}
}
