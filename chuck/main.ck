/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

0 => global int seed;

Gain masterVolume;
global Gain musicVolume;
global Gain foleyVolume;
global Gain buttonVolume;
0.1 => buttonVolume.gain;
global Dyno lim;
0::ms => lim.attackTime;
musicVolume => lim => masterVolume => dac;
foleyVolume => lim;
buttonVolume => lim;

global Conductor conductor;
global Event intro;
fun void introControl() {
	intro => now;
	while(true) {
		Background bg(musicVolume, conductor) @=> conductor.bg;
		Rhythm rhythm(musicVolume) @=> conductor.rhythm;
		Melody melody(musicVolume, conductor) @=> conductor.melody;
		conductor.start();
		intro => now;
		conductor.exit();
	}
}
spork ~ introControl();

global Event menu;
fun void menuControl() {
	while(true) {
		menu => now;
		conductor.menu();
	}
}
spork ~ menuControl();

global Event book;
fun void bookControl() {
	while(true) {
		book => now;
		conductor.book();
	}
}
spork ~ bookControl();

global Event game;
fun void gameControl() {
	while(true) {
		game => now;
		conductor.unsilence();
	}
}
spork ~ gameControl();

0.5 => global float volume;
0.5 => global float mVolume;
0.5 => global float fVolume;
global Event volumeChange;

fun void volumeControl() {
	while(true) {
		volume => masterVolume.gain;
		mVolume => musicVolume.gain;
		fVolume => foleyVolume.gain;
		volumeChange => now;
	}
}

Machine.add("foley.ck");
spork ~ volumeControl();

while(true) {
	1::second => now;
}
