/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

public class MelodyGenerator {
	Note notes[];

	fun void init(MelodyGraph g) {
		RhythmGenerator rgen;
		rgen.init();

		Note ns[rgen.beats.cap()];
		Note f @=> ns[0];
		Math.random2(0, g.tones.cap()-1) => int last_id;
		g.getMidi(last_id) => f.midi;
		rgen.beats[0] => f.length;
		for (1 => int i; i < rgen.beats.cap(); i++) {
			Note n @=> ns[i];
			g.addWeight(last_id);
			g.setLastUsed(last_id, i-1);
			g.getNeighbour(last_id, i) => last_id;
			g.getMidi(last_id) => n.midi;
			rgen.beats[i] => n.length;
		}

		ns @=> notes;
	}
}
