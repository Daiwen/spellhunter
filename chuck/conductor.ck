/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

global Event transition;

public class Conductor extends Part {
	1 => int shouldContinue;
	[84, 88, 91, 96] @=> int tonic[];
	[93, 96, 100, 105] @=> int mediant[];
	[86, 89, 93, 98] @=> int pre_dominant[];
	[91, 95, 98, 103] @=> int dominant[];

	[tonic, mediant, pre_dominant, dominant] @=> int progression[][];
	int current;

	Part bg;
	Part rhythm;
	Part melody;
	RhythmGenerator rgen;

	Shred @ shreds[2];

	fun int[] current_chord() {
		return progression[current];
	}

	fun playProgression() {
		while(shouldContinue) {
			for (0 => current; current < progression.cap(); current++) {
				rgen.bar() => now;
			}
		}
	}

	fun void play() {
		rhythm.start();
		rgen.bar() => now;
		bg.start();
		rgen.bar() => now;
		melody.start();
		spork ~ playProgression() @=> shreds[1];
		transition => now;
		rhythm.stop();
		rgen.bar() => now;
		bg.stop();
		while (shouldContinue) {
			second => now;
		}
	}

	fun void start() {
		0 => current;
		1 => shouldContinue;
		spork ~ play() @=> shreds[0];
	}

	fun void exit() {
		0 => shouldContinue;
		for (Shred @ s : shreds) {
			if (null == s) {
				continue;
			}
			s.exit();
		}
		rhythm.exit();
		bg.exit();
		melody.exit();
	}
	
	fun void stop() {
		exit();
	}

	fun void silence() {
		rhythm.silence();
		bg.silence();
		melody.silence();
	}

	fun void unsilence() {
		rhythm.unsilence();
		bg.unsilence();
		melody.unsilence();
	}

	fun menu() {
		melody.silence();
	}

	fun book() {
		bg.silence();
		melody.silence();
		rhythm.silence();
	}
}
