/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


global Gain foleyVolume;
global Gain buttonVolume;

fun void playFoley(string name, Gain g) {
	SndBuf buf => g;
	name => buf.read;
	10 :: second => now;
}

fun void listenToFoley(Event e, string name, Gain g) {
	while(true) {
		e => now;
		spork ~ playFoley(name, g);
	}
}

global Event f_castSpell;
spork ~ listenToFoley(f_castSpell, "sfx/castSpell.wav", foleyVolume);

global Event f_ice;
spork ~ listenToFoley(f_ice, "sfx/ice.wav", foleyVolume);

global Event f_push;
spork ~ listenToFoley(f_push, "sfx/push.wav", foleyVolume);

global Event f_teleport;
spork ~ listenToFoley(f_teleport, "sfx/teleport.wav", foleyVolume);

global Event f_dispell;
spork ~ listenToFoley(f_dispell, "sfx/dispell.wav", foleyVolume);

global Event f_throwArrow;
spork ~ listenToFoley(f_throwArrow, "sfx/throwArrow.wav", foleyVolume);

global Event f_brokenArrow;
spork ~ listenToFoley(f_brokenArrow, "sfx/brokenArrow.wav", foleyVolume);

global Event f_plouf;
spork ~ listenToFoley(f_plouf, "sfx/plouf.wav", foleyVolume);

global Event f_fall;
spork ~ listenToFoley(f_fall, "sfx/fall.wav", foleyVolume);

global Event f_button;
spork ~ listenToFoley(f_button, "sfx/button.wav", foleyVolume);

global Event f_fire;
spork ~ listenToFoley(f_fire, "sfx/fire.wav", foleyVolume);

global Event f_pickup;
spork ~ listenToFoley(f_pickup, "sfx/pickup.wav", foleyVolume);

global Event f_death;
spork ~ listenToFoley(f_death, "sfx/death.wav", foleyVolume);

global Event f_walk;
spork ~ listenToFoley(f_walk, "sfx/walk.wav", foleyVolume);


Glockenspiel glockenspiel;
global Conductor conductor;
private class Button {
	fun string get() {
		return "";
	}
}

private class Button1 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][0]);
	}
}

private class Button2 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][1]);
	}
}

private class Button3 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][2]);
	}
}

private class Button4 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][3]);
	}
}

private class Button5 extends Button {
	fun string get() {
		return glockenspiel.midiToName(conductor.progression[conductor.current][1] + 12);
	}
}

fun void listenToButton(Event e, Button b, Gain g) {
	while(true) {
		e => now;
		spork ~ playFoley(b.get(), g);
	}
}


global Event button1;
spork ~ listenToButton(button1, new Button1(), buttonVolume);
global Event button2;
spork ~ listenToButton(button2, new Button2(), buttonVolume);
global Event button3;
spork ~ listenToButton(button3, new Button3(), buttonVolume);
global Event button4;
spork ~ listenToButton(button4, new Button4(), buttonVolume);
global Event button5;
spork ~ listenToButton(button5, new Button5(), buttonVolume);
global Event xbutton1;
spork ~ listenToFoley(xbutton1, "shakers/11_Small_Loud.wav", buttonVolume);
global Event xbutton2;
spork ~ listenToFoley(xbutton2, "percussions/27_ELCWoo39.wav", buttonVolume);

while(true) {
	1::second => now;
}
