/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

private class Tone {
	int midi;
	int weight;
	int lastUsed;
}

public class MelodyGraph {
	Tone tones[];
	int neighbours[][];	

	fun int getMagicWeight(Tone t, int currentTime) {
		return Math.max(currentTime - t.lastUsed - t.weight, 1) $ int;
	}

	fun int getNeighbour(int id, int currentTime) {
		0 => int size;
		for (0 => int i; i < neighbours[id].cap(); i++) {
			 getMagicWeight(tones[neighbours[id][i]], currentTime) + size => size;;
		}

		int weightedTable[size];
		0 => int cnt;
		for (0 => int i; i < neighbours[id].cap(); i++) {
			for(0 => int j; j < getMagicWeight(tones[neighbours[id][i]], currentTime); j++) {
				neighbours[id][i] => weightedTable[cnt];
				cnt++;
			}
		}

		return weightedTable[Math.random2(0, size-1)];
	}

	fun int getMidi(int id) {
		return tones[id].midi;
	}

	fun void addWeight(int id) {
		tones[id].weight++;
	}

	fun void setLastUsed(int id, int timestamp) {
		timestamp => tones[id].lastUsed;
	}

	fun void clique(int ts[]) {
		Tone nts[ts.cap()];
		int all[ts.cap()];
		for (0 => int i; i < ts.cap(); i++) {
			Tone t @=> nts[i];
			ts[i] => t.midi;
			0 => t.weight;
			0 => t.lastUsed;
			i => all[i];
		}

		nts @=> tones;

		int ns[ts.cap()][ts.cap()];
		for (0 => int i; i < ts.cap(); i++) {
			all @=> ns[i];
		}
		ns @=> neighbours;
	}
}
