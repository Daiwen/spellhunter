/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

public class Rhythm extends Part {
	int shouldContinue;
	0 => int isSilence;

	Gain volume => Envelope env;
	RhythmGenerator rgen;

	fun Rhythm(Gain mv) {
		env => mv;
		0.05 => volume.gain;
		0 :: ms => env.duration;
		1 => env.keyOn;
		rgen.beat => env.duration;
	}

	fun void base() {
		SndBuf percussion => LPF f1 => volume;
		920 => f1.freq;
		string names[4];
		me.dir() + "percussions/27_ELCWoo39.wav" => names[0];
		me.dir() + "percussions/10_BigCon74.wav" => names[1];
		me.dir() + "percussions/27_ELCWoo39.wav" => names[2];
		me.dir() + "percussions/14_BirdWo09.wav" => names[3];

		while (shouldContinue) {
			for (0 => int i; i < names.cap(); i ++) {
				names[i] => percussion.read;
				rgen.beat / 2 => now;
			}
		}
	}
	fun void tresillo() {
		SndBuf buf => ADSR env => Gain vol => volume;
		10 => vol.gain;
		(0 :: second, second / 4, 0, 0::ms) => env.set;
		me.dir() + "shakers/7_Small_Soft.wav" => string name;
		[ 1.5 * rgen.beat, rgen.beat /2, rgen.beat, rgen.beat] @=> dur rhythm[];
		while (shouldContinue) {
			for (0 => int i; i < rhythm.cap(); i++) {
				name => buf.read;
				1 => env.keyOn;
				rhythm[i]/2 => now;
			}
		}
	}

	Shred @ shreds[2];
	fun start() {
		1 => shouldContinue;
		spork ~ base() @=> shreds[0];
		spork ~ tresillo() @=> shreds[1];
	}

	fun stop() {
		0 => shouldContinue;
	}

	fun exit() {
		0 => shouldContinue;
		for (Shred @ s : shreds) {
			if (null == s) {
				continue;
			}
			s.exit();
		}
	}

	fun silence() {
		if (isSilence)
			return;
		1 => env.keyOff;
	}

	fun unsilence() {
		if (!isSilence)
			return;
		1 => env.keyOn;
	}
}
