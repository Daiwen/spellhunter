# Puzzle

Two civilizations with non-overlapping domains of magic
Threats to these civilizations
Resolving the threats comes from combining their domain of magic

Eiwen wakes up
She leaves in an antic roman inspired civilization
She goes and learn the basics of magic
She learns that a volcano is threatening the city and the school of magic has been developed to prevent that from happening
The winters are getting colder and their magic does not help
She goes north to learn how people used to the cold are fairing
She learns their magic, teach them hers
She develops new magic
Help improve the lives of people

The player reads Eiwen's story as they discover new spells. Eiwen left pages of
her notebook behind her as a way for someone to learn the spells she learned.

# RPG

The game starts as you are cast out of the society you were a part of and get
accepted in a society that flourishes despite having to face a very hostile environment.
This new society builds itself around respect, empathy, cooperation and inclusion.
Lessons learned from an often violent past that almost led to extinction.
You experience that first hand as an outsider.
One of the hostile aspect of the environment is an ever more aggressive species
living close to the cities.
Use unreliabe narrators in the society to convey that the cities are in fact
implented close to the species habitat because of the presence resources.
The game asks the player to solve this conflict.

The story serves as an illustration of the scientific method, both in terms of
discovering new part of the language, and learning more about the "hostile"
species.
Different NPC have competing hypotheses that are only partially correct and the
player must put it to the test to refine and unify them.
