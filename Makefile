# Spellhunter, a create your own spell puzzle game.
# Copyright (C) 2021-2024 Quentin Lambert
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

LIBS=$(subst libs,bin,$(shell ls libs/*.js))
CHUCK=$(subst chuck,bin/chuck,$(shell ls chuck/*.ck))
SAMPLES=$(subst chuck,bin/chuck,$(shell ls chuck/*/*.wav))
WEBCHUCK=$(subst libs,bin,$(shell ls libs/webchuck/*))
SVGS=$(shell ls pre_res/spell_svg/*.svg)
PNGS=$(subst pre_res/spell_svg,res/spell,$(SVGS:.svg=.png))


res/spell:
	$(shell mkdir $@)

res/spell/%.png: pre_res/spell_svg/%.svg res/spell
	inkscape $< --export-background-opacity=0 -w 1080 --export-area-drawing -o $@

bin/favicon.png: pre_res/icon.svg
	inkscape $< --export-background-opacity=0 -w 128 --export-area-drawing -o $@

bin/favicon.ico: CONVERT := magick
bin/favicon.ico: bin/favicon.png
	$(CONVERT) bin/favicon.png  bin/favicon.ico

bin/index.html: js.html
	cp $< $@

bin:
	mkdir $@

bin/webchuck: | bin
	mkdir $@

bin/chuck: | bin
	mkdir $@

bin/chuck/percussions: | bin/chuck
	mkdir $@

bin/chuck/glockenspiel: | bin/chuck
	mkdir $@

bin/chuck/woodblocks: | bin/chuck
	mkdir $@

bin/chuck/shakers: | bin/chuck
	mkdir $@

bin/chuck/piano: | bin/chuck
	mkdir $@

bin/chuck/sfx: | bin/chuck
	mkdir $@

bin/%.js: libs/%.js | bin
	cp $< $@

bin/chuck/%.ck: chuck/%.ck | bin/chuck
	cp $< $@

bin/chuck/percussions/%.wav: chuck/percussions/%.wav | bin/chuck/percussions
	cp $< $@

bin/chuck/piano/%.wav: chuck/piano/%.wav | bin/chuck/piano
	cp $< $@

bin/chuck/woodblocks/%.wav: chuck/woodblocks/%.wav | bin/chuck/woodblocks
	cp $< $@

bin/chuck/shakers/%.wav: chuck/shakers/%.wav | bin/chuck/shakers
	cp $< $@

bin/chuck/glockenspiel/%.wav: chuck/glockenspiel/%.wav | bin/chuck/glockenspiel
	cp $< $@

bin/chuck/sfx/%.wav: chuck/sfx/%.wav | bin/chuck/sfx
	cp $< $@

bin/webchuck/%.js: libs/webchuck/%.js | bin/webchuck
	cp $< $@

bin/webchuck/%.wasm: libs/webchuck/%.wasm | bin/webchuck
	cp $< $@

js: $(PNGS) $(LIBS) $(CHUCK) $(SAMPLES) $(WEBCHUCK) bin/favicon.ico bin/index.html
	haxe js.hxml

js-debug: $(PNGS) $(LIBS) $(CHUCK) $(SAMPLES) $(WEBCHUCK) bin/favicon.ico bin/index.html
	haxe js.hxml --debug
