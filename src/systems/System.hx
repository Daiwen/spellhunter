/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package systems;

class System {
    public static var ALL : Array<System> = [];
    public static var GC : Array<System> = [];

	public var destroyed(default,null) = false;
	public var tmod(get,never) : Float; inline function get_tmod() return Game.ME.tmod;

	public var cd : dn.Cooldown;

	public function new () {
		ALL.push(this);

		cd = new dn.Cooldown(Const.FPS);
	}

    public function dispose() {
        ALL.remove(this);

		cd.dispose();
		cd = null;
    }

    public function preUpdate() {
		cd.update(tmod);
	}

	public function update() {
	}

    public function postUpdate() {
	}
}
