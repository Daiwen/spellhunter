/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package systems;

import Level.Coord;
import haxe.ds.Option;

interface Source {
	public var cx(get, set) : Int;
	public var cy(get, set) : Int;
	public var xr(get, set) : Float;
	public var yr(get, set) : Float;
	public var centerX(get,never) : Float;
	public var centerY(get,never) : Float;

	public function checkHit(e : Entity, isEffect : Bool) : Bool;
	public function checkHitPosition(p : Coord) : Bool;
}

typedef Action = {
	var source : Source;
	var entity : Option<ent.comp.Spellable>;
}

class TileSource implements Source {
    public var cx(get, set) : Int;
	inline function get_cx() return proto.cx;
	inline function set_cx(v : Int) return proto.cx = v;
    public var cy(get, set) : Int;
	inline function get_cy() return proto.cy;
	inline function set_cy(v : Int) return proto.cy = v;
    public var xr(get, set) : Float;
	inline function get_xr() return proto.xr;
	inline function set_xr(v : Float) return proto.xr = v;
    public var yr(get, set) : Float;
	inline function get_yr() return proto.yr;
	inline function set_yr(v : Float) return proto.yr = v;

	public var centerX(get,never) : Float; inline function get_centerX() return (cx+xr)*Const.GRID;
	public var centerY(get,never) : Float; inline function get_centerY() return (cy+yr)*Const.GRID;

	var proto : ProtoEntity;

	public function new(p : ProtoEntity){
		this.proto = p;
	}

	public function checkHit(e : Entity, isEffect : Bool) {
		var myShape = tools.Geometry.Shape.Circle((cx+0.5) * Const.GRID, (cy+0.5) * Const.GRID, Const.GRID*0.75);
		var eShape = e.collisionShape;

		return tools.Geometry.hasShapeShapeIntersection(myShape, eShape);
	}

	public function checkHitPosition(c : Coord) {
		var cx, cy;
		switch c {
			case Point(x, y):
				cx = x;
				cy = y;
		}
		var dist = M.distSqr((cx + .5) * Const.GRID, (cy + .5) * Const.GRID, centerX, centerY);
		var rad = Const.GRID + Const.GRID*0.10;
		return dist < rad * rad;
	}
}

class Heat extends System {
	public static var ME : Heat;

	var sources : Array<Source>;
	var tiles : Map<Coord, Source>;
	var tileNeighbours : Map<Coord, Bool>;

	var heatActions : Array<Action>;
	var coolActions : Array<Action>;

	var heatImmune : Array<Entity>;
	var coolImmune : Array<Entity>;

	var warming : Map<ent.comp.Elementable, ent.comp.Elementable.WarmingData>;
	var warmingTile : Map<Coord, Bool>;

	public function new() {
		super();
		ME = this;
	}

	public function addEntity(el : Entity) {
		sources.push(el);
	}

	public function removeEntity(el : Entity) {
		sources.remove(el);
	}

	public function addTile(p : ProtoEntity) {
		var s = new TileSource(p);
		sources.push(s);
		var c = Point(p.cx, p.cy);
		tiles.set(c, s);
		tileNeighbours.remove(c);

		var cx, cy;
		switch c {
			case Point(x, y):
				cx = x;
				cy = y;
		}

		var ns = [ Point(cx + 1, cy), Point(cx - 1, cy), Point(cx, cy + 1), Point(cx, cy - 1) ] ;

		for (n in ns) {
			if (!tiles.exists(n)) {
				tileNeighbours.set(n, true);
			}
		}
	}

	public function removeTile(p : ProtoEntity) {
		var c = Point(p.cx, p.cy);
		var s = tiles.get(c);
		sources.remove(s);
		tiles.remove(c);

		var cx, cy;
		switch c {
			case Point(x, y):
				cx = x;
				cy = y;
		}

		var ns = [ Point(cx + 1, cy), Point(cx - 1, cy), Point(cx, cy + 1), Point(cx, cy - 1) ] ;

		for (n in ns) {
			if (tiles.exists(n)) {
				tileNeighbours.set(c, true);
				break;
			}
		}
	}

	override function update() {
		var time = .1;
		var level = Game.ME.level;
		var wid = level.wid;

		var notWarmingTile = warmingTile.copy();
		var notWarming = warming.copy();

		for (s in sources) {
			var affected = ent.comp.Elementable.ALL.filter(function (e) { return (!e.me.isDead && e.me != s && !heatImmune.contains(e.me) && s.checkHit(e.me, true)); });
			var affectedEntities = spl.Factory.resolveLinks(affected);

			for (a in affectedEntities) {
				var ael = a.me.getElementable();
				switch ael {
					case Some (ael):

						switch ael.getWarmable() {
							case Some(d) if (!warming.exists(ael)) :
								cd.setS("warmup"+(ael.me.uid), d.time, true, () -> { d.hot(); warming.remove(ael); });
								warming.set(ael, d);
								ael.warmUp();
							default:
						}

						notWarming.remove(ael);
					case None:
				}
			}

			for (c in spl.Factory.getAffectedCells(s)) {
				var cx, cy;
				switch c {
					case Point(x, y):
						cx = x;
						cy = y;
				}

				if (!warmingTile.exists(c) && level.hasWarmable(cx, cy)) {
					cd.setS("warmupTile"+(cx*wid+cy), time, true, () -> { level.warmUp(cx, cy); warmingTile.remove(c); });
					warmingTile.set(c, true);
				}

				if (level.hasWarmable(cx, cy)) {
					notWarmingTile.remove(c);
				}
			}
		}


		for (c in notWarmingTile.keys()) {
				var cx, cy;
				switch c {
					case Point(x, y):
						cx = x;
						cy = y;
				}

				cd.unset("warmupTile"+(cx*wid+cy));
				warmingTile.remove(c);
		}

		for (i in notWarming.keyValueIterator()) {
				cd.unset("warmup"+(i.key.me.uid));
				warming.remove(i.key);
				i.value.interrupt();
		}

		for (a in heatActions) {
			var el = a.source;
			var affected = ent.comp.Elementable.ALL.filter(function (e) { return (!e.me.isDead && e.me != el && !heatImmune.contains(e.me) && el.checkHit(e.me, true)); });
			switch (a.entity) {
				case Some(e): affected.remove(e);
				case None:
			}
			var affectedEntities = spl.Factory.resolveLinks(affected);

			for (a in affectedEntities) {
				var ael = a.me.getElementable();
				switch ael {
					case Some (ael):
						ael.warmUpSkipTransition();
					case None:
				}
			}

			for (c in spl.Factory.getAffectedCells(el)) {
				var cx, cy;
				switch c {
					case Point(x, y):
						cx = x;
						cy = y;
				}

				Game.ME.level.warmUp(cx, cy);
			}
		}

		for (a in coolActions) {
			var el = a.source;
			var affected = ent.comp.Elementable.ALL.filter(function (e) { return ( !e.me.isDead && e.me != el && !coolImmune.contains(e.me) && el.checkHit(e.me, true)); });
			switch (a.entity) {
				case Some(e): affected.remove(e);
				case None:
			}
			var affectedEntities = spl.Factory.resolveLinks(affected);

			for (a in affectedEntities) {
				var ael = a.me.getElementable();
				switch ael {
					case Some (ael):
						cd.unset("warmup"+(a.me.uid));
						warming.remove(ael);
						ael.coolDown();
					case None:
				}
			}

			for (c in spl.Factory.getAffectedCells(el)) {
				var cx, cy;
				switch c {
					case Point(x, y):
						cx = x;
						cy = y;
				}

				cd.unset("warmupTile"+(cx*wid+cy));
				warmingTile.remove(c);
				Game.ME.level.coolDown(cx, cy);
			}
		}


		heatActions = new Array();
		coolActions = new Array();

		heatImmune = new Array();
		coolImmune = new Array();
	}

	override function postUpdate() {
		super.postUpdate();
	}

	public function addHeatAction(s : Source, sp : Option<ent.comp.Spellable>) {
		switch (sp) {
			case Some (e): coolImmune.push(e.me);
			case None: None;
		}

		var a = { source : s, entity : sp};
		heatActions.push(a);
	}

	public function addCoolAction(s : Source, sp : Option<ent.comp.Spellable>) {
		switch (sp) {
			case Some (e): heatImmune.push(e.me);
			case None:
		}

		var a = { source : s, entity : sp};
		coolActions.push(a);
	}

	public function init() {
		sources = new Array();
		tiles = new Map();
		tileNeighbours = new Map();
		heatActions = new Array();
		coolActions = new Array();
		warmingTile = new Map();
		warming = new Map();

		heatImmune = new Array();
		coolImmune = new Array();
	}
}
