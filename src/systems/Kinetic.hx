/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package systems;

import dn.Process;
import Level.Coord;
import haxe.ds.Option;

typedef CollisionData = {
	var point : h2d.col.Point;
	var canCollide : () -> Bool;
}

typedef BumpData = {
	var entity : Entity;
	var dx : Float;
	var dy : Float;
}

class Kinetic extends System {
	public static var ME : Kinetic;

	public var level(get,never) : Level; inline function get_level() return Game.ME.level;

	var slides : Map<Coord, Enum_DoorDirection>;
	var entities : Array<ent.comp.Collidable>;
	var repulsors : Map<ent.comp.Collidable, Int>;
	var colliders : Array<ent.comp.Collidable>;
	var endCollisions : Map<ent.comp.Collidable, CollisionData>;
	var toBump : Array<BumpData>;

	public function new() {
		super();
		ME = this;
	}

	public function addEntity(e : ent.comp.Collidable) {
		entities.push(e);
	}

	public function removeEntity(e : ent.comp.Collidable) {
		entities.remove(e);
	}

	function updateEntity(c : ent.comp.Collidable) {
		var e = c.me;

		var slide = slides.get(Point(e.cx, e.cy));

		if (slide != null) {
			var force = .09;
			switch (slide) {
				case Up:
					if (e.dy >= 0)
						e.dy = -force;
				case Down:
					if (e.dy <= 0)
						e.dy = force;
				case Left:
					if (e.dx >= 0)
						e.dx = -force;
				case Right:
					if (e.dx <= 0)
						e.dx = force;
			}
		}

		var	cdata = endCollisions.get(c);

		// X
		var steps = M.ceil( M.fabs(e.dxTotal*tmod) );
		var step = e.dxTotal*tmod / steps;
		while( steps>0 ) {
			var oldX = e.centerX;
			e.xr+=step;

			if( ( e.canCollide && level.hasCollision(e.cx+1,e.cy)) && e.xr>=0.8 ) {
				e.xr = 0.8;
				e.dx = 0; // stop movement
			}

			if( ( e.canCollide && level.hasCollision(e.cx-1,e.cy)) && e.xr<=0.2 ) {
				e.xr = 0.2;
				e.dx = 0;
			}

			while( e.xr>1 ) { e.xr--; e.cx++; }
			while( e.xr<0 ) { e.xr++; e.cx--; }
			steps--;

			if ( cdata != null && (oldX < cdata.point.x && cdata.point.x <= e.centerX || oldX > cdata.point.x && cdata.point.x >= e.centerX) && cdata.canCollide() )
				break;
		}

		if( ( e.canCollide && level.hasCollision(e.cx+1,e.cy)) && e.xr>=0.8 ) {
			e.xr = 0.8;
			e.dx = 0; // stop movement
		}

		if( ( e.canCollide && level.hasCollision(e.cx-1,e.cy)) && e.xr<=0.2 ) {
			e.xr = 0.2;
			e.dx = 0;
		}

		e.dx*=Math.pow(e.frict,tmod);
		e.bdx*=Math.pow(e.bumpFrict,tmod);
		if( M.fabs(e.dx)<=0.0005*tmod ) e.dx = 0;
		if( M.fabs(e.bdx)<=0.0005*tmod ) e.bdx = 0;

		// Y
		var steps = M.ceil( M.fabs(e.dyTotal*tmod) );
		var step = e.dyTotal*tmod / steps;
		while( steps>0 ) {
			var oldY = e.centerY;
			e.yr+=step;

			if( e.canCollide && level.hasCollision(e.cx,e.cy-1) && e.yr<=0.2 ) {
				e.dy = 0;
				e.yr = 0.2;
			}
			if( e.canCollide && level.hasCollision(e.cx,e.cy+1) && e.yr>=0.9 ) {
				e.dy = 0;
				e.yr = 0.9;
			}

			while( e.yr>1 ) { e.yr--; e.cy++; }
			while( e.yr<0 ) { e.yr++; e.cy--; }
			steps--;

			if ( oldY != e.centerY ) {
				Game.ME.sortEntities();
			}
			if ( cdata != null && (oldY < cdata.point.y && cdata.point.y <= e.centerY || oldY > cdata.point.y && cdata.point.y >= e.centerY) && cdata.canCollide() )
				break;
		}

		if( e.canCollide && level.hasCollision(e.cx,e.cy-1) && e.yr<=0.2 ) {
			e.dy = 0;
			e.yr = 0.2;
		}
		if( e.canCollide && level.hasCollision(e.cx,e.cy+1) && e.yr>=0.9 ) {
			e.dy = 0;
			e.yr = 0.9;
		}

		e.dy*=Math.pow(e.frict,tmod);
		e.bdy*=Math.pow(e.bumpFrict,tmod);
		if( M.fabs(e.dy)<=0.0005*tmod ) e.dy = 0;
		if( M.fabs(e.bdy)<=0.0005*tmod ) e.bdy = 0;

	}

	override function update() {
		super.update();
		applyRepulsors();

		for (e in entities) {
			updateEntity(e);
		}

		for (e in colliders) {
			var collided = ent.comp.Collidable.ALL.filter(function (ec) { return e.checkHit(ec.me); });
			e.collide(collided);
		}
	}

	override function postUpdate() {
		super.update();

		applyBumps();
	}

	private function applyBumps() {
		for (b in toBump) {
			var e = b.entity;
			var dx = b.dx;
			var dy = b.dy;

			e.superBump(dx, dy);

			switch (e.getCollidable()) {
				case Some (c):
					var collided = ent.comp.Collidable.ALL.filter(function (ec) { return c.checkHit(ec.me); });
					updateEndCollision(c, collided);
				case None:
			}
		}

		toBump = new Array();
	}

	private function applyRepulsors() {
		for (i in repulsors.keyValueIterator()) {
			var me = i.key.me;
			var mass1 = i.value;
			for (j in repulsors.keyValueIterator()) {
				var you = j.key.me;
				var mass2 = j.value;
				if( mass1 <= mass2 && me.uid != you.uid && Math.abs(me.cx-you.cx) <= 2 && Math.abs(me.cy-you.cy) <= 2 ) {
					if( me.checkHit(you, false) ) {
						var dist = M.dist(you.centerX, you.centerY, me.centerX, me.centerY);
						var ang = Math.atan2(you.centerY-me.centerY, you.centerX-me.centerX);
						var force = 0.4;
						var repelPower = (me.collisionRadius+you.collisionRadius - dist) / (me.collisionRadius+you.collisionRadius);
						me.dx -= Math.cos(ang) * repelPower * force;
						me.dy -= Math.sin(ang) * repelPower * force;
					}
				}
			}
		}
	}

	public function init() {
		entities = new Array();
		slides = new Map();
		repulsors = new Map();
		colliders = new Array();
		endCollisions = new Map();
		toBump = new Array();
	}

	public function addSlide (x : Int, y : Int, d : Enum_DoorDirection) {
		slides.set(Point(x, y), d);
	}

	public function removeSlide (x : Int, y : Int) {
		slides.remove(Point(x, y));
	}

	public function addRepulsor(e : ent.comp.Collidable, mass : Int) {
		repulsors.set(e, mass);
	}

	public function removeRepulsor(e : ent.comp.Collidable) {
		repulsors.remove(e);
	}

	public function setSpeed(c : ent.comp.Collidable, dx : Float, dy : Float) {
		var e = c.me;

		e.dx = dx;
		e.dy = dy;
	}

	public function superBump(e : Entity, dx : Float, dy : Float) {
		toBump.push({ entity : e, dx : dx, dy : dy });
	}

	public function addCollider(e : ent.comp.Collidable) {
		colliders.push(e);
		if (!e.me.isStatic()) {
			updateEndCollision(e);
		}
	}

	private function updateEndCollision(c : ent.comp.Collidable, ?ignored=null) {
		var endCollision = c.computeCollision(ignored);

		switch (endCollision) {
			case Some (data):
				endCollisions.set(c, data);
			case None:
		}
	}

	public function removeCollider(e : ent.comp.Collidable) {
		colliders.remove(e);
		endCollisions.remove(e);
	}
}
