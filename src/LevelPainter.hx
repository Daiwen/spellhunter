/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import tools.IntSet;
import tools.WFC;
import haxe.ds.Option;

@:build(tileAnnotator.JsonMacro.justTypes("./res/world/ntileset_annot.json"))
class Biome {
	public var isAntiMagic : Bool;
	public var isWall : Bool;
	public var liquid : TA_biome;
	public var ground : TA_biome;
	public var collision : TA_biome;

	public function new(am, w, g, c) {
		isAntiMagic = am;
		isWall = w;
		ground = g;
		collision = c;

		var liquids = [ TA_biome.Water, TA_biome.Ice, TA_biome.Lava, TA_biome.SolidLava ];
		liquid = liquids.contains(ground) ? ground : liquids.contains(collision) ? collision : TA_biome.Empty;
	}

	public function getQuarterTile(l : TA_level) {
		var empty = LevelPainter.emptyQuarterTile;

		var getBiome = (b1, b2) -> b1 == b2 ? b1 : TA_biome.Empty;
		var getBiomes = (b, refs : Array<TA_biome>) -> refs.contains(b) ? b : TA_biome.Empty;

		var t =
			switch l {
				case GAntiMagicEdge | WAntiMagic : {
					  isWall : isWall
					, isAntiMagic : isAntiMagic
					, biome : getBiomes(collision, [TA_biome.Wood, TA_biome.Stone ])
					}
				case GWoodEdge | WWood : {
					  isWall : isWall
					, isAntiMagic : isAntiMagic
					, biome : getBiome(collision, TA_biome.Wood)
					}
				case GStoneEdge | WStone : {
					  isWall : isWall
					, isAntiMagic : false
					, biome : getBiome(collision, TA_biome.Stone)
					}
				case AboveEdge : { isWall : isWall, isAntiMagic : isAntiMagic, biome : isWall ? collision : ground };
				case GGrass : {
					  isWall : false
					, isAntiMagic : false
					, biome : getBiome(ground, TA_biome.Grass)
					}
				case GSand : {
					  isWall : false
					, isAntiMagic : false
					, biome : getBiome(ground, TA_biome.Sand)
				}
				case GAntiMagic : {
					  isWall : false
					, isAntiMagic : (ground == TA_biome.Empty || !isWall) && isAntiMagic
					, biome : liquid
					}
				case GAntiMagicLiquid : {
					  isWall : isWall
					, isAntiMagic : isAntiMagic
					, biome : liquid
					}
				case GSolidLava : {
					  isWall : false
					, isAntiMagic : false
					, biome : getBiome(ground, TA_biome.SolidLava)
					}
				case GIce : {
					  isWall : false
					, isAntiMagic : false
					, biome : getBiome(ground, TA_biome.Ice)
					}
				case BelowEdge : {
					  isWall : isWall
					, isAntiMagic : isAntiMagic
					, biome : collision == TA_biome.Empty ? ground : collision
					}
			}

		return t;
	}
}

@:build(tileAnnotator.JsonMacro.justTypes("./res/world/ntileset_annot.json"))
enum QTC { QT (isWall : Bool, isAntiMagic : Bool, biome : TA_biome); }

enum ConstraintLocation {
	TopLeft (c : QTC);
	Top (l : QTC, r : QTC);
	TopRight (c : QTC);
	Right (u : QTC, d : QTC);
	BottomRight (c : QTC);
	Bottom (l : QTC, r : QTC);
	BottomLeft (c : QTC);
	Left (u : QTC, d : QTC);
}

class Neighbours extends tools.Neighbours {
	static public function all(annotations : Array<TA_annotations>) {
		var neighbours = new Array();
		for (a in annotations) {
			var c = a.constraints;
			var i = tools.Neighbours.full(annotations.length);
			var t = { neighbours : i, priority : a.priority, kind : new EquatableTC(c, Some (a.level)) };
			neighbours.push(t);
		}

		return neighbours;
	}

	static public function fromAnnotations(annotations : Array<TA_annotations>) : Array<tools.Tile<EquatableTC>> {
		var buckets = new Map();

		var addTile = (i, c) -> {
			var s = buckets.get(c);

			if (null == s) {
				s = new IntSet();
				buckets.set(c, s);
			}

			s.add(i);
		};

		var toEnum = (c) -> QT(c.isWall, c.isAntiMagic, c.biome);

		for (i in annotations.keyValueIterator()) {
			var constraints = i.value.constraints;
			addTile(i.key, TopLeft(toEnum(constraints.top_left)));
			addTile(i.key, TopRight(toEnum(constraints.top_right)));
			addTile(i.key, BottomRight(toEnum(constraints.bottom_right)));
			addTile(i.key, BottomLeft(toEnum(constraints.bottom_left)));

			addTile(i.key, Top(toEnum(constraints.top_left), toEnum(constraints.top_right)));
			addTile(i.key, Bottom(toEnum(constraints.bottom_left), toEnum(constraints.bottom_right)));
			addTile(i.key, Right(toEnum(constraints.top_right), toEnum(constraints.bottom_right)));
			addTile(i.key, Left(toEnum(constraints.top_left), toEnum(constraints.bottom_left)));
		}

		var getCandidates = (k) -> {
			var s = buckets.get(k);

			if (null == s) {
				s = new IntSet();
			}

			return s;
		};

		var neighbours = new Array();
		for (a in annotations) {
			var c = a.constraints;
			var i = new tools.Neighbours();

			i.topLeft = getCandidates(BottomRight(toEnum(c.top_left)));
			i.topRight = getCandidates(BottomLeft(toEnum(c.top_right)));
			i.bottomLeft = getCandidates(TopRight(toEnum(c.bottom_left)));
			i.bottomRight = getCandidates(TopLeft(toEnum(c.bottom_right)));

			i.top = getCandidates(Bottom(toEnum(c.top_left), toEnum(c.top_right)));
			i.bottom = getCandidates(Top(toEnum(c.bottom_left), toEnum(c.bottom_right)));
			i.left = getCandidates(Right(toEnum(c.top_left), toEnum(c.bottom_left)));
			i.right = getCandidates(Left(toEnum(c.top_right), toEnum(c.bottom_right)));
			var t = { neighbours : i, priority : a.priority, kind : new EquatableTC(c, Some (a.level)) };
			neighbours.push(t);
		}

		return neighbours;
	}
}

class EquatableTC {
	public var c(default, null) : TA_tile;
	var level : Option<TA_level>;

	public function new (c : TA_tile, l : Option<TA_level>) {
		this.c = c;
		level = l;
	}

	static function isEmptyTile(qt) {
		return qt.biome == Empty
			&& !qt.isAntiMagic
			&& !qt.isWall;
	}

	static function isCompatibleEmpty(l, qt1, qt2) {
		return
			isEmptyTile(qt1)
			&& switch l {
				case AboveEdge : true;
				case WAntiMagic: true;
				case GAntiMagicEdge: true;
				case GWoodEdge: true;
				case GStoneEdge: true;
				case WWood : TA_biome.Wood != qt2.biome;
				case WStone : TA_biome.Stone != qt2.biome;
				case GGrass : TA_biome.Grass != qt2.biome;
				case GSand : TA_biome.Sand != qt2.biome;
				case GAntiMagic : true;
				case GAntiMagicLiquid : qt2.isWall || !qt2.isAntiMagic;
				case GSolidLava : TA_biome.SolidLava != qt2.biome;
				case GIce : TA_biome.Ice != qt2.biome;
				case BelowEdge : TA_biome.Hole != qt2.biome;
			}
	}

	static function isCompatibleLiquid(l, qt1, qt2) {
		var isLiquid = (qt) -> [ TA_biome.Water, TA_biome.Ice, TA_biome.Lava, TA_biome.SolidLava ].contains(qt.biome);
		return
			qt1.isWall == qt2.isWall
			&& qt1.isAntiMagic == qt2.isAntiMagic
			&& switch l {
				case GAntiMagicLiquid : isLiquid(qt1) && isLiquid(qt2);
				default : false;
			}
	}


	static function isCompatibleHole(l, qt1, qt2) {
		return
			!(qt2.biome == TA_biome.Hole)
			&& qt1.isWall
			&& !qt1.isAntiMagic
			&& qt1.biome == TA_biome.Empty
			&& l == BelowEdge;
	}

	static function isCompatible(l, qt1, qt2) {
		return isCompatibleEmpty(l, qt1, qt2)
			|| isCompatibleLiquid(l, qt1, qt2)
			|| isCompatibleHole(l, qt1, qt2);
	}

	public static function isQuarterTileEqual(qt1, l1, qt2, l2) {
		var isEqual = (qt1, qt2) ->
			qt1.isWall == qt2.isWall
			&& qt1.isAntiMagic == qt2.isAntiMagic
			&& qt1.biome == qt2.biome;

		return
			isEqual(qt1, qt2)
			|| switch [l1, l2] {
				case [Some (l), None] : isCompatible(l, qt1, qt2);
				case [None, Some (l)] : isCompatible(l, qt2, qt1);
				default : false;
			};
	}

	public static function isTileEqual(c1, ?level1 = None, c2, ?level2 = None) {
		return isQuarterTileEqual(c1.top_left, level1, c2.top_left, level2) &&
			isQuarterTileEqual(c1.top_right, level1, c2.top_right, level2) &&
			isQuarterTileEqual(c1.bottom_left, level1, c2.bottom_left, level2) &&
			isQuarterTileEqual(c1.bottom_right, level1, c2.bottom_right, level2);
	}

	public function isEqual(ec : EquatableTC) {
		return isTileEqual(c, level, ec.c, ec.level);
	}
}

@:build(tileAnnotator.JsonMacro.addFields("annotatedTiles", "./res/world/ntileset_annot.json"))
class LevelPainter {
	var state : Array<Array<Biome>>;
	var seed : Int;
	var level : Level;

	public var bgH(default, null) : Int;
	public var bgW(default, null) : Int;

	var layerSolvers : Array<tools.WFC<EquatableTC>>;
	var layerInvalidated : Array<Bool>;
	var bgSolver : tools.WFC<EquatableTC>;
	var tiles : Array<Array<TA_Tile>>;
	var bgTiles : Array<TA_Tile>;
	public var topLayers(default, null) : Array<Array<TA_Tile>>;
	public var bottomLayers(default, null) : Array<Array<TA_Tile>>;
	public var bgLayer(default, null) : Array<TA_Tile>;

	final layerNames = haxe.EnumTools.createAll(TA_level);
	final topLayerNames = [ WAntiMagic, WWood, WStone ];
	final bottomLayerNames = [ BelowEdge, GSolidLava, GAntiMagic, GAntiMagicLiquid, GSand, GGrass, GAntiMagicEdge, GWoodEdge, GStoneEdge, GIce, AboveEdge ];

	public static final emptyQuarterTile = {
		  isWall : false
		, isAntiMagic : false
		, biome : TA_biome.Empty
		};

	public static final emptyTile = {
		  top_left : emptyQuarterTile
	    , top_right : emptyQuarterTile
		, bottom_left : emptyQuarterTile
		, bottom_right : emptyQuarterTile
		};

	public function new(l : Level, seed : Int) {
		layerNames.reverse();
		level = l;
		state = new Array();
		this.seed = seed;

		for (cx in 0...l.wid) {
			state[cx] = new Array();

			for (cy in 0...l.hei) {
				var antiMagic =
					   l.level.l_NCollisions.getInt(cx, cy) == 1
					|| l.level.l_NCollisions.getInt(cx, cy) == 8
					|| l.level.l_Ground.getInt(cx, cy) == 4
					|| l.level.l_Ground.getInt(cx, cy) == 5;
				if (antiMagic) {
					level.initAntiMagic(cx, cy);
				}

				var v = l.level.l_NCollisions.getInt(cx, cy);
				var wall = false;
				var collision = TA_biome.Empty;
				switch (v) {
					case 1:
						wall = true;
						level.initWall(cx, cy);
					case 2:
						wall = true;
						collision = TA_biome.Wood;
						new ent.WoodWall(cx, cy);
					case 3:
						wall = true;
						collision = TA_biome.Stone;
						new ent.StoneWall(cx, cy);
					case 4:
						wall = false;
						collision = TA_biome.Water;
						level.addWater(cx, cy);
					case 5:
						wall = false;
						collision = TA_biome.Lava;
						level.createLavaLight(cx, cy, true);
						level.addLava(cx, cy);
					case 6:
						wall = false;
						collision = TA_biome.Empty;
						level.initAltar(cx, cy);
					case 7:
						wall = false;
						collision = TA_biome.Hole;
						level.initHole(cx, cy);
					case 8:
						wall = false;
						collision = TA_biome.Lava;
						level.createLavaLight(cx, cy, true);
						level.addLava(cx, cy);
					case 9:
						level.initEndZone(cx, cy);
					default:
				}

				var v = l.level.l_Ground.getInt(cx, cy);
				var ground =
					switch (v) {
						case 1: TA_biome.Grass;
						case 2: TA_biome.Sand;
						case 3 | 5:
							level.createLavaLight(cx, cy, false);
							level.addSolidLava(cx, cy);
							TA_biome.SolidLava;
						case 4: TA_biome.Empty;
						case 6:
							level.addIce(cx, cy);
							TA_biome.Ice;
						default:
							TA_biome.Empty;
					}

				state[cx][cy] = new Biome(antiMagic, wall, ground, collision);
			}
		}

		var defaultConstraint = new EquatableTC(emptyTile, None);

		layerSolvers = new Array();
		layerInvalidated = new Array();
		topLayers = new Array();
		bottomLayers = new Array();
		bgLayer = new Array();
		tiles = new Array();

		for (f in layerNames.keyValueIterator()) {
			tiles[f.key] = annotatedTiles.filter((t : TA_Tile) -> t.annotations.level == f.value || EquatableTC.isTileEqual(t.annotations.constraints, None, emptyTile, None));
			var annots = tiles[f.key].map((t) -> t.annotations);
			var tiles =
				switch (f.value) {
					case (AboveEdge | BelowEdge |  GAntiMagicEdge | GWoodEdge | GStoneEdge) : Neighbours.all(annots);
					case WAntiMagic
						| WStone
						| WWood
						| GGrass
						| GSand
						| GAntiMagic
						| GAntiMagicLiquid
						| GSolidLava
						| GIce : Neighbours.fromAnnotations(annots);
			}
			layerSolvers[f.key] = new tools.WFC(l.hei-1, l.wid-1, tiles, defaultConstraint);
			layerSolvers[f.key].seed = seed;
			layerInvalidated[f.key] = true;
		}

		initBG(seed);

		for (i in 0...(l.wid - 1)) {
			for (j in 0...(l.hei - 1)) {
				for (ls in layerSolvers.keyValueIterator()) {
					var c = new EquatableTC({
					  top_left : state[i][j].getQuarterTile(layerNames[ls.key])
					, bottom_left : state[i][j+1].getQuarterTile(layerNames[ls.key])
					, top_right : state[i+1][j].getQuarterTile(layerNames[ls.key])
					, bottom_right : state[i+1][j+1].getQuarterTile(layerNames[ls.key])
					},
					None);

					ls.value.setTile(i, j, c);
				}
			}
		}
	}

	var grassConstraint : EquatableTC;
	var grassTiles : Array<tools.Tile<EquatableTC>>;

	function initBG(seed) {
		var grassQT = {
		  isWall : false
		, isAntiMagic : false
		, biome : TA_biome.Grass
		};
		var grassTile = {
		  top_left : grassQT
		, bottom_left : grassQT
		, top_right : grassQT
		, bottom_right : grassQT
		};
		grassConstraint = new EquatableTC(grassTile, None);

		bgTiles = annotatedTiles.filter((t : TA_Tile) -> EquatableTC.isTileEqual(t.annotations.constraints, None, grassTile, None));
		var annots = bgTiles.map((t) -> t.annotations);
		grassTiles = Neighbours.fromAnnotations(annots);

		resizeBG();
	}

	function resizeBG() {
		var nbgH = Math.ceil(Game.ME.stageHei/(tileSize * Const.SCALE));
		var nbgW = Math.ceil(Game.ME.stageWid/(tileSize * Const.SCALE));

		if (null != bgH && null != bgW && nbgH <= bgH && nbgW <= bgW)
			return;

		bgH = nbgH;
		bgW = nbgW;
		bgSolver = new tools.WFC(bgH, bgW, grassTiles, grassConstraint);
		bgSolver.seed = seed;
	}

	public function runWFC() {
		for (l in layerSolvers.keyValueIterator()) {
			if (!layerInvalidated[l.key])
				continue;

			layerInvalidated[l.key] = false;

			var indices = l.value.run();
			if (l.key < bottomLayerNames.length) {
				bottomLayers[l.key] = indices.map((i) -> tiles[l.key][i]);
			} else {
				topLayers[l.key-bottomLayerNames.length] = indices.map((i) -> tiles[l.key][i]);
			}
		}

		var indices = bgSolver.run();
		bgLayer = indices.map((i) -> bgTiles[i]);
	}

	function invalidateLayer(c) {
		switch c.liquid {
			case SolidLava | Lava: layerInvalidated[layerNames.indexOf(GSolidLava)] = true;
			case Ice | Water: layerInvalidated[layerNames.indexOf(GIce)] = true;
			default:
		}
	}

	public function set(x, y, c) {
		state[x][y] = c;
		invalidateLayer(c);

		for (i in [x-1, x]) {
			for (j in [y-1, y]) {
				if (j == level.hei-1 || i == level.wid-1) {
					continue;
				}

				for (ls in layerSolvers.keyValueIterator()) {
					var c = new EquatableTC({
					  top_left : state[i][j].getQuarterTile(layerNames[ls.key])
					, bottom_left : state[i][j+1].getQuarterTile(layerNames[ls.key])
					, top_right : state[i+1][j].getQuarterTile(layerNames[ls.key])
					, bottom_right : state[i+1][j+1].getQuarterTile(layerNames[ls.key])
					},
					None);

					ls.value.setTile(i, j, c);
				}
			}
		}
	}

	public function setSeed(s) {
		for (l in layerSolvers) {
			l.seed = s;
		}
	}

	public function resize() {
		resizeBG();
	}
}
