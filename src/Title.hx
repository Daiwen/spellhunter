/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2025 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import ui.Components;

@:uiComp("about")
class AboutComp extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <about>
		<container id="cexit">
			<button(Title.getTiles()) id="exit"/>
		</container>
		<text id="about" />
	</about>;

	public function new(c : tools.MenuControl, showMenu : () -> Void, ?parent) {
		super(parent);
		initComponent();

		layout = Vertical;
		verticalAlign = Top;
		horizontalAlign = Middle;
		this.about.text = "Spellhunter Copyright (C) 2021-2023  Quentin Lambert\nThis program comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome to redistribute it under certain conditions.\nVisit https://gitlab.com/Daiwen/spellhunter for more information.";

		this.exit.onClick = showMenu;
		this.exit.onOver = function () {
			ChucK.broadcastEvent("button1");
		}
		this.exit.label =  "Back";

		c.addItem(this.exit);
	}

	public function resize() {
		var t = Title.getTiles();

		this.exit.resize(t.width, t.height);
	}
}

@:uiComp("credits")
class CreditsComp extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <credits>
		<container id="cexit">
			<button(Title.getTiles()) id="exit"/>
		</container>
		<text id="credits"/>
	</credits>;

	public function new(c : tools.MenuControl, showMenu : () -> Void, ?parent) {
		super(parent);
		initComponent();

		layout = Vertical;
		verticalAlign = Top;
		this.credits.text = "a word about the samples used\nsebastien benard\nmagical sand\nclint hoagland\ngmtk\nfamily";

		this.exit.onClick = showMenu;
		this.exit.onOver = function () {
			ChucK.broadcastEvent("button1");
		}
		this.exit.label =  "Back";

		c.addItem(this.exit);
	}


	public function resize() {
		var t = Title.getTiles();

		this.exit.resize(t.width, t.height);
	}
}

typedef MenuOptions = {
	var root : dn.Process;
	var start : () -> Void;
	var load : () -> Void;
	var showAbout : () -> Void;
	var showCredits : () -> Void;
	var control : tools.MenuControl;
}

@:uiComp("title-menu")
class TitleMenuComp extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <title-menu>
		<button(Title.getTiles()) public id="start"/>
		<button(Title.getTiles()) public id="load"/>
		<button(Title.getTiles()) public id="settings"/>
		<button(Title.getTiles()) public id="about"/>
		<button(Title.getTiles()) public id="credits"/>
	</title-menu>;

	public function new(mo : MenuOptions, ?parent) {
		super(parent);

		initComponent();

		layout = Vertical;

		// Override
		this.start.label = "Start";
		this.load.label = "Load";
		this.settings.label = "Settings";
		this.about.label = "About";
		this.credits.label = "Credits";

		this.start.setVerticalMargin(30);
		this.load.setVerticalMargin(30);
		this.settings.setVerticalMargin(30);
		this.about.setVerticalMargin(30);
		this.credits.setVerticalMargin(30);

		this.start.onClick = function() {
			mo.start();
		}
		this.start.onOver = function() {
			mo.control.select(this.start);
			ChucK.broadcastEvent("button1");
		}

		this.load.onClick = function() {
			mo.load();
		}
		this.load.onOver = function() {
			mo.control.select(this.load);
			ChucK.broadcastEvent("button2");
		}

		this.credits.onClick = function() {
			mo.showCredits();
		}
		this.credits.onOver = function() {
			mo.control.select(this.credits);
			ChucK.broadcastEvent("button3");
		}

		this.about.onClick = function() {
			mo.showAbout();
		}
		this.about.onOver = function() {
			mo.control.select(this.about);
			ChucK.broadcastEvent("button4");
		}

		this.settings.onClick = function() {
			new SettingsMenu(mo.root);
		}
		this.settings.onOver = function() {
			mo.control.select(this.settings);
			ChucK.broadcastEvent("button5");
		}

		mo.control.addItem(this.start);
		mo.control.addItem(this.load);
		mo.control.addItem(this.settings);
		mo.control.addItem(this.about);
		mo.control.addItem(this.credits);
	}

	public function resize() {
		var t = Title.getTiles();

		this.start.resize(t.width, t.height);
		this.load.resize(t.width, t.height);
		this.settings.resize(t.width, t.height);
		this.about.resize(t.width, t.height);
		this.credits.resize(t.width, t.height);
	}
}

typedef MenuControllers = {
	var menu : tools.MenuControl;
	var about : tools.MenuControl;
	var credits : tools.MenuControl;
}

@:uiComp("title")
class TitleContainer extends h2d.Object implements h2d.domkit.Object {
	static var SRC = <title>
		<title-menu(mo) id="menu"/>
		<about(cs.about, showMenu) id="about" />
		<credits(cs.credits, showMenu) id="credits" />
	</title>;

	public function new(root : dn.Process, start : () -> Void, load : () -> Void, cs : MenuControllers, ?parent) {
		super(parent);

		var mo = { root : root, start : start, load : load, showAbout : showAbout, showCredits : showCredits, control : cs.menu };

		initComponent();

		this.about.visible = false;
		this.credits.visible = false;

		this.menu.y = 60;
	}

	function showAbout() {
		this.about.visible = true;
		this.menu.visible = false;
		this.credits.visible = false;
	}

	function showCredits() {
		this.credits.visible = true;
		this.menu.visible = false;
		this.about.visible = false;
	}

	function showMenu() {
		this.menu.visible = true;
		this.credits.visible = false;
		this.about.visible = false;
	}

	public function isMenu() {
		return this.menu.visible;
	}

	public function isAbout() {
		return this.about.visible;
	}

	public function isCredits() {
		return this.credits.visible;
	}

	public function resize() {
		this.menu.resize();
		this.about.resize();
		this.credits.resize();
	}
}

class Title extends dn.Process {
	var mask : h2d.Bitmap;
	var img : h2d.Object;
	var spell : h2d.Object;
	var color : h3d.Vector;

	var center : h2d.Flow;

	var menuControl : tools.MenuControl;
	var aboutControl : tools.MenuControl;
	var creditsControl : tools.MenuControl;

	var title : TitleContainer;

	public function new() {
		super(Main.ME);

		var control = new CustomControl();

		engine.backgroundColor = 0x000;

		createRoot(Boot.ME.uiRoot);
		spell = spl.Graphics.spell(Spell (Collider, Elemental (Fire), Projectile));
		var b = spell.getBounds();
		img = new h2d.Object();
		img.addChild(spell);
		img.scale(stageHei*0.95/b.height);
		root.add(img, 0);

		mask = new h2d.Bitmap(h2d.Tile.fromColor(0x0, stageWid, stageHei, 0.6), root);
		root.add(mask, 1);

		center = new h2d.Flow();
		center.horizontalAlign = Middle;
		center.verticalAlign = Top;

		root.add(center, 3);

		menuControl = new tools.MenuControl.VerticalMenuControl(control, "Menu");
		menuControl.repl = Main.ME.repl;
		aboutControl = new tools.MenuControl.VerticalMenuControl(control, "About");
		aboutControl.repl = Main.ME.repl;
		creditsControl = new tools.MenuControl.VerticalMenuControl(control, "Credit");
		creditsControl.repl = Main.ME.repl;

		var controllers = { menu : menuControl, about : aboutControl, credits : creditsControl };
		title = new TitleContainer(this, skip, load, controllers, center);

		ChucK.broadcastEvent("intro");
		ChucK.broadcastEvent("menu");

		menuControl.logSelect();

		onResize();
		dn.Process.resizeAll();
	}

	override function onDispose() {
		super.onDispose();
	}

	function skip() {
		Main.ME.startGame();
		destroy();
	}

	function load() {
		Main.ME.loadGame();
		destroy();
	}

	override function onResize() {
		super.onResize();

		var b = img.getBounds();
		img.x = ( stageWid*0.5 );
		img.y = ( stageHei*0.5 );


		title.resize();

		mask.scaleX = stageWid;
		mask.scaleY = stageHei;

		center.minWidth = center.maxWidth = stageWid;
		center.minHeight = center.maxHeight = stageHei;
	}

	override function postUpdate() {
		super.postUpdate();
	}

	override function update() {
		super.update();

		if (title.isAbout())
			aboutControl.update();

		else if (title.isCredits())
			creditsControl.update();

		else if (title.isMenu())
			menuControl.update();
	}

	static public function getTiles() {
		var w = Std.int(32 * 4 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);
		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.TitleNotSelected)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.TitleSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.TitleSelected)));
		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	override public function resume() {
		super.resume();

		menuControl.logSelect();
	}
}
