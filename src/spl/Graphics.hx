/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;

import spl.Lang;
import spriteTree.SpriteData.SaveData;

typedef SubId = {
	var token : spl.Token.AnyTokenType;
	var index : Int;
}


@:build(spriteTree.JsonMacro.buildTypes("./res/spell/spriteTree.json"))
class Graphics {

	static private function getOffset<T>(d : SaveData<T>) {
		return new h2d.col.Point(d.position.x * Const.SPELL_SCALE, d.position.y * Const.SPELL_SCALE);
	}

	static private function placeSprite<T>(t : h2d.Tile, d : SaveData<T>, color) {
		var hook = new h2d.Object();
		var p = getOffset(d);
		hook.x = p.x;
		hook.y = p.y;
		var img = new h2d.Bitmap(t, hook);
		img.colorAdd = h3d.Vector.fromColor(color);
		img.scaleX = Const.SPELL_SCALE * d.scale;
		img.scaleY = Const.SPELL_SCALE * d.scale;

		return hook;
	}

	static private function makeWordObject<T>(t : h2d.Tile, d : SaveData<T>) {
		var hook = new h2d.Object();
		var word = placeSprite(t, d, 0x5b2c90);
		hook.addChild(word);

		return hook;
	}

	static public function spell(s : Spell) : h2d.Object {
		var hook = makeWordObject(hxd.Res.spell.spell.toTile(), spriteData.spell_png);

		switch s {
			case Spell(k, e, mk) :
				var mk = motionKind(mk);
				var k = kind(k);
				var e = effect(e);

				var sub = new h2d.Object(hook);
				sub.addChild(mk);
				placeSpellSub(AnyMotionKindToken (TMotionKind), sub);

				var sub = new h2d.Object(hook);
				sub.addChild(e);
				placeSpellSub(AnyEffectToken (TEffect), sub);

				var sub = new h2d.Object(hook);
				sub.addChild(k);
				placeSpellSub(AnyKindToken (TKind), sub);
		}

		return hook;
	}

	static public function motionKind(mk : MotionKind) {
		return switch (mk) {
			case Projectile:
				makeWordObject(hxd.Res.spell.projectile.toTile(), spriteData.projectile_png);
			case Attached (Ground):
				makeWordObject(hxd.Res.spell.ground.toTile(), spriteData.ground_png);
			case Attached (Entity (e)):
				var hook = makeWordObject(hxd.Res.spell.attached.toTile(), spriteData.attached_png);
				var e = entity(e);

				var sub = new h2d.Object(hook);
				sub.addChild(e);
				placeAttachedEntity(sub);

				hook;
		}
	}

	static public function entity(e : EntityId) {
		return switch (e) {
			case Caster: makeWordObject(hxd.Res.spell.caster.toTile(), spriteData.caster_png);
			case Marked: makeWordObject(hxd.Res.spell.marked.toTile(), spriteData.marked_png);
		}

	}

	static public function kind(k : Kind) {
		return switch (k) {
			case Collider: makeWordObject(hxd.Res.spell.collider.toTile(), spriteData.collider_png);
			case Trigger: makeWordObject(hxd.Res.spell.trigger.toTile(), spriteData.trigger_png);
			case Activer: makeWordObject(hxd.Res.spell.activer.toTile(), spriteData.activer_png);
		}
	}

	static public function effect(e : Effect) {
		return switch (e) {
			case Elemental(Ice) :
				makeWordObject(hxd.Res.spell.ice.toTile(), spriteData.ice_png);
			case Elemental(Fire) :
				makeWordObject(hxd.Res.spell.fire.toTile(), spriteData.fire_png);
			case Physical (Push) :
				makeWordObject(hxd.Res.spell.push.toTile(), spriteData.push_png);
			case Physical (Teleport) :
				makeWordObject(hxd.Res.spell.teleport.toTile(), spriteData.teleport_png);
			case Mark :
				makeWordObject(hxd.Res.spell.mark.toTile(), spriteData.mark_png);
			case Sequence (e1, e2):
				var hook = makeWordObject(hxd.Res.spell.double.toTile(), spriteData.double_png);
				var e1 = effect(e1);
				var e2 = effect(e2);

				var sub = new h2d.Object(hook);
				sub.addChild(e1);
				placeSequenceSub(0, sub);

				var sub = new h2d.Object(hook);
				sub.addChild(e2);
				placeSequenceSub(1, sub);

				hook;

			case Meta(s) :
				var hook = makeWordObject(hxd.Res.spell.meta.toTile(), spriteData.meta_png);

				var s = spell(s);
				var sub = new h2d.Object(hook);
				sub.addChild(s);
				placeMetaSub(sub);

				hook;
			case Link(e) :
			   var hook = makeWordObject(hxd.Res.spell.link.toTile(), spriteData.link_png);

				var e = entity(e);
				var sub = new h2d.Object(hook);
				sub.addChild(e);
				placeLinkedEntity(sub);

				hook;
		}
	}

	static public function ofToken<T>(t : Token<T>, color : Int) {
		return
			switch t {
				case TCaster : placeSprite(hxd.Res.spell.caster.toTile(), spriteData.caster_png, color);
				case TMarked : placeSprite(hxd.Res.spell.marked.toTile(), spriteData.marked_png, color);
				case TMeta : placeSprite(hxd.Res.spell.meta.toTile(), spriteData.meta_png, color);
				case TPush : placeSprite(hxd.Res.spell.push.toTile(), spriteData.push_png, color);
				case TTeleport : placeSprite(hxd.Res.spell.teleport.toTile(), spriteData.teleport_png, color);
				case TFire : placeSprite(hxd.Res.spell.fire.toTile(), spriteData.fire_png, color);
				case TIce : placeSprite(hxd.Res.spell.ice.toTile(), spriteData.ice_png, color);
				case TSequence : placeSprite(hxd.Res.spell.double.toTile(), spriteData.double_png, color);
				case TTrigger : placeSprite(hxd.Res.spell.trigger.toTile(), spriteData.trigger_png, color);
				case TActiver : placeSprite(hxd.Res.spell.activer.toTile(), spriteData.activer_png, color);
				case TCollider : placeSprite(hxd.Res.spell.collider.toTile(), spriteData.collider_png, color);
				case TProjectile : placeSprite(hxd.Res.spell.projectile.toTile(), spriteData.projectile_png, color);
				case TEntity : placeSprite(hxd.Res.spell.attached.toTile(), spriteData.attached_png, color);
				case TGround : placeSprite(hxd.Res.spell.ground.toTile(), spriteData.ground_png, color);
				case TSpell : placeSprite(hxd.Res.spell.spell.toTile(), spriteData.spell_png, color);
				case TMark : placeSprite(hxd.Res.spell.mark.toTile(), spriteData.mark_png, color);
				case TLink : placeSprite(hxd.Res.spell.link.toTile(), spriteData.link_png, color);
			};
	}

	static public function placeSpellSub(ct : spl.Token.AnyTokenType, c : h2d.Object) {
		switch ct {
			case AnySpellToken (_): return;
			case AnyEntityIdToken (_): return;
			case AnyEffectToken (_):
				c.x = spriteData.spell_png.anchors.effect.point.x * Const.SPELL_SCALE;
				c.y = spriteData.spell_png.anchors.effect.point.y * Const.SPELL_SCALE;
				c.rotation = spriteData.spell_png.anchors.effect.angle;
			case AnyKindToken (_):
				c.x = spriteData.spell_png.anchors.kind.point.x * Const.SPELL_SCALE;
				c.y = spriteData.spell_png.anchors.kind.point.y * Const.SPELL_SCALE;
				c.rotation = spriteData.spell_png.anchors.kind.angle;
			case AnyMotionKindToken (_):
				c.x = spriteData.spell_png.anchors.motionKind.point.x * Const.SPELL_SCALE;
				c.y = spriteData.spell_png.anchors.motionKind.point.y * Const.SPELL_SCALE;
				c.rotation = spriteData.spell_png.anchors.motionKind.angle;
		}
	}

	static public function placeSequenceSub(i : Int, c : h2d.Object) {
		if (i == 0) {
			c.x = spriteData.double_png.anchors.first.point.x * Const.SPELL_SCALE;
			c.y = spriteData.double_png.anchors.first.point.y * Const.SPELL_SCALE;
			c.rotation = spriteData.double_png.anchors.first.angle;
		} else if (i == 1) {
			c.x = spriteData.double_png.anchors.second.point.x * Const.SPELL_SCALE;
			c.y = spriteData.double_png.anchors.second.point.y * Const.SPELL_SCALE;
			c.rotation = spriteData.double_png.anchors.second.angle;
		}

		return;
	}

	static public function placeAttachedEntity(c : h2d.Object) {
		c.x = spriteData.attached_png.anchors.entity.point.x * Const.SPELL_SCALE;
		c.y = spriteData.attached_png.anchors.entity.point.y * Const.SPELL_SCALE;
		c.rotation = spriteData.attached_png.anchors.entity.angle;

		return;
	}

	static public function placeLinkedEntity(c : h2d.Object) {
		c.x = spriteData.link_png.anchors.target.point.x * Const.SPELL_SCALE;
		c.y = spriteData.link_png.anchors.target.point.y * Const.SPELL_SCALE;
		c.rotation = spriteData.link_png.anchors.target.angle;

		return;
	}

	static public function placeMetaSub(c : h2d.Object) {
		c.x = spriteData.meta_png.anchors.spell.point.x * Const.SPELL_SCALE;
		c.y = spriteData.meta_png.anchors.spell.point.y * Const.SPELL_SCALE;
		c.rotation = spriteData.meta_png.anchors.spell.angle;

		return;
	}

	static public function placeObject<T>(t : Token<T>, id : SubId, c : h2d.Object) {
		switch t {
			case TCaster | TMarked : return;
			case TTeleport
				| TPush
				| TFire
				| TIce : return;
			case TMark : return;
			case TActiver | TTrigger | TCollider : return;
			case TProjectile | TGround : return;
			case TSequence : placeSequenceSub(id.index, c);
			case TMeta : placeMetaSub(c);
			case TLink : placeLinkedEntity(c);
			case TEntity : placeAttachedEntity(c);
			case TSpell : placeSpellSub(id.token, c);
		};
	}

	static public function getPlaceHolders(t : spl.Token.AnyTokenType, color : Int) : Null<h2d.Object> {
		switch t {
			case AnySpellToken (_): return null;
			case AnyEntityIdToken (_):
				return placeSprite(hxd.Res.spell.no_entity.toTile(), spriteData.no_entity_png, color);
			case AnyEffectToken (_):
				return placeSprite(hxd.Res.spell.no_effect.toTile(), spriteData.no_effect_png, color);
			case AnyKindToken (_):
				return placeSprite(hxd.Res.spell.no_kind.toTile(), spriteData.no_kind_png, color);
			case AnyMotionKindToken (_):
				return placeSprite(hxd.Res.spell.no_motion.toTile(), spriteData.no_motion_png, color);
		}
	}

	static public function getPlaceHolderOffset(t : spl.Token.AnyTokenType) : h2d.col.Point {
		switch t {
			case AnySpellToken (_): return new h2d.col.Point();
			case AnyEntityIdToken (_): return new h2d.col.Point();
			case AnyEffectToken (_):
				return getOffset(spriteData.no_effect_png);
			case AnyKindToken (_):
				return getOffset(spriteData.no_kind_png);
			case AnyMotionKindToken (_):
				return getOffset(spriteData.no_motion_png);
		}
	}
}
