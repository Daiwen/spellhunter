/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;
import spl.Lang;
import haxe.ds.Option;
import spl.Editor;
import spl.editor.Display.DropdownSelectorComp;


enum TokenType<T> {
	TEntityId : TokenType<EntityId>;
	TEffect : TokenType<Effect>;
	TKind : TokenType<Kind>;
	TMotionKind : TokenType<MotionKind>;
	TSpell : TokenType<Spell>;
}

enum AnyTokenType {
	AnyEntityIdToken (v : TokenType<EntityId>);
	AnyEffectToken (v : TokenType<Effect>);
	AnyKindToken (v : TokenType<Kind>);
	AnyMotionKindToken (v : TokenType<MotionKind>);
	AnySpellToken (v : TokenType<Spell>);
}

enum Token<T> {
	TCaster : Token<EntityId>;
	TMarked : Token<EntityId>;
	TTeleport : Token<Effect>;
	TPush : Token<Effect>;
	TFire : Token<Effect>;
	TIce : Token<Effect>;
	TSequence : Token<Effect>;
	TMeta : Token<Effect>;
	TMark : Token<Effect>;
	TLink : Token<Effect>;
	TActiver : Token<Kind>;
	TTrigger : Token<Kind>;
	TCollider : Token<Kind>;
	TProjectile : Token<MotionKind>;
	TGround : Token<MotionKind>;
	TEntity : Token<MotionKind>;
	TSpell : Token<Spell>;
}

typedef TokenData<T> = {
	var label : String;
	var description : String;
	var extract : (Array<DropdownSelectorComp>) -> Option<T>;
	var token : Token<T>;
	var subs : Array<AnyTokenType>;
}

enum AnyTokenData {
	AnyEntityIdData (v : TokenData<EntityId>);
	AnyEffectData (v : TokenData<Effect>);
	AnyKindData (v : TokenData<Kind>);
	AnyMotionKindData (v : TokenData<MotionKind>);
	AnySpellData (v : TokenData<Spell>);
	AnyNamedSpell (s : Spell);
}

class Util {
	static public function getLabel(d) {
		switch d {
			case (AnyEntityIdData ({ label : label })
			| AnyEffectData ({ label : label })
			| AnyKindData ({ label : label })
			| AnyMotionKindData ({ label : label })
			| AnySpellData ({ label : label })) : return label;
			case AnyNamedSpell (l) : return spl.Named.label(l);
		}
	}

	static public function getDescription(d) {
		switch d {
			case (AnyEntityIdData ({ description : description })
			| AnyEffectData ({ description : description })
			| AnyKindData ({ description : description })
			| AnyMotionKindData ({ description : description })
			| AnySpellData ({ description : description })) : return description;
			case AnyNamedSpell (l) : return spl.Named.description(l);
		}
	}

	static public function getGraphics(d, control) {
		var color = getColor(getTokenType(d));
		switch d {
			case AnyEntityIdData ({ token : token }) : return spl.Graphics.ofToken(token, color);
			case AnyEffectData ({ token : token }) : return spl.Graphics.ofToken(token, color);
			case AnyKindData ({ token : token }) : return spl.Graphics.ofToken(token, color);
			case AnyMotionKindData ({ token : token }) : return spl.Graphics.ofToken(token, color);
			case AnySpellData ({ token : token }) : return spl.Graphics.ofToken(token, color);
			case AnyNamedSpell (s) :
				var o = { state : Game.ME.spellState
						, control  : control
						, repl : Main.ME.repl
						, selector : None
						};
				var spell = spl.editor.Display.getSpellComponent(o, s, 0);
				return spell;
		}
	}


	static public function getTokenType(d) {
		switch d {
			case AnyEntityIdData (_) : return AnyEntityIdToken (TEntityId);
			case AnyEffectData (_) : return AnyEffectToken (TEffect);
			case AnyKindData (_) : return AnyKindToken (TKind);
			case AnyMotionKindData (_) : return AnyMotionKindToken (TMotionKind);
			case AnySpellData (_) : return AnySpellToken (TSpell);
			case AnyNamedSpell (_) : return AnySpellToken (TSpell);
		}
	}

	static public function compareAnyToken(at1, at2) {
		return
			switch [at1, at2] {
				case [AnyEntityIdToken (_), AnyEntityIdToken (_)]: true;
				case [AnyEffectToken (_), AnyEffectToken (_)]: true;
				case [AnyKindToken (_), AnyKindToken (_)]: true;
				case [AnyMotionKindToken (_), AnyMotionKindToken (_)]: true;
				case [AnySpellToken (_), AnySpellToken (_)]: true;
				case ([AnyEntityIdToken (_), _]
				| [AnyEffectToken (_), _]
				| [AnyKindToken (_), _]
				| [AnyMotionKindToken (_), _]
				| [AnySpellToken (_), _]): false;
			}
	}

	static public function getColor(t) {
		switch t {
			case AnyEntityIdToken (_) : return 0xb169ba;
			case AnyEffectToken (_) : return 0xaf7f66;
			case AnyKindToken (_) : return 0x7c9a51;
			case AnyMotionKindToken (_) : return 0x79acb9;
			case AnySpellToken (_) : return 0x6a59a8;
		}
	}

	static public function getRadius(t) {
		switch t {
			case AnyEntityIdToken (_) : return 100;
			case AnyEffectToken (_) : return 150;
			case AnyKindToken (_) : return 100;
			case AnyMotionKindToken (_) : return 100;
			case AnySpellToken (_) : return 250;
		}
	}
}

class LangToToken {
	static public function entityId(r : EntityId) {
		var extract = function (_) { return Some (r); };
		switch r {
			case Caster: return { label : "caster", description : "this is a caster", extract : extract, token : TCaster, subs : [] };
			case Marked:  return { label : "marked", description : "this is a marked", extract : extract, token : TMarked, subs : [] };
		}
	}

	static public function kind(k : Kind) {
		var extract = function (_) { return Some (k); };
		switch k {
			case Collider: return { label : "collider", description : "this is a collider", extract : extract, token : TCollider, subs : [] };
			case Trigger: return { label : "trigger", description : "this is a trigger", extract : extract, token : TTrigger, subs : [] };
			case Activer: return { label : "activer", description : "this is a activer", extract : extract, token : TActiver, subs : [] };
		}
	}

	static public function motionKind(mk : MotionKind) {
		var extract = function (subs : Array<DropdownSelectorComp>) {
			switch mk {
				case Projectile | Attached(Ground): { return Some (mk); };
				case Attached(Entity (_)):
					var val =
						switch [ subs[0].extract(TEntityId) ] {
							case [ Some(ei) ]: Some (Attached(Entity (ei)));
							default: None;
					}
					return val;
			}
		};
		switch mk {
			case Projectile: return { label : "projectile", description : "this is a projectile", extract : extract, token : TProjectile, subs : [] };
			case Attached(Ground): return { label : "ground", description : "this is a ground", extract : extract, token : TGround, subs : [] };
			case Attached(Entity(_)): return { label : "entity", description : "this is an entity", extract : extract, token : TEntity, subs : [ AnyEntityIdToken (TEntityId) ] };
		}
	}

	static public function effect(e : Effect) {
		var extract = function (subs : Array<DropdownSelectorComp>) {
			switch e {
				case Physical(_) | Elemental(_): return Some (e);
				case Sequence(_):
					var val =
						switch [ subs[0].extract(TEffect), subs[1].extract(TEffect) ] {
							case [Some(e1), Some(e2)]: Some (Sequence(e1, e2));
						default: None;
					}
					return val;
				case Meta(_):
					var val =
						switch [ subs[0].extract(TSpell) ] {
							case [Some(s)]: Some (Meta(s));
						default: None;
					}
					return val;
				case Mark:
					return Some (Mark);
				case Link(_):
					var val =
						switch [ subs[0].extract(TEntityId) ] {
							case [Some(ei)]: Some (Link(ei));
						default: None;
					}
					return val;
			}
		};
		switch e {
			case Elemental(Ice): return { label : "ice", description : "this is a ice", extract : extract, token : TIce, subs : [] };
			case Elemental(Fire): return { label : "fire", description : "this is a fire", extract : extract, token : TFire, subs : [] };
			case Physical(Teleport): return { label : "teleport", description : "this is a teleport", extract : extract, token : TTeleport, subs : [] };
			case Physical(Push): return { label : "push", description : "this is a push", extract : extract, token : TPush, subs : [] };
			case Sequence(_): return { label : "sequence", description : "this is a sequence", extract : extract, token : TSequence, subs : [ AnyEffectToken (TEffect), AnyEffectToken (TEffect) ] };
			case Meta(_): return { label : "meta", description : "this is a meta", extract : extract, token : TMeta, subs : [ AnySpellToken (TSpell) ] };
			case Mark: return { label : "mark", description : "this is a mark", extract : extract, token : TMark, subs : [] };
			case Link(_): return { label : "link", description : "this is a link", extract : extract, token : TLink, subs : [ AnyEntityIdToken (TEntityId) ] };
		}
	}

	static public function spell(s : Spell) {
		var extract = function (subs : Array<DropdownSelectorComp>) {
			switch s {
				case Spell(k, e, mk):
					var val =
						switch [ subs[0].extract(TKind), subs[1].extract(TEffect), subs[2].extract(TMotionKind) ] {
							case [ Some (k), Some(e), Some(mk) ]: Some (Spell(k, e, mk));
							default: None;
					}
					return val;
			}
		};
		switch s {
			case Spell(_): return { label : "spell", description : "this is a spell", extract : extract, token : TSpell, subs : [ AnyKindToken (TKind), AnyEffectToken (TEffect), AnyMotionKindToken (TMotionKind) ] };
		}
	}
}

class LangToSprite {
	static public function addSpellSprite(sprite : h2d.SpriteBatch, s : Spell, ?x = 0., ?y = 0.) {
		switch (s) {
			case Spell (_, e, _): getEffectSprite(sprite, e, x, y);
		}
		var l = LangToToken.spell(s);
		var t = Assets.lang.getTile(l.label);
		var e = new h2d.SpriteBatch.BatchElement(t);
		e.x = x;
		e.y = y;
		sprite.add(e);
		return sprite;
	}

	static public function getEffectSprite(sprite : h2d.SpriteBatch, e : Effect, ?x = 0., ?y = 0.) {
		var getEffectTile = function (e) {
			  var l = LangToToken.effect(e);
			  return Assets.lang.getTile(l.label);
		};
		var t = getEffectTile(e);
		var e = new h2d.SpriteBatch.BatchElement(t);
		e.x = x;
		e.y = y;
		sprite.add(e);
	}
}
