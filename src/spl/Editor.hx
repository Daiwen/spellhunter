/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;
import ui.Components;
import spl.Token;
import haxe.ds.Option;

import spl.editor.Spellbook.SpellbookComp;

import spl.editor.Display;
import spl.editor.Display.Options;
import spl.editor.Display.DropdownSelectorComp;

class Editor extends dn.Process {
	public static var ME : Editor;

	var control : CustomControl = null;
	var mask : h2d.Bitmap;

	var altar : spl.editor.Altar;
	var spellbook : spl.editor.Spellbook;
	var selector : spl.editor.Selector;

	public function new() {
		super(Main.ME);
		ME = this;
		
		createRoot(Boot.ME.uiRoot);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering

		var imgRoot = new h2d.Object();
		root.add(imgRoot, 1);

		control = new CustomControl();

		altar = new spl.editor.Altar(this, imgRoot, control);
		altar.onClose = close;
		altar.pause();

		spellbook = new spl.editor.Spellbook(this, imgRoot, control);
		spellbook.pause();

		selector = new spl.editor.Selector(this, imgRoot, control);
		selector.shiftObject = this.altar.spellHook;
		selector.pause();

		mask = new h2d.Bitmap(h2d.Tile.fromColor(0x0, 1, 1, 0.6), root);
		root.under(mask);

		dn.Process.resizeAll();

		pause();
	}

	override function resume(){
		super.resume();
		Boot.ME.uiRoot.addChild(this.root);
	}

	override function onResize() {
		super.onResize();

		mask.scaleX = M.ceil(stageWid);
		mask.scaleY = M.ceil(stageHei);
	}

	public function close() {
		Game.ME.hud.invalidate();
		Game.ME.resume();
		Game.ME.spellState.newWords.clear();

		for (c in Game.ME.spellCaster) {
			c.refresh();
		}

		dn.heaps.slib.SpriteLib.DISABLE_ANIM_UPDATES = false;

		pause();
	}

	function open (state : spl.State) {
		dn.heaps.slib.SpriteLib.DISABLE_ANIM_UPDATES = true;

		Game.ME.pause();
		tools.CustomControl.suspend(0.25);

		resume();
	}

	public function openSpellbook(state : spl.State) {
		open(state);

		var label = switch (Game.ME.spellCaster[0].formula) {
			case Some (f): Some (spl.Named.label(f));
			case None : None;
		}

		var idx : Option<Int> = None;
		var choices = new Array();
		for (td in state.allWords.keyValueIterator()) {
			switch (label) {
				case Some (l) if (l == Util.getLabel(td.value)): idx = Some (choices.length);
				default:
			}
			var td = td.value;

			var t = Util.getTokenType(td);
			if (Util.compareAnyToken(t, spl.Token.AnyTokenType.AnySpellToken (TSpell))) {
				var w = new spl.editor.Word(td, state);
				choices.push(w);
			}
		}

		spellbook.onClose = () -> {
			close();
			altar.setSpell(Game.ME.spellCaster[0].formula, selector);
		};

		if (choices.length > 0) {
			spellbook.open(choices, idx);
		} else {
			close();
		}
	}

	public function openAltar(state : spl.State) {
		open(state);

		if (null == altar.spell)
			altar.setSpell(Game.ME.spellCaster[0].formula, selector);

		spellbook.onOpen = function () {
			this.onResize();
			altar.pause();
			altar.hide();
		}
		spellbook.onClose = function () {
			altar.resume();
			altar.show();
		};

		selector.onOpen = function () {
			this.onResize();
			altar.pause();
		}
		selector.onClose = function () {
			altar.resume();
		};

		altar.resume();
		altar.show();
		altar.open();
	}

	override function pause() {
		super.pause();

		root.remove();
	}
}
