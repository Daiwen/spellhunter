/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;
import haxe.ds.Option;

class Caster {
	var spells : Array<ent.Spell>;
	private var canCast = true;
	private var i : Int;
	public var formula(get, never) : Option<spl.Lang.Spell>;
	public var isLocked = false;

	inline function get_formula() return Game.ME.spellState.slots[i];

	public function new(i) {
		spells = new Array();
		this.i = i;

		refresh();
	}

	public function refresh() {
		castSpell = Factory.buildSpell(formula);
	}

	public function trigger(x : Float, y : Float) {
		if (isLocked)
			return;

		for (s in spells.copy())
			if (!s.isDead)
				s.touch();

		if (canCast) {
			castSpell(x, y, this);
			Game.ME.hero.castProjectile();
		}
	}

	dynamic private function castSpell(x : Float, y : Float, c : Caster) : Void {
	}

	public function addSpell(s : ent.Spell) {
		spells.push(s);
	}

	public function removeSpell(s : ent.Spell) {
		spells.remove(s);

		if (spells.length == 0)
			canCast = true;
	}

	public function purge() {
		canCast = true;

		for (s in spells)
			s.destroy();
	}

	public function stopCast() {
		canCast = false;
	}

	public function log() {
		switch (formula) {
			case None: Game.ME.repl.log("no spell equipped");
			case Some (s): Game.ME.repl.log(Lang.Print.spell(s));
		}	
	}
}
