/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;

enum EntityId {
	Caster;
	Marked;
}

enum Element {
	Fire;
	Ice;
}

enum Physics {
	Push;
	Teleport;
}

enum Effect {
	Elemental(e : Element);
	Physical(p : Physics);
	Sequence(e1 : Effect, e2 : Effect);
	Meta(s : Spell);
	Mark;
	Link(e : EntityId);
}

enum Kind {
	Collider;
	Trigger;
	Activer;
}

enum Anchor {
	Ground;
	Entity(r : EntityId);
}

enum MotionKind {
	Projectile;
	Attached(a : Anchor);
}

enum Spell {
	Spell(kind : Kind, effect : Effect,	motion : MotionKind);
}

class Default {
	static public function spell() {
		var k = kind();
		var e = effect();
		var m = motionKind();

		return Spell (k, e, m);
	}

	static public function kind() {
		return Collider;
	}

	static public function element() {
		return Fire;
	}

	static public function anchor() {
		return Ground;
	}

	static public function motionKind() {
		return Projectile;
	}

	static public function effect() {
		var e = element();

		return Elemental(e);
	}

	static public function physics() {
		return Push;
	}

	static public function entityId() {
		return Caster;
	}
}

class Print {
	static public function physics(p : Physics) {
		switch p {
			case Teleport: return "teleport";
			case Push: return "push";
		}
	}

	static public function entityId(r : EntityId) {
		switch r {
			case Caster: return "caster";
			case Marked: return "marked";
		}
	}

	static public function effect(e : Effect) {
		switch e {
			case Elemental(e) : return "elemental(" + element(e) + ")";
			case Physical(p) : return "physical(" + physics(p) + ")";
			case Sequence(e1, e2) : return "sequence(" + effect(e1) + ", " + effect(e2) + ")";
			case Meta(s) : return "meta(" + spell(s) + ")";
			case Mark : return "mark";
			case Link(ei) : return "link(" + entityId(ei) + ")";
		}
	}

	static public function anchor(a : Anchor) {
		switch a {
			case Ground : return "ground";
			case Entity(ei) : return "entity(" + entityId(ei) + ")";
		}
	}

	static public function motionKind(mk : MotionKind) {
		switch mk {
			case Projectile : return "projectile";
			case Attached(a) : return "attached(" + anchor(a) + ")";
		}
	}

	static public function kind(k : Kind) {
		switch k {
			case Collider : return "collider";
			case Trigger : return "trigger";
			case Activer : return "activer";
		}
	}

	static public function element(e : Element) {
		switch e {
			case Fire : return "fire";
			case Ice : return "ice";
		}
	}

	static public function spell(s : Spell) {
		switch s {
			case Spell(k, e, mk) : return "spell(" + kind(k) + ", " + effect(e) + ", " + motionKind(mk) + ")";
		}
	}
}

