/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;

import spl.Token.AnyTokenData;
import haxe.ds.Option;

typedef WordData = {
	var name : String;
	var description : String;
}

class State {
	public var allWords : Map<String, AnyTokenData>;
	public var newWords : Map<String, Bool>;
	public var slots : Array<Option<spl.Lang.Spell>>;
	public var book : Map<spl.Lang.Spell, Bool>;
	public var wordData : Map<String, WordData>;

	public function new() {
		allWords = new Map();
		newWords = new Map();
		slots = new Array();
		book = new Map();
		wordData = new Map();
	}
}
