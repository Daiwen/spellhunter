/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl;

import spl.Lang;

class Named {
	static public function label(s) {
		var sp = switch s {
			case Spell (Collider, Elemental (Fire), Projectile): "Fire Bolt";
			case Spell (Collider, Elemental (Fire), Attached (Ground)): "Fire Mine";
			case Spell (Collider, Physical (Push), Projectile): "Force Push";
			case Spell (Collider, Physical (Teleport), Projectile): "Teleport";
			case Spell (Collider, Meta (Spell (Collider, Elemental (Fire), Projectile)), Projectile): "Bouncing Fire";
			case Spell (Collider, Elemental (Ice), Projectile): "Ice Bolt";
			case Spell (Trigger, Elemental (Ice), Attached (Ground)): "Freeze Bomb";
			case Spell (Activer, Elemental (Ice), Attached (Entity (Caster))): "Ice Shield";
			case Spell (Collider, Sequence (Elemental (Ice), Physical (Push)), Projectile): "Icy Push";
			case Spell (Activer, Elemental (Fire), Attached (Entity (Marked))): "Marked Fire";
			case Spell (Collider, Sequence (Mark, Meta (Spell (Trigger, Physical (Teleport), Attached (Entity (Marked))))), Projectile): "Recall from afar";
			case Spell (Collider, Link (Caster), Projectile): "Link";
			default: Print.spell(s);
		};

		return sp;
	}

	static public function description(s) {
		var sp = switch s {
			case Spell (Collider, Elemental (Fire), Projectile): "Fire Bolt";
			case Spell (Collider, Elemental (Fire), Attached (Ground)): "Fire Mine";
			case Spell (Collider, Physical (Push), Projectile): "Force Push";
			case Spell (Collider, Physical (Teleport), Projectile): "Teleport";
			case Spell (Collider, Meta (Spell (Collider, Elemental (Fire), Projectile)), Projectile): "Bouncing Fire";
			case Spell (Collider, Elemental (Ice), Projectile): "Ice Bolt";
			case Spell (Trigger, Elemental (Ice), Attached (Ground)): "Freeze Bomb";
			case Spell (Activer, Elemental (Ice), Attached (Entity (Caster))): "Ice Shield";
			case Spell (Collider, Sequence (Elemental (Ice), Physical (Push)), Projectile): "Icy Push";
			case Spell (Activer, Elemental (Fire), Attached (Entity (Marked))): "Marked Fire";
			case Spell (Collider, Sequence (Mark, Meta (Spell (Trigger, Physical (Teleport), Attached (Entity (Marked))))), Projectile): "Recall from afar";
			case Spell (Collider, Link (Caster), Projectile): "Link";
			default: Print.spell(s);
		};

		return sp;
	}
}
