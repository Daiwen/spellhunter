/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl.editor;
import haxe.ds.Option;
import ui.Components;
import spl.Token;

typedef SpellbookOptions = {
	var close : () -> Void;
	var control : tools.CustomControl;
}

class SpellbookWrapperComp extends ButtonComp {
	private function getTiles(i : Word) {
		var w = Std.int(40 * Const.UI_SCALE);
		var h = Std.int(16 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Bookmark)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.BookmarkSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.BookmarkSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	public function new(item : Word, ?parent ) {
		super(getTiles(item), parent);

		setVerticalMargin(10);

		label = item.name;

		initComponent();
	}
}

@:uiComp("spellbook")
class SpellbookComp extends h2d.Object implements h2d.domkit.Object {
	static var SRC = <spellbook>
			<container id="words" />
			<button(tiles) id="closeButton" />
			<anchor id="display" />
			<text-field(o.control, nameTiles) id="wordName" />
			<text-field(o.control, descrTiles) id="description" />
		</spellbook>;

	private var width : Float = 0;
	private var height : Float = 0;

	private function getTextFieldTiles(width : Float, height : Float) {
		var w = Std.int(width * Const.UI_SCALE);
		var h = Std.int(height * Const.UI_SCALE);

		var bTile = h2d.Tile.fromColor(0xbdb781, w, h, 1);
		var baseTile = new h2d.SpriteBatch(bTile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(bTile));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xa85959, w, h, 1)));
		var aTile = h2d.Tile.fromColor(0xaf7f66, w, h, 1);
		var activeTile = new h2d.SpriteBatch(aTile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(aTile));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	private function getExitTiles() {
		var w = Std.int(32 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookCloseNotSelected)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookCloseSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookCloseSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	var bookmarks(default, null) : h2d.Object;
	var options : SpellbookOptions;
	var menu : tools.MenuControl.GridMenuControl;

	dynamic public function onPush(i : Int) {
	}

	static public function getNameTileSize() {
		return new h2d.col.Point(50 * Const.UI_SCALE, 10 * Const.UI_SCALE);
	}

	static public function getDescrTileSize() {
		return new h2d.col.Point(200 * Const.UI_SCALE, 200 * Const.UI_SCALE);
	}

	public function new(o : SpellbookOptions, ?parent) {
		super(parent);

		this.menu = new tools.MenuControl.GridMenuControl(o.control, "Spellbook");
		this.menu.repl = Main.ME.repl;

		var tiles = getExitTiles();
		var nameTileSize = getNameTileSize();
		var nameTiles = getTextFieldTiles(nameTileSize.x, nameTileSize.y);
		var descrTileSize = getDescrTileSize();
		var descrTiles = getTextFieldTiles(descrTileSize.x, descrTileSize.y);

		initComponent();

		this.closeButton.getName = () -> "Back";
		this.closeButton.onClick = o.close;
		this.closeButton.onOver = () -> {
			ChucK.broadcastEvent("xbutton2");
		};

		menu.addItem(1, this.closeButton);
		menu.addItem(1, this.wordName);
		menu.addItem(1, this.description);

		this.bookmarks = new h2d.Object(this.words);

		this.wordName.align = h2d.Text.Align.Center;
		this.wordName.onOver = () -> {
			ChucK.broadcastEvent("xbutton2");
		};

		this.options = o;

		this.description.maxWidth = descrTileSize.x;
		this.description.onOver = () -> {
			ChucK.broadcastEvent("xbutton2");
		};
	}

	public function close() {
		this.bookmarks.removeChildren();
		this.display.removeChildren();
		this.wordName.clear();
		this.description.clear();

		menu.clear(0);
		menu.resetSelected();
	}

	private function wrapItem(i: Int, item : Word, parent) {
			var wrapper = new SpellbookWrapperComp(item, parent);

			wrapper.onOver = function() {
				display.removeChildren();

				ChucK.broadcastEvent("xbutton1");

				this.wordName.text = item.name;
				this.description.text = item.description;
				this.wordName.onFocusLost = (v) -> {
					item.name = v;
					wrapper.label = item.name;
				};
				this.description.onFocusLost = (v) -> {
					item.description = v;
				};

				var g = Util.getGraphics(item.tdata, menu.control);
				if (null != g) {
					display.addChild(g);
					return;
				}

				switch (item.tdata) {
					default:
						var lt = new h2d.SpriteBatch(Assets.lang.tile, display);
						var l = item.defaultName;
						lt.add(new h2d.SpriteBatch.BatchElement(Assets.lang.getTile(l)));
				}
			}

			wrapper.onClick = function() {
				options.close();
				item.onSelect(i);
			};

			wrapper.getName = function() {
				return item.name;
			};

			menu.addItem(0, wrapper);

			return wrapper;
	}

	public function open(items : Array<Word>, idx : Option<Int>) {
		for (i in items.keyValueIterator()) {
			var wrapper = wrapItem(i.key, i.value, this.bookmarks);
			var b = wrapper.getBounds();
			wrapper.x = -b.width;
			wrapper.y = i.key * (wrapper.height + wrapper.marginUp + wrapper.marginDown) ;

			switch (idx) {
				case Some (idx) if (i.key == idx): menu.select(wrapper);
				default:
			}
		}

		menu.over();
	}

	public function update() {
		menu.update();
	}

	public function resize(x, y, w, h) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;

		this.closeButton.x = width - 1.5 * width/10;
		this.closeButton.y = height/10;

		this.display.x = width/4;
		this.display.y = height/2;

		var nameTileSize = getNameTileSize();
		this.wordName.resize(nameTileSize.x, nameTileSize.y);
		var b = this.wordName.getBounds();
		this.wordName.x = 5*width/7 - b.width/2;
		this.wordName.y = height/5;

		var descrTileSize = getDescrTileSize();
		this.description.resize(descrTileSize.x, descrTileSize.y);
		this.description.maxWidth = descrTileSize.x;
		var b = this.description.getBounds();
		this.description.x = 5*width/7 - b.width/2;
		this.description.y = height/4;

		var t = getExitTiles();
		this.closeButton.resize(t.width, t.height);

		this.words.x = w*0.1;
		this.words.y = h*0.1;
	}
}

class Spellbook extends dn.Process {
	var img : h2d.Bitmap;
	var comp(default, null) : SpellbookComp;
	var control : tools.CustomControl;

	public function new(p : dn.Process, imgRoot, control) {
		super(p);

		this.control = control;

		createRoot(p.root);

        img = new h2d.Bitmap(hxd.Res.spellbook.toAseprite().toTile());
        root.addChildAt(img, 0);

		var spellbookOptions =
		    { close : close
			, control : control
			};

		comp = new SpellbookComp(spellbookOptions, root);
	}


	override function update() {
		super.update();

		comp.update();

		if( control.probe(Exit) ) {
			tools.CustomControl.suspend(0.3);
			close();
		}
	}


	override function onResize() {
		super.onResize();

		var scale = Const.UI_SCALE;
		img.x = stageWid*0.5 - img.tile.width*scale*0.5;
		img.y = stageHei*0.5 - img.tile.height*scale*0.5;
		img.scaleX = scale;
		img.scaleY = scale;

		comp.resize(img.x, img.y, img.tile.width*scale, img.tile.height*scale);
	}


	override function resume() {
		super.resume();
		root.visible = true;
		onResize();
	}

	override function pause() {
		super.pause();
		root.visible = false;
	}

	dynamic public function onOpen() {
	}

	public function open(items : Array<Word>, i : Option<Int>) {
		tools.CustomControl.suspend(0.3);
		ChucK.broadcastEvent("book");
		resume();
		onOpen();
		comp.open(items, i);
	}

	dynamic public function onClose() {
	}

	public function close() {
		tools.CustomControl.suspend(0.3);
		pause();
		comp.close();
		onClose();
	}
}
