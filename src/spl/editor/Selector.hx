/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl.editor;
import haxe.ds.Option;
import ui.Components;
import spl.Token;

typedef SelectorOptions = {
	var close : () -> Void;
	var control : tools.CustomControl;
}

class SelectorWrapperComp extends ButtonComp {
	private function getTiles(i : Word) {
		var w = Std.int(40 * Const.UI_SCALE);
		var h = Std.int(16 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.Bookmark)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.BookmarkSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.BookmarkSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	public function new(item : Word, ?parent ) {
		super(getTiles(item), parent);

		setVerticalMargin(10);

		label = item.name;

		initComponent();
	}
}

@:uiComp("selector")
class SelectorComp extends h2d.Object implements h2d.domkit.Object {
	static var SRC = <selector>
			<container id="words" />
			<text-field(o.control, nameTiles) id="wordName" />
			<text-field(o.control, descrTiles) id="description" />
		</selector>;

	private var width : Float = 0;
	private var height : Float = 0;

	private var display = new h2d.Object();
	var isLeft : Bool;

	private function getTextFieldTiles(width : Float, height : Float) {
		var w = Std.int(width * Const.UI_SCALE);
		var h = Std.int(height * Const.UI_SCALE);

		var bTile = h2d.Tile.fromColor(0xbdb781, w, h, 1);
		var baseTile = new h2d.SpriteBatch(bTile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(bTile));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xa85959, w, h, 1)));
		var aTile = h2d.Tile.fromColor(0xaf7f66, w, h, 1);
		var activeTile = new h2d.SpriteBatch(aTile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(aTile));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	private function getExitTiles() {
		var w = Std.int(32 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookCloseNotSelected)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookCloseSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.SpellbookCloseSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	var bookmarks(default, null) : h2d.Object;
	var options : SelectorOptions;
	var menu : tools.MenuControl.GridMenuControl;

	dynamic public function onPush(i : Int) {
	}

	public function new(o : SelectorOptions, ?parent) {
		super(parent);

		this.menu = new tools.MenuControl.GridMenuControl(o.control, "Spellbook");
		this.menu.repl = Main.ME.repl;

		var tiles = getExitTiles();
		var nameTileSize = spl.editor.Spellbook.SpellbookComp.getNameTileSize();
		var nameTiles = getTextFieldTiles(nameTileSize.x, nameTileSize.y);
		var descrTileSize = spl.editor.Spellbook.SpellbookComp.getDescrTileSize();
		var descrTiles = getTextFieldTiles(descrTileSize.x, descrTileSize.y);

		initComponent();

		menu.addItem(1, this.wordName);
		menu.addItem(1, this.description);

		this.bookmarks = new h2d.Object(this.words);

		this.wordName.align = h2d.Text.Align.Center;

		this.options = o;

		this.description.maxWidth = descrTileSize.x;
	}

	public function close() {
		this.bookmarks.removeChildren();
		this.wordName.clear();
		this.description.clear();

		menu.clear(0);
		menu.resetSelected();
	}

	private function wrapItem(i: Int, item : Word, parent) {
			var wrapper = new SelectorWrapperComp(item, parent);

			wrapper.onOver = function() {
				item.onSelect(i);
				display.removeChildren();

				this.wordName.text = item.name;
				this.description.text = item.description;
				this.wordName.onFocusLost = (v) -> {
					item.name = v;
					wrapper.label = item.name;
				};
				this.description.onFocusLost = (v) -> {
					item.description = v;
				};

				var g = Util.getGraphics(item.tdata, menu.control);
				if (null != g) {
					display.addChild(g);
					return;
				}

				switch (item.tdata) {
					default:
						var lt = new h2d.SpriteBatch(Assets.lang.tile, display);
						var l = item.defaultName;
						lt.add(new h2d.SpriteBatch.BatchElement(Assets.lang.getTile(l)));
				}
			}

			wrapper.onClick = function() {
				options.close();
				item.onSelect(i);
			};

			wrapper.getName = function() {
				return item.name;
			};

			menu.addItem(0, wrapper);

			return wrapper;
	}

	public function open(t : spl.Token.AnyTokenType, items : Array<Word>, display : h2d.Object, isLeft : Bool, idx : Option<Int>) {
		this.display = display;
		this.isLeft = isLeft;

		var nb = items.length;
		var radius = Util.getRadius(t);

		for (i in items.keyValueIterator()) {
			var wrapper = wrapItem(i.key, i.value, this.bookmarks);
			var b = wrapper.getBounds();
			wrapper.x = radius * Math.cos(i.key * 2 * Math.PI / nb) - b.width/2;
			wrapper.y = radius * Math.sin(i.key * 2 * Math.PI / nb) - b.height/2;

			switch (idx) {
				case Some (idx) if (i.key == idx): menu.select(wrapper);
				default:
			}
		}

		menu.over();
	}

	public function update() {
		menu.update();
	}

	public function resize(x, y, w, h) {
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;

		var p = display.localToGlobal();
		this.words.x = p.x - this.x;
		this.words.y = p.y - this.y;

		var nameTileSize = spl.editor.Spellbook.SpellbookComp.getNameTileSize();
		this.wordName.resize(nameTileSize.x, nameTileSize.y);
		var b = this.wordName.getBounds();
		this.wordName.x =
			if (isLeft)
				2*width/7 - b.width/2;
			else
				5*width/7 - b.width/2;
		this.wordName.y = height/5;

		var descrTileSize = spl.editor.Spellbook.SpellbookComp.getDescrTileSize();
		this.description.resize(descrTileSize.x, descrTileSize.y);
		var b = this.description.getBounds();
		this.description.x =
			if (isLeft)
				2*width/7 - b.width/2;
			else
				5*width/7 - b.width/2;
		this.description.y = height/4;
	}
}

class Selector extends dn.Process {
	var img : h2d.Bitmap;
	var comp(default, null) : SelectorComp;
	var control : tools.CustomControl;

	var index : Option<Int>;
	var items : Array<Word>;

	public function new(p : dn.Process, imgRoot, control) {
		super(p);

		this.control = control;

		createRoot(p.root);

        img = new h2d.Bitmap(hxd.Res.spellbook.toAseprite().toTile());
        root.addChildAt(img, 0);

		var closeScreen = new ui.Components.ButtonComp(getTiles());
		closeScreen.onClick = close;
        root.addChildAt(closeScreen, 1);

		var spellbookOptions =
		    { close : close
			, control : control
			};

		comp = new SelectorComp(spellbookOptions);
        root.addChildAt(comp, 2);
	}



	override function update() {
		super.update();

		comp.update();

		if( control.probe(Exit) ) {
			tools.CustomControl.suspend(0.3);

			switch(index) {
				case Some (i) : items[i].onSelect(i);
				case None :
			}

			close();
		}
	}


	override function onResize() {
		super.onResize();

		if(currentParent == null)
			return;

		var scale = dn.heaps.Scaler.bestFit_i(480,270);
		img.scaleX = scale;
		img.scaleY = scale;
		if (isLeft) {
			img.x = stageWid - img.tile.width * scale * 0.5;
		} else {
			img.x = -img.tile.width * scale * 0.5;
		}
		img.y = stageHei*0.5 - img.tile.height*scale*0.5;

		comp.resize(img.x, img.y, img.tile.width*scale, img.tile.height*scale);
	}


	override function resume() {
		super.resume();
		root.visible = true;
		onResize();
	}

	override function pause() {
		super.pause();
		root.visible = false;
	}

	dynamic public function onOpen() {
	}

	public var shiftObject : h2d.Object;

	public function updateShift(t : spl.Token.AnyTokenType, base : h2d.col.Point) {
		var radius = Util.getRadius(t);
		var marginX = 30;
		var marginY = 15;

		var left = base.x - radius - marginX;
		var right = base.x + radius + marginX;
		var up = base.y - radius - marginY;
		var down = base.y + radius + marginY;

		shiftObject.x -=
			if (isLeft) {
				if (left < 0) left else if (right > stageWid/2) right - stageWid/2 else 0;
			} else {
				if (left < stageWid/2) left - stageWid/2 else if (right > stageWid) right - stageWid else 0;
			}
		shiftObject.y -= if (up < 0) up else if (down > stageHei) down - stageHei else 0;
	}

	var currentParent : h2d.Object;
	var current : h2d.Object;
	var isLeft : Bool;

	public function open(items : Array<Word>, display : spl.editor.Display.WordComp, i : Option<Int>) {
		current = display;
		currentParent = current.parent;
		current.remove();

		index = i;
		this.items = items;

		var t = switch (i) {
			case Some (i) : Util.getTokenType(items[i].tdata);
			case None : Util.getTokenType(items[0].tdata);
		}

		var p = currentParent.localToGlobal();
		isLeft = p.x < stageWid*0.5;
		updateShift(t, p);
		tools.CustomControl.suspend(0.3);
		onOpen();
		comp.open(t, items, currentParent, isLeft, i);
		resume();
	}

	dynamic public function onClose() {
	}

	public function close() {
		currentParent.removeChildren();
		currentParent.addChild(current);
		tools.CustomControl.suspend(0.3);
		pause();
		comp.close();
		onClose();
	}

	private function getTiles() {
		var w = stageWid;
		var h = stageHei;

		var bTile = h2d.Tile.fromColor(0xffffff, w, h, 0.);
		var baseTile = new h2d.SpriteBatch(bTile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(bTile));
		var hoverTile = new Array();
		var activeTile = new h2d.SpriteBatch(bTile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(bTile));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}
}
