/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl.editor;
import ui.Components;
import spl.Lang;
import spl.Token;
import haxe.ds.Option;

import spl.editor.Spellbook.SpellbookComp;
import spl.editor.Display;
import spl.editor.Display.DropdownSelectorComp;

#if( js )
import js.Browser.document;
#end

typedef SpellSlotOptions = {
	var edit : (SpellSlotComp, Int) -> Void;
	var currentSlotId : Int;
	var setCurrentSlot : (SpellSlotComp) -> Void;
	var close : () -> Void;
	var control : tools.MenuControl.VerticalMenuControl;
	var repl : tools.Hake;
	var addSpellTo : (h2d.Scene) -> Bool;
}

class SpellSlotComp extends ButtonComp {
	var id : Int;

	var isActive : Bool;

	public function new(o : SpellSlotOptions, id : Int, ?parent) {
		super(ui.Hud.SpellSlotUiComp.getTiles(id), parent);

		label = "spell " + (id + 1);

		this.id = id;

		initComponent();

		if (id == o.currentSlotId) {
			setActive();
			o.setCurrentSlot(this);
		}

		onOut = function() {
			if (isActive)
				showActive();
			else
				showBase();
		};


		onClick = function() {
			o.edit(this, id);
			logMenu();
		};
	}

	public function setActive() {
		dom.active = true;
		isActive = true;
		showActive();
		onSetActive();
	}

	dynamic public function onSetActive() {
	}

	public function unsetActive() {
		dom.active = false;
		isActive = false;
		showBase();
	}

	dynamic public function logMenu() {
	}
}

@:uiComp("slot-container")
class SlotContainerComp extends h2d.Flow implements h2d.domkit.Object implements tools.MenuControl.MenuItem {
	static var SRC = <slot-container>
		</slot-container>;

	var menu : tools.MenuControl;
	var parentMenu : tools.MenuControl.Menu;
	var options : SpellSlotOptions;

	var slots : Array<SpellSlotComp>;

	public function redraw() {
		this.removeChildren();
		this.menu.clear();
		this.slots = new Array();

		for (i in 0...Game.ME.spellCaster.length)
			if (!Game.ME.spellCaster[i].isLocked) {
				var s = new SpellSlotComp(options, i, this);
				s.onSetActive = function () {
					options.currentSlotId = i;
				};
				s.logMenu = logSelect;
				s.onOver = function () {
					ChucK.broadcastEvent("xbutton2");
					parentMenu.select(this);
				}
				menu.addItem(s);

				this.slots.push(s);
			}
	}

	public function new(o : SpellSlotOptions, ?parent) {
		super(parent);

		initComponent();
		options = o;

		menu = new tools.MenuControl.HorizontalMenuControl(o.control.control, "");
		menu.repl = o.repl;

		slots = new Array();

		redraw();

		this.layout = Horizontal;
	}

	public function over() {
		menu.over();
	}

	public function out() {
		menu.out();
	}

	public function click() {
		menu.click();
	}

	public function logSelect() {
		options.repl.log("Spell " + (options.currentSlotId + 1) + " is currently selected.");

		var back = {
			label : "Back",
			select : function() {
				onBack();
			},
		};

		menu.show([back]);
	}

	public function unroll() {
		menu.next();
	}

	public function register(m : tools.MenuControl.Menu) {
		parentMenu = m;
	}

	public function rollBack() {
		menu.back();
	}

	dynamic public function getName() {
		return "Slots";
	}

	dynamic public function onBack() {
	}

	public function resize() {
		var t = ui.Hud.SpellSlotUiComp.getTiles(0);

		for (s in slots) {
			s.resize(t.width, t.height);
		}
	}
}

@:uiComp("dropdown-container")
class DropdownContainerComp extends h2d.Object implements h2d.domkit.Object {
	var spell : DropdownSelectorComp;
	var startPoint : h2d.col.Point = null;

	var interactive : h2d.Interactive;

	var options : SpellSlotOptions;

	var slotContainer : SlotContainerComp;

	private var width : Float = 0;
	private var height : Float = 0;

	static var SRC = <dropdown-container>
			<container id = "slots" />
			<button(dTiles) id = "download" />
			<button(eTiles) id = "exit" />
		</dropdown-container>;

	private function getExitTiles() {
		var w = Std.int(32 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.AltarNotSelected)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.AltarSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.AltarSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	private function getDownloadTiles() {
		var w = Std.int(32 * Const.UI_SCALE);
		var h = Std.int(32 * Const.UI_SCALE);

		var baseTile = new h2d.SpriteBatch(Assets.buttons.tile);
		baseTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.DownloadNotSelected)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.DownloadSelected)));
		var activeTile = new h2d.SpriteBatch(Assets.buttons.tile);
		activeTile.add(new h2d.SpriteBatch.BatchElement(Assets.buttons.getTile(Assets.buttonsDict.DownloadSelected)));

		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	public function new(o : SpellSlotOptions, ?parent) {
		super(parent);

		var eTiles = getExitTiles();
		var dTiles = getDownloadTiles();

		options = o;

		initComponent();

		this.interactive =  new h2d.Interactive(width, height, this);

		slotContainer = new SlotContainerComp(o, this.slots);
		slotContainer.onBack = options.control.logSelect;

		this.interactive.propagateEvents = true;
		this.interactive.onPush = function(e) {
			startPoint = new h2d.col.Point(e.relX, e.relY);
		};

		this.interactive.onMove = function(e) {
			if (startPoint == null)
				return;

			spell.parent.x += e.relX - startPoint.x;
			spell.parent.y += e.relY - startPoint.y;

			startPoint = new h2d.col.Point(e.relX, e.relY);
		};

		this.interactive.onRelease = function(e) {
			if (startPoint == null)
				return;

			spell.parent.x += e.relX - startPoint.x;
			spell.parent.y += e.relY - startPoint.y;

			startPoint = null;
		};

		this.download.onClick = function (){
			var scene = new h2d.Scene();
			var isSuccess = options.addSpellTo(scene);
			if (!isSuccess)
				return;
			var render = scene.captureBitmap();
			var pixels = render.tile.getTexture().capturePixels();
			var png = pixels.toPNG();
#if( js )
			var blob = new js.html.Blob([png.getData()]);
			var url = js.html.URL.createObjectURL(blob);
			var download = document.createAnchorElement();
			download.href = url;
			download.download = "spell.png";
			download.click();
			download.remove();
			js.html.URL.revokeObjectURL(url);
#end
		};
		this.download.getName = () -> "Download";
		this.download.onOver = () -> {
			ChucK.broadcastEvent("xbutton2");
		};
		this.exit.onClick = o.close;
		this.exit.onOver = () -> {
			ChucK.broadcastEvent("xbutton2");
		};
		this.exit.getName = () -> "Exit";
	}

	private function getCenterPoint() {
		var b = spell.getBounds();

		return new h2d.col.Point(x + width/2 - b.width/2, y + height/2);
	}

	public function setSpell (root : h2d.Object, c : DropdownSelectorComp) {
		options.control.clear();

		c.getName = function () {
			return "Spell";
		};

		options.control.addItem(c);
		options.control.addItem(slotContainer);
		options.control.addItem(this.download);
		options.control.addItem(this.exit);

		c.onBack = options.control.logSelect;

		spell = c;
		root.addChild(c);
	}

	public function resize(w, h) {
		width = w;
		height = h;

		this.interactive.width = width;
		this.interactive.height = height;

		var t = ui.Hud.SpellSlotUiComp.getTiles(0);

		this.slots.x = Std.int( w*0.35 );
		this.slots.y = Std.int( h*0.9 );
		this.slots.y = Std.int( h - t.width - h * 0.005 );

		slotContainer.resize();

		var t = getExitTiles();
		this.exit.resize(t.width, t.height);

		this.exit.x = Std.int(w * 0.9);
		this.exit.y = Std.int(h *0.1);

		var t = getDownloadTiles();
		this.download.resize(t.width, t.height);

		this.download.x = Std.int(w * 0.9 - t.width - 10);
		this.download.y = Std.int(h *0.1);
	}

	public function redrawSlots() {
		slotContainer.redraw();
	}
}

class Altar extends dn.Process {
	public var spellHook(default,null) : h2d.Object;
	public var spell(default,null) : DropdownSelectorComp = null;
	public var dropdownContainer(default,null) : DropdownContainerComp = null;
	var img : h2d.Bitmap;
	public var control(default,null) : tools.MenuControl.VerticalMenuControl;

	var currentSlotId : Int = 0;
	var currentSlot : SpellSlotComp;
	var spells : Array<DropdownSelectorComp> = new Array();

	var options : Options;

	function centerSpellHook () {
		spellHook.x = stageWid*0.25;
		spellHook.y = stageHei*0.5;
	}

	public function new(p : dn.Process, imgRoot, control) {
		super(p);

		createRoot(p.root);
		spellHook = new h2d.Object();
		centerSpellHook();
		root.add(spellHook, 4);

		this.control = new tools.MenuControl.VerticalMenuControl(control, "Altar");
		this.control.repl = Main.ME.repl;

		img = new h2d.Bitmap(hxd.Res.altar.toAseprite().toTile());
		root.add(img, 1);

		var spellSlotOptions = { edit : edit
			, currentSlotId : currentSlotId
			, setCurrentSlot : function (s) { currentSlot = s; }
			, close : spl.Editor.ME.close
			, control : this.control
			, repl : Main.ME.repl
			, addSpellTo : addSpellTo
		};
		dropdownContainer = new DropdownContainerComp(spellSlotOptions, root);
	}

	public function open() {
		onOpen();
		resume();
		control.over();
		ChucK.broadcastEvent("book");
	}

	dynamic public function onOpen() {
	}

	public function close() {
		save();
		onClose();
		pause();
	}

	dynamic public function onClose() {
	}

	override public function resume() {
		super.resume();
		onResize();

		dropdownContainer.setSpell(this.spellHook, spell);
		spell.redraw();

		control.logSelect();

		spell.onBack = control.logSelect;

		dropdownContainer.redrawSlots();
	}

	public function hide() {
		root.visible = false;
	}

	public function show() {
		root.visible = true;
	}

	override public function pause() {
		super.pause();
		onPause();
	}

	dynamic public function onPause() {
	}

	override function update () {
		super.update();

		control.update();

		if( control.control.probe(Exit) || control.control.probe(Interact) ) {
			close();
		}
	}

	override function onResize() {
		super.onResize();

		if (spell != null)
			spell.redraw();

		var scale = dn.heaps.Scaler.bestFit_i(480,270);
		img.x = stageWid*0.5 - img.tile.width*scale*0.5;
		img.y = stageHei - img.tile.height*scale*0.5;
		img.scaleX = scale;
		img.scaleY = scale;

		dropdownContainer.resize(stageWid, stageHei);
	}

	public function setSpell(s : Option<spl.Lang.Spell>, selector) {
		if (spell != null)
			spell.remove();

		options = { state : Game.ME.spellState
				  , control : control.control
				  , repl : Main.ME.repl
				  , selector : Some (selector)
		};

		spell = Display.getASpellComponent(options, s, spell);
		centerSpellHook();
		dropdownContainer.setSpell(spellHook, spell);
	}

	public function getSpell() {
		return spell.extract(TSpell);
	}

	public function getSpellDisplay() {
		spell.remove();
		return spell;
	}

	private function edit(slot, id) {
		save();

		currentSlot.unsetActive();
		currentSlotId = id;
		currentSlot = slot;
		slot.setActive();
		spell.remove();
		spell = Display.getASpellComponent(options, Game.ME.spellCaster[id].formula, spells[id]);
		centerSpellHook();
		dropdownContainer.setSpell(spellHook, spell);
		dropdownContainer.redrawSlots();
	}

	function addSpellTo(scene : h2d.Scene) {
		var spell = getSpell();

		switch spell {
			case Some (s):
				var o = {  state : options.state
					, control : control.control
					, repl : Main.ME.repl
					, selector : options.selector
				};

				var sComp = Display.getSpellComponent(o, s, 0);
				sComp.x = 0.1 * stageWid;
				sComp.y = 0.5 * stageHei;
				scene.addChild(sComp);
				return true;
			case None: return false;
		};
	}

	public function save() {
		var spell = spell.extract(TSpell);

		switch spell {
			case Some (s) if (!Game.ME.spellState.book.exists(s)): Game.ME.spellState.book.set(s, false);
			case Some (_):
			case None:
		};

		Game.ME.spellState.slots[currentSlotId] = spell;
	}
}
