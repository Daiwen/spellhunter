/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl.editor;
import ui.Components;
import ui.SpriteNodeComp;
import spl.Token;
import haxe.ds.Option;

import spl.editor.Word;
import spl.editor.Spellbook.SpellbookComp;

typedef Options = {
	var state : spl.State;
	var repl : tools.Hake;
	var control : tools.CustomControl;
	var selector : Option<Selector>;
}

@:uiComp("word")
class WordComp extends h2d.Object implements h2d.domkit.Object {
	static var SRC = <word>
			<anchor id="bg" />
			<anchor id="label" />
			<container id="current" />
		</word>;

	var defaultItem : h2d.Text;
	var items : Array<Word> = new Array();

	public var selected(default, null) : Option<Int> = None;

	static final activeGrey = 0x777777;
	static final baseGrey = 0x222222;
	static final activeRed : Int = 0xaf7f66;
	static final baseRed : Int = 0xa85959;
	static final activeYellow : Int = 0xefef5e;
	static final baseYellow : Int = 0xfbff8c;

	var activeColor : Int = activeRed;
	var baseColor : Int = baseRed;
	var isActive : Bool = false;

	var baseTile : String;
	var activeTile : String;
	var bgTile : String;
	var baseOffset = 0.;
	var activeOffset = 0.;
	var offsetY : Float;

	var options : Options;
	var tokenType : AnyTokenType;

	var missingGlow : vfx.shdr.RadialGradient;

	private function initInteractive() {
		baseTile = Assets.buttonsDict.SpellButtonUp;
		baseOffset = -6.;
		activeOffset = 0.;

		this.current.enableInteractive = true;
		this.current.interactive.shape = new h2d.col.Circle(0, 0, OrbitalContainerComp.circleRadius);

		this.current.interactive.onPush = function(_) {
			bgTile = activeTile;
			offsetY = activeOffset;
			redraw();
		}

		this.current.interactive.onRelease = function(_) {
			bgTile = baseTile;
			offsetY = baseOffset;
			redraw();
		}

		this.current.interactive.onClick = function(_) {
			click();
		};
		this.current.interactive.onOver = function(_) {
			over();
			ChucK.broadcastEvent("xbutton1");
		};
		this.current.interactive.onOut = function(_) {
			out();
		};

		bgTile = baseTile;
		offsetY = baseOffset;
		updateCurrent();
	}

	public function new(o : Options, t : AnyTokenType, ?parent) {
		super(parent);
		tokenType = t;

		initComponent();

		options = o;

		var txt = new h2d.Text(hxd.res.DefaultFont.get());
		txt.font = Settings.uiFont;
		txt.text = "";
		defaultItem = txt;
		var b = defaultItem.getBounds();
		defaultItem.x = -b.width/2;
		defaultItem.y = -b.height/2;

		baseTile = Assets.buttonsDict.SpellButtonDown;
		activeTile = Assets.buttonsDict.SpellButtonDown;
		bgTile = baseTile;

		offsetY = baseOffset = activeOffset = 0;

		updateCurrent();
	}

	public function redraw() {
		var bgColor = baseColor;

		if (isActive)
			bgColor = activeColor;

		drawBg(bgColor);

		if (null == this.current.interactive)
			return;

		var b = this.bg.getBounds();
		var radius = Math.max(b.width, b.height)/2;
		this.current.interactive.shape = new h2d.col.Circle(0, 0, radius);
	}

	function oldBg(color) {
		var spr = new HSprite(Assets.buttons, bgTile, this.bg);
		spr.setCenterRatio(0.5, 0.5);
		spr.scaleX=Const.UI_SCALE;
		spr.scaleY=Const.UI_SCALE;

		var g = new h2d.Object(this.bg);
		g.y = offsetY;

		var graphics = new h2d.Graphics(g);
		graphics.beginFill(color, 1);
		graphics.drawCircle(0, 0, OrbitalContainerComp.circleRadius-1);
		graphics.endFill();

		return g;
	}

	private function decorate(g : h2d.Object, color : Int) {
		if (g == null)
			return;

		var b = g.getBounds();
		var radius = Math.max(b.width, b.height)/2;
		var active = new h2d.Graphics(g);
		active.x = -g.x;
		active.y = -g.y;
		var alpha = if (isActive) 0.3 else 0;
		active.beginFill(color, alpha);
		active.drawCircle(0, 0, radius);
		active.endFill();
		this.bg.addChild(g);
	}

	function drawBg(color) {
		this.bg.removeChildren();

		switch (selected) {
			case None:
				var g = spl.Graphics.getPlaceHolders(tokenType, Util.getColor(tokenType));

				var radius = if (g == null) {
					OrbitalContainerComp.circleRadius;
				} else {
					var b = g.getBounds();
					Math.max(b.width, b.height)/2;
				}

				radius *= 1.1;

				var graphics = new h2d.Graphics(this.bg);
				graphics.beginFill(color, 1);
				graphics.drawCircle(0, 0, radius);
				graphics.endFill();

				missingGlow = new vfx.shdr.RadialGradient();
				missingGlow.center = new h3d.Vector(0, 0, 0);
				updateGlow();
				missingGlow.radius = radius;
				missingGlow.innerColor = h3d.Vector4.fromColor(color);
				missingGlow.innerColor.a = 1;
				missingGlow.outerColor = h3d.Vector4.fromColor(color);
				missingGlow.outerColor.a = 0;

				graphics.addShader(missingGlow);

				decorate(g, color);

			case Some (idx):
				var g = Util.getGraphics(items[idx].tdata, options.control);
				decorate(g, color);
				if (null != g)
					return;

				var g = oldBg(color);

				if (Reflect.field(Assets.langDict, items[idx].name) == null)
					return;

				var spr = new HSprite(Assets.lang, items[idx].name, g);
				spr.setCenterRatio(0.5, 0.5);
				spr.scaleX=Const.UI_SCALE;
				spr.scaleY=Const.UI_SCALE;
		}
	}

	dynamic public function onSelect(i : Int) {
	}

	public function addItem(w : Word) {
		items.push(w);

		w.onSelect = function(i) {
			this.setSelected(Some (i), false);
			this.onSelect(i);
		};

		if (items.length == 2) {
			switch ([options.selector]) {
				case [Some (_)]:
					initInteractive();
				default:
			}
		}
	}

	public function cleanItems() {
		items = new Array();
		selected = None;
	}

	public function setSelected(i, isNew) {
		this.selected = i;
		if (isNew) {
			activeColor = activeYellow;
			baseColor = baseYellow;
		}
		else
			switch i {
				case Some (_):
					activeColor = activeGrey;
					baseColor = baseGrey;
				case None:
					activeColor = activeRed;
					baseColor = baseRed;
			}

		updateCurrent();
	}

	private function updateCurrent() {
		this.current.visible = true;
		this.current.removeChildren();
		this.label.removeChildren();

		switch this.selected {
			case None:
				this.label.addChild(defaultItem);
				getName = function () {
					return defaultItem.text;
				};
			case Some (i):
				var currentItem = new h2d.Text(Settings.uiFont, this.label);
				currentItem.text = items[i].name;
				var b = currentItem.getBounds();
				currentItem.x = -b.width/2;
				currentItem.y = -b.height/2;
				getName = function () {
					return items[i].name;
				};
		}
		var b = this.current.getBounds();
		this.x = -b.width/2;
		this.y = -b.height/2;
		if (this.current.enableInteractive) {
			this.current.interactive.x = b.width/2;
			this.current.interactive.y = b.height/2;
		}
		this.bg.x = b.width/2;
		this.bg.y = b.height/2;
		redraw();
	}

	public function click() {
		switch (options.selector) {
			case Some(selector):
				selector.open(items, this, selected);
			case None:
		}
	}

	dynamic public function onOver() {
	}

	public function over() {
		onOver();
		isActive = true;
		redraw();
	}

	public function out() {
		isActive = false;
		redraw();
	}

	dynamic public function getName() {
		return "";
	}

	public function updateGlow() {
		if (null == missingGlow)
			return;


		var p = this.bg.localToGlobal();

		missingGlow.center.x = p.x;
		missingGlow.center.y = p.y;
	}
}

@:uiComp("dropdown-selector")
class DropdownSelectorComp extends h2d.Object implements h2d.domkit.Object implements tools.MenuControl.MenuItem {

	var tokenType : AnyTokenType;

	public var currentToken(default, null) : Option<AnyTokenData>;

	public var subs(default, null) : Array<DropdownSelectorComp> = new Array();

	var choices = new Array();

	var level : Int;

	var menu : tools.MenuControl;
	var parentMenu : tools.MenuControl.Menu;

	var spriteElement : ui.SpriteElement;

	static var SRC = <dropdown-selector>
		<sprite-node(Util.getColor(token)) id="suffix" />
		<word(o, tokenType) id="selector"/>
	</dropdown-selector>;

	private var options : Options;

	private function initLabel() {
		var idx = None;
		var isNew = false;

		if (choices.length == 1 || (choices.length > 0 && Type.enumEq(tokenType, AnySpellToken (TSpell)))) {
		  this.currentToken = Some (choices[0]);
		}

		for (i in 0...choices.length) {
			var ltxt = Util.getLabel(choices[i]);

			switch this.currentToken {
				case None:
				case Some (cdata):
					var label = Util.getLabel(cdata);
					if (ltxt == label) {
						idx = Some (i);
						isNew = options.state.newWords.exists(label);
						menu.getName = () -> label;
					}
			}

			var item = new Word(choices[i], options.state);

			this.selector.addItem(item);
		}

		this.selector.setSelected(idx, isNew);
	}

	public function new(o : Options, token : AnyTokenType, level : Int, index : Int, t : Option<AnyTokenData>, ?subs : Array<DropdownSelectorComp>, ?parent : h2d.Object) {
		super(parent);
		this.level = level;
		this.options = o;
		tokenType = token;

		spriteElement = new ui.SpriteElement(this, { token : token, index : index });

		this.currentToken = t;

		choices = new Array();
		for (td in o.state.allWords) {
			var t = Util.getTokenType(td);
			if (Util.compareAnyToken(t, token)) {
				choices.push(td);
			}
		}

		initComponent();

		menu = new tools.MenuControl.VerticalMenuControl(o.control, "");
		menu.repl = o.repl;
		menu.onRollBack = function () {
			parentMenu.unlock();
			over();
		};
		menu.onLock = function () {
			this.parentMenu.lock(menu);
			out();
		};
		menu.onUnlock = function () {
			menu.out();
		};
		menu.onSelect = function () {
			out();
			this.parentMenu.lock(menu);
		};

		initLabel();

		updateSubComponents(subs);

		this.selector.onSelect = function(id) {

			switch (choices[id]) {
				case (AnyNamedSpell (s)):
					var s = Display.getSpellComponent(o, s, level);
					var subs = s.subs;
					currentToken = s.currentToken;
					updateSubComponents(subs);
					this.selector.cleanItems();
					initLabel();
					redraw();
				default:
					currentToken = Some(choices[id]);
					updateSubComponents(null);
			}

			over();
		};

		this.selector.onOver = function() {
			if (parentMenu == null)
				return;
			parentMenu.select(this);
		}
	}

	private function updateSubsComponentsFromTokenData<T>(td:TokenData<T>, ?subs : Array<DropdownSelectorComp>) {
		this.menu.clear();
		for (i in 0...td.subs.length) {
			var c;

			if (subs != null) {
				c = subs[i];
			}
			else {
				c = new DropdownSelectorComp(options, td.subs[i], level+1, i, None);
			}

			c.onBack = logSelect;

			this.menu.addItem(c);
			this.suffix.addSpriteChild(td.token, c.spriteElement);
			this.subs.push(c);
		}
	}

	private function updateSubComponents(?subs : Array<DropdownSelectorComp>) {
		this.suffix.remove();
		for (s in this.subs.iterator()) {
			s.remove();
		}
		this.subs = new Array();
		this.suffix = new SpriteNodeComp(Util.getColor(tokenType), this);
		switch currentToken {
			case Some (AnyEntityIdData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnyEffectData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnyKindData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnyMotionKindData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			case Some (AnySpellData (t)):
				updateSubsComponentsFromTokenData(t, subs);
			default:
		}
	}

	private override function draw(ctxt) {
		super.draw(ctxt);

		this.selector.updateGlow();
	}

	public function redraw() {
		this.suffix.resize();
		updateSubComponents(this.subs.copy());
		this.selector.redraw();

		for (s in this.subs)
			s.redraw();
	}

	public function extract<T>(ty : TokenType<T>) : Option<T> {
		switch [ currentToken, ty ] {
			case [ Some (AnyEntityIdData({ extract : extract })), TEntityId ]: return extract(subs);
			case [ Some (AnyEffectData({ extract : extract })), TEffect ]: return extract(subs);
			case [ Some (AnyKindData({ extract : extract })), TKind ]: return extract(subs);
			case [ Some (AnyMotionKindData({ extract : extract })), TMotionKind ]: return extract(subs);
			case [ Some (AnySpellData({ extract : extract })), TSpell ]: return extract(subs);
			default: return None;
		};
	}

	dynamic public function getName() {
		return this.selector.getName();
	}

	public function click() {
		this.selector.click();
		out();
	}

	public function over() {
		this.selector.over();
	}

	public function out() {
		this.selector.out();
	}

	public function register(m : tools.MenuControl.Menu) {
		parentMenu = m;
	}

	public function unroll() {
		if (subs.length == 0)
			return;

		parentMenu.lock(menu);
		menu.over();
		out();
	}

	public function rollBack() {
		parentMenu.rollBack();
	}

	private function extractString() {
		var subString =
		if (subs.length > 0) {
		var subStrings = [for (s in subs) s.extractString()];
		 "(" + subStrings.join(", ") + ")";
		} else {
			"";
		}
		return this.selector.getName() + subString;
	}

	public function logSelect() {
		options.repl.log(extractString());

		var back = {
			label : "Back",
			select : onBack,
		};

		var edit = {
			label : "Edit",
			select : function () {
				this.selector.click();
			},
		};

		menu.show([back, edit]);
	}

	dynamic public function onBack() {
	}
}

class Display {
	static public function getEntityIdComponent(o : Options, r : spl.Lang.EntityId, level, i) {
		return new DropdownSelectorComp(o, AnyEntityIdToken (TEntityId), level, i, Some (AnyEntityIdData(LangToToken.entityId(r)))); }

	static public function getEffectComponent(o : Options, e : spl.Lang.Effect, level, i) {
		switch e {
			case Sequence(e1, e2):
				var e1Component = getEffectComponent(o, e1, level+1, 0);
				var e2Component = getEffectComponent(o, e2, level+1, 1);

				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, i, Some (AnyEffectData(LangToToken.effect(e))), [e1Component, e2Component]);
			case Meta(s):
				var sComponent = getSpellComponent(o, s, level+1);

				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, i, Some (AnyEffectData(LangToToken.effect(e))), [sComponent]);
			case Mark | Elemental(_) | Physical(_):
				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, i, Some (AnyEffectData(LangToToken.effect(e))));
			case Link(ei):
				var eiComponent = getEntityIdComponent(o, ei, level+1, 0);

				return new DropdownSelectorComp(o, AnyEffectToken (TEffect), level, i, Some (AnyEffectData(LangToToken.effect(e))), [eiComponent]);
		}
	}

	static public function getMotionKindComponent(o : Options, mk : spl.Lang.MotionKind, level, i) {
		switch mk {
			case Projectile | Attached(Ground):
				return new DropdownSelectorComp(o, AnyMotionKindToken (TMotionKind), level, i, Some (AnyMotionKindData(LangToToken.motionKind(mk))), []);
			case Attached(Entity (ei)):
				var eiComponent = getEntityIdComponent(o, ei, level+1, 0);

				return new DropdownSelectorComp(o, AnyMotionKindToken (TMotionKind), level, i, Some (AnyMotionKindData(LangToToken.motionKind(mk))), [eiComponent]);
		}
	}

	static public function getKindComponent(o : Options, k : spl.Lang.Kind, level, i) {
		return new DropdownSelectorComp(o, AnyKindToken (TKind), level, i, Some (AnyKindData(LangToToken.kind(k))));
	}

	static public function getSpellComponent(o : Options, s : spl.Lang.Spell, level) : DropdownSelectorComp {
		switch s {
			case Spell(k, e, mk):
				var kComponent = getKindComponent(o, k, level+1, 0);
				var eComponent = getEffectComponent(o, e, level+1, 1);
				var mkComponent = getMotionKindComponent(o, mk, level+1, 2);

				return new DropdownSelectorComp(o, AnySpellToken (TSpell), level, 0, Some (AnySpellData(LangToToken.spell(s))), [kComponent, eComponent, mkComponent], null);
		}
	}

	static public function getASpellComponent(o : Options, s : Option<spl.Lang.Spell>, ?spell) {
		var spl =
			switch s {
				case Some (s):
					getSpellComponent(o, s, 0);
				case None:
					if (spell == null)
						new DropdownSelectorComp(o, AnySpellToken (TSpell), 0, 0, None);
					else
						spell;
			};
		spl.redraw();
		return spl;
	}
}
