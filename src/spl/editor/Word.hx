/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package spl.editor;

import spl.Token;

class Word {
    public var tdata(default, null) : AnyTokenData;
	public var name(get, set) : String;
	public var description(get, set) : String;
	var data : spl.State.WordData;

	public var defaultName(default, null) : String;
	var defaultDescription : String;

	inline function get_name() return data.name;
	inline function set_name(v) {
		return
			if (v == "")
				data.name = defaultName;
			else
				data.name = v;
	}
	inline function get_description() return data.description;
	inline function set_description(v) {
		return
			if (v == "")
				data.description = defaultDescription;
			else
				data.description = v;
	}

	public function new(td : AnyTokenData, m : spl.State) {
		tdata = td;

		defaultName = Util.getLabel(td);
		defaultDescription = Util.getDescription(td);

		data = m.wordData.get(defaultName);

		if (data != null)
			return;

		data = { name : defaultName, description : defaultDescription }
		m.wordData.set(defaultName, data);
	}

	dynamic public function onSelect(i : Int) {
	}
}
