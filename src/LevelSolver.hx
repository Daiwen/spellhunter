/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import tools.IntSet;
import tools.WFC;
import haxe.ds.Option;

enum SolvedCell {
	Unreachable(isWalkable : Bool);
	Reachable;
}

enum CellState {
	Unwalkable;
	Walkable;
	Goal;
}

class EquatableState {
	public var state(default, null) : CellState;
	public var isReachable(default, null) : Option<Bool>;

	public function new(state, isReachable) {
		this.state = state;
		this.isReachable = isReachable;
	}

	public function isEqual(es : EquatableState) {
		return state == es.state &&
			(isReachable == None
			 || es.isReachable == None
			 || isReachable == es.isReachable);
	}
}

class SolverNeighbours extends tools.Neighbours {
	static var allIndices = [for (i in 0...4) i];
	static function getReachableNeighbours() {
			var i = new tools.Neighbours();

			i.topLeft = new IntSet();
			i.topLeft.addList(allIndices);
			i.topRight = new IntSet();
			i.topRight.addList(allIndices);
			i.bottomLeft = new IntSet();
			i.bottomLeft.addList(allIndices);
			i.bottomRight = new IntSet();
			i.bottomRight.addList(allIndices);

			i.top = new IntSet();
			i.top.addList([0,2,3]);
			i.bottom = new IntSet();
			i.bottom.addList([0,2,3]);
			i.left = new IntSet();
			i.left.addList([0,2,3]);
			i.right = new IntSet();
			i.right.addList([0,2,3]);

			return i;
	}

	static function getNonReachableNeighbours() {
			var i = new tools.Neighbours();

			i.topLeft = new IntSet();
			i.topLeft.addList(allIndices);
			i.topRight = new IntSet();
			i.topRight.addList(allIndices);
			i.bottomLeft = new IntSet();
			i.bottomLeft.addList(allIndices);
			i.bottomRight = new IntSet();
			i.bottomRight.addList(allIndices);

			i.top = new IntSet();
			i.top.addList([0,1]);
			i.bottom = new IntSet();
			i.bottom.addList([0,1]);
			i.left = new IntSet();
			i.left.addList([0,1]);
			i.right = new IntSet();
			i.right.addList([0,1]);

			return i;
	}

	static public function fromCellStates() {
		var unwalkable = {
			neighbours : tools.Neighbours.full(4),
			priority : 1,
			kind : new EquatableState(Unwalkable, Some(false)),
		}
		var nrWalkable = {
			neighbours : getNonReachableNeighbours(),
			priority : 2,
			kind : new EquatableState(Walkable, Some(false)),
		}
		var rWalkable = {
			neighbours : getReachableNeighbours(),
			priority : 1,
			kind : new EquatableState(Walkable, Some(true)),
		}
		var goal = {
			neighbours : getReachableNeighbours(),
			priority : 1,
			kind : new EquatableState(Goal, Some(true)),
		}

		return [unwalkable, nrWalkable, rWalkable, goal];
	}
}

class LevelSolver {
	var level : Level;

	var solver : tools.WFC<EquatableState>;
	var solution : Array<Bool>;
	public var invalidated : Bool;

	public function new(l : Level, hasGoal : (Int, Int) -> Bool) {
		level = l;
		invalidated = false;
		var tiles = SolverNeighbours.fromCellStates();
		var defaultConstraint = new EquatableState(Unwalkable, None);
		solver = new tools.WFC(l.hei, l.wid, tiles, defaultConstraint);

		for (cx in 1...(l.wid - 1)) {
			for (cy in 1...(l.hei - 1)) {
				var state =
					if (hasGoal(cx, cy)) {
						Goal;
					} else {
						l.isWalkable(cx, cy) ? Walkable : Unwalkable;
					}

				solver.setTile(cx, cy, new EquatableState(state, None));
			}
		}
	}

	public function isReachable(cx, cy) {
		if (invalidated) {
			invalidated = false;
			var indices = solver.run();
			solution = indices.map((i)-> i == 2 || i == 3);
		}

		return solution[cx + cy * level.wid];
	}

#if (debug)
	public function debugSolution(root : h2d.Object) {
		debug(root, solution, (v) -> if (v) 0x00ff00 else 0xff0000);
	}

	public function debugState(root : h2d.Object) {
		debug(root, solver.baseGrid, (v) ->
				switch (v.state) {
					case Goal: 0xffff00;
					case Unwalkable: 0xff0000;
					case Walkable: 0x00ff00;
				}
		);
	}

	var debug_graphics : h2d.Object;
	var debug_invalidated : Bool;
	public function debug<T>(root : h2d.Object, data : Array<T>, getColor : (T) -> Int) {
		if (!debug_invalidated)
			return;

		if (debug_graphics != null)
			debug_graphics.remove();

		debug_graphics = new h2d.Object(root);

		debug_invalidated = false;
		var size = Const.GRID;
		for (s in data.keyValueIterator()) {
			var color = getColor(s.value);

			var h = new h2d.Graphics(debug_graphics);
			h.beginFill(color, 0.5);
			h.drawRect(-size/2, -size/2, size, size);
			h.endFill();

			h.x = s.key % level.wid * size;
			h.y = Std.int(s.key/level.wid) * size;
		}
	}
#end

	public function set(x, y, s) {
		solver.setTile(x, y, new EquatableState(s ? Walkable : Unwalkable, None));
		invalidated = true;
#if (debug)
		debug_invalidated = true;

#end
	}
}
