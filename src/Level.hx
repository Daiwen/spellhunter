/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import ent.comp.Spellable as S;
import spl.Lang;

enum Coord { Point (x : Int, y : Int); }
enum Neighbourhood {
	Moore;
	VonNeumann;
}

class Level extends dn.Process {
	public var TILES(default, null) = new Array();
	public var TEMP_TILES(default, null) : Map<Coord, ProtoEntity> = new Map();
	public var game(get,never) : Game; inline function get_game() return Game.ME;
	public var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;

	public var wid : Int;
	public var hei : Int;

	public var level : World_Level;
	public var solver(default, null) : LevelSolver;
	public var isSolved(default, null) : Bool;
	var painter : LevelPainter;
	var ntilesetSource : h2d.Tile;
	var normalsSource : h2d.Tile;
	var dispellSource : h3d.mat.Texture;
	var maskSource : h3d.mat.Texture;

	var invalidated = true;
	var isHeroNextToAltar = false;

	var wallTile : Map<Coord, h2d.Tile> = new Map();
	var walls : Map<Coord, Bool> = new Map();

	public var collisions(default, null) : Map<Coord, Bool> = new Map();

	var holes : Map<Coord, Bool> = new Map();
	public var antiMagic(default, null) : Map<Coord, Bool> = new Map();
	var endZones : Map<Coord, Bool> = new Map();
	var triggers : Map<Coord, ent.Steppable> = new Map();
	var triggerables : Map<Coord, ent.Triggerable> = new Map();
	var bonuses : Map<Coord, ent.Bonus> = new Map();

	var waters : Map<Coord, Bool> = new Map();
	var ices : Map<Coord, Bool> = new Map();

	var lava : Map<Coord, Bool> = new Map();
	var solidLava : Map<Coord, Bool> = new Map();
	var lavaLights : Map<Coord, Lights.LightSource> = new Map();

	var altars : Map<Coord, Bool> = new Map();
	var marks : Map<Coord, Bool> = new Map();

	var texts : h2d.Object;
	var fgTexts : h2d.Object;
	var bg : h2d.TileGroup;
	var holeRoot : h2d.Object;
	public var dyns(default, null) : h2d.Object;
	var bottom : h2d.TileGroup;
	var bottomFx : h2d.Object;
	var altar : h2d.Object;
	var top : h2d.TileGroup;
	var topFx : h2d.Object;
	var post : h2d.Object;

	public function new(l : World_Level) {
		super(Game.ME);
		createRootInLayers(Game.ME.scroller, Const.DP_BG);

		level = l;

		ntilesetSource = hxd.Res.world.ntileset.toAseprite().toTile();
		normalsSource = hxd.Res.world.nnormals.toAseprite().toTile();
		dispellSource = hxd.Res.world.dispell.toAseprite().getTexture();
		maskSource = hxd.Res.world.idmask.toAseprite().getTexture();

		wid = l.l_NCollisions.cWid;
		hei = l.l_NCollisions.cHei;

		fgTexts = new h2d.Object();
		game.scroller.add(fgTexts, Const.DP_TOP);

		holeRoot = new h2d.Object(root);
		dyns = new h2d.Object(root);
		dyns.y = LevelPainter.tileSize*1/4;
		bg = new h2d.TileGroup(ntilesetSource);
		game.root.add(bg, Const.DP_TRUE_BG);
		bottom = new h2d.TileGroup(ntilesetSource);
		bottom.x = LevelPainter.tileSize/2;
		bottom.y = LevelPainter.tileSize*3/4;
		game.scroller.add(bottom, Const.DP_BG);
		bottomFx = new h2d.Object();
		bottomFx.y = LevelPainter.tileSize/2;
		game.scroller.add(bottomFx, Const.DP_FX_BG);
		altar = new h2d.Object();
		game.scroller.add(altar, Const.DP_MAIN);
		top = new h2d.TileGroup(ntilesetSource);
		top.x = LevelPainter.tileSize/2;
		top.y = LevelPainter.tileSize/4;
		game.scroller.add(top, Const.DP_FRONT);
		topFx = new h2d.Object();
		game.scroller.add(topFx, Const.DP_FX_FRONT);
		post = new h2d.Object(Boot.ME.postRoot);

		texts = new h2d.Object();
		game.scroller.add(texts, Const.DP_BG);
	}
	
	override function onDispose() {
		super.onDispose();

		texts.remove();
		fgTexts.remove();

		holeRoot.remove();
		dyns.remove();
		top.remove();
		topFx.remove();
		bg.remove();
		bottom.remove();
		bottomFx.remove();
		altar.remove();
		post.remove();

		for (t in TILES) {
			t.destroy();
		}
		for (t in TEMP_TILES.iterator()) {
			t.destroy();
		}
		TILES = null;
		TEMP_TILES = null;
	}

	public function initWall(cx, cy) {
		var pe = new ProtoEntity("Wall");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addCollision(cx, cy);
		var pe = new ProtoEntity("Anti-magic field");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addAntiMagic(cx, cy);
	}

	public function initEndZone(cx, cy) {
		var pe = new ProtoEntity("Endzone");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addEndZone(cx, cy);
	}

	public function initAltar(cx, cy) {
		var pe = new ProtoEntity("Altar");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addCollision(cx, cy);
		addAntiMagic(cx, cy);
		var pe = new ProtoEntity("Anti-magic field");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addAltar(cx, cy);
	}

	public function initHole(cx, cy) {
		var pe = new ProtoEntity("Hole");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addHole(cx, cy);

		var sprite = new HSprite(Assets.tiles);
		holeRoot.addChild(sprite);
		sprite.x = cx * Const.GRID;
		sprite.y = cy * Const.GRID;
		sprite.set(Assets.tilesDict.Hole);
	}

	public function initAntiMagic(cx, cy) {
		var pe = new ProtoEntity("Anti-magic field");
		TILES.push(pe);
		pe.cx = cx;
		pe.cy = cy;
		addAntiMagic(cx, cy);
	}

	public function createLavaLight(cx : Int, cy : Int, v : Bool) {
		var x = (cx + 0.5) * Const.GRID;
		var y = (cy + 0.5) * Const.GRID;

		var pos = new h3d.Vector4(x, y, Const.GRID * 4, Const.GRID/2);
		var s = new Lights.LightSource(pos, 0xee5b41);
		game.lights.addLightSource(s);
		s.visible = v;
		lavaLights.set(Point(cx, cy), s);
	}

	public function hasGoal(cx, cy) {
		return cx == (wid - 2);
	}

	public function initLevel() {
		solver = new LevelSolver(this, hasGoal);
		painter = new LevelPainter(this, Game.ME.curLevelIdx);

		for(e in level.l_Entities.all_Text) {
			switch (e.f_isAccessible) {
				case (Both | Sightless):
					game.repl.log(e.f_str);
				case Sighted:
			}
		}

		for (tb in level.l_Entities.all_ThornyBush) {
			new ent.ThornyBush(tb.cx, tb.cy);
		}

		for (s in level.l_Entities.all_NewSpell) {
			var nsuid = Game.NSUid.Id (Game.ME.curLevelIdx, s.cx, s.cy);
			if (!Game.ME.newSpells.exists(nsuid)) {
				var sp = switch s.f_name {
					case FireBolt: Spell (Collider, Elemental (Fire), Projectile);
					case FireTrap: Spell (Collider, Elemental (Fire), Attached (Ground));
					case Push: Spell (Collider, Physical (Push), Projectile);
					case Teleport: Spell (Collider, Physical (Teleport), Projectile);
					case BouncingFire: Spell (Collider, Meta (Spell (Collider, Elemental (Fire), Projectile)), Projectile);
					case IceBolt: Spell (Collider, Elemental (Ice), Projectile);
					case TIceTrap: Spell (Trigger, Elemental (Ice), Attached (Ground));
					case IceShield: Spell (Activer, Elemental (Ice), Attached (Entity (Caster)));
					case IcePush: Spell (Collider, Sequence (Elemental (Ice), Physical (Push)), Projectile);
					case MarkedFire: Spell (Activer, Elemental (Fire), Attached (Entity (Marked)));
					case RecallFromAfar: Spell (Collider, Sequence (Mark, Meta (Spell (Trigger, Physical (Teleport), Attached (Entity (Marked))))), Projectile);
					case Link: Spell (Collider, Link (Caster), Projectile);
				};
				new ent.NewSpell(s.cx, s.cy, nsuid, sp);
			}
		}

		for (s in level.l_Entities.all_NewSlot) {
			var nsuid = Game.NSUid.Id (Game.ME.curLevelIdx, s.cx, s.cy);
			if (!Game.ME.newSpells.exists(nsuid)) {
				new ent.NewSlot(s.cx, s.cy, nsuid);
			}
		}

		for (t in level.l_Entities.all_Trigger) {
			new ent.Trigger(t.cx, t.cy, t.f_targets);
		}

		for (s in level.l_Entities.all_Switch) {
			new ent.Switch(s.cx, s.cy, s.f_targets);
		}

		for (r in level.l_Entities.all_Rock) {
			new ent.Rock(r.cx, r.cy);
		}

		for (b in level.l_Entities.all_Bridge) {
			new ent.Bridge(b.cx, b.cy, b.f_isTriggerable, b.f_isActive);
		}

		for (d in level.l_Entities.all_Door) {
			new ent.Door(d.cx, d.cy, d.f_isTriggerable, d.f_isActive, d.f_direction);
		}

		for (at in level.l_Entities.all_ArrowTrap) {
			new ent.ArrowTrap(at.cx, at.cy, at.f_period, at.f_target, at.f_isTriggerable, at.f_isActive);
		}

		var marked = S.ALL.filter(function (e : ent.comp.Spellable) {
			var marked = level.l_Entities.all_Mark.filter( function(m) { return m.cx == e.cx && m.cy == e.cy; });
		   	return marked.length > 0;
		});

		Game.ME.mark(marked);

		isSolved = solver.isReachable(game.hero.cx, game.hero.cy);
	}

	public inline function isValidX(cx) return (cx>=0 && cx<wid);
	public inline function isValidY(cy) return (cy>=0 && cy<hei);
	public inline function isValid(cx,cy) return isValidX(cx) && isValidY(cy);
	public inline function coordId(cx,cy) return cx + cy*wid;


	public function render() {
		top.clear();
		topFx.removeChildren();
		bg.clear();
		bottom.clear();
		bottomFx.removeChildren();
		altar.removeChildren();
		texts.removeChildren();
		fgTexts.removeChildren();
		painter.runWFC();

		for(e in level.l_Entities.all_Text) {
			switch (e.f_isAccessible) {
				case (Both | Sighted):
					var w = new h2d.Object(texts);
					var tf = new h2d.Text(Settings.gameFont, w);
					tf.text = e.f_str;
					tf.x = Std.int( -tf.textWidth*0.5 );
					tf.y = Std.int( -tf.textHeight*0.5 );
					w.x = Std.int( e.cx * Const.GRID + Const.GRID*0.5 );
					w.y = Std.int( e.cy * Const.GRID + Const.GRID*0.5 );
				case Sightless:
			}
		}

		dyns.removeChildren();
		post.removeChildren();

		for (w in waters.keys()) {
			switch w {
				case Point(cx, cy):
					var sprite = new HSprite(Assets.tiles);
					dyns.addChild(sprite);
					sprite.x = cx * Const.GRID;
					sprite.y = cy * Const.GRID;
					sprite.set(Assets.tilesDict.Water);
			}
		}

		for (l in lava.keys()) {
			switch l {
				case Point(cx, cy):


					var sprite = new HSprite(Assets.tiles);
					dyns.addChild(sprite);
					sprite.x = cx * Const.GRID;
					sprite.y = cy * Const.GRID;
					sprite.set(Assets.tilesDict.Lava);

					if (Settings.shaders) {
						var shader = new vfx.shdr.Lava();
						sprite.addShader(shader);
					}

					if (Settings.shaders) {
						var color = 0xa85959;
						var radius = Const.GRID*0.75 + Const.GRID/10;
						var size = 2*Const.GRID;

						var h = new h2d.Graphics(post);
						h.beginFill(color, 0.);
						h.drawRect(-size/2, -size/2, size, size);
						h.endFill();

						h.x = (cx + 0.5) * Const.GRID;
						h.y = (cy + 0.25) * Const.GRID;

						var shader = new vfx.shdr.Heat(radius, h.x, h.y);
						h.addShader(shader);
					}
			}
		}

		for (a in altars.keys()) {
			switch a {
				case Point(cx, cy):
					if (Math.abs(cx - game.hero.cx) <= 1 && Math.abs(cy - game.hero.cy) <= 1) {
						var sprite = new HSprite(Assets.tiles);
						altar.addChild(sprite);
						sprite.x = cx * Const.GRID;
						sprite.y = cy * Const.GRID;
						sprite.set(Assets.tilesDict.Altar);

						if (Settings.shaders) {
							var shader = new vfx.shdr.Silhouette(sprite.x + 0.5 * Const.GRID, sprite.y + 0.5 * Const.GRID);
							sprite.addShader(shader);
						}

						if (Settings.textHints) {
							var tf = new h2d.Text(Settings.gameFont, fgTexts);
							tf.text = "Press \"E\"";
							tf.x = sprite.x + Std.int( (Const.GRID -tf.textWidth)*0.5 );
							tf.y = sprite.y + Std.int( -tf.textHeight*0.5 );
						}

					}

					var nsprite = new HSprite(Assets.tiles);
					altar.addChild(nsprite);
					nsprite.x = cx * Const.GRID;
					nsprite.y = cy * Const.GRID;
					nsprite.set(Assets.tilesDict.Altar);

				default:
			}
		}

		var size = LevelPainter.tileSize;
		for (i in 0...painter.bgW) {
			for (j in 0...painter.bgH) {
				var at = painter.bgLayer[i + j * (painter.bgW)];
				var x = at.x * size;
				var y = at.y * size;
				var t = ntilesetSource.sub(x, y, size, size);
				bg.add(i*size, j*size, t);
			}
		}
		if (Settings.shaders) {
			var shader = new vfx.shdr.DrawNormals(normalsSource);
			shader.setPriority(50);
			bg.addShader(shader);

			var shader = new vfx.shdr.DrawAlt(dispellSource);
			shader.setPriority(50);
			bg.addShader(shader);

			var shader = new vfx.shdr.DrawMask(maskSource);
			shader.setPriority(50);
			bg.addShader(shader);
		}
		for (l in painter.bottomLayers) {
			for (i in 0...wid-1) {
				for (j in 0...hei-1) {
					var at = l[i + j * (wid - 1)];
					var x = at.x * size;
					var y = at.y * size;
					var t = ntilesetSource.sub(x, y, size, size);
					bottom.add(i*size, j*size, t);
				}
			}
		}
		if (Settings.shaders) {
			var shader = new vfx.shdr.DrawNormals(normalsSource);
			shader.setPriority(50);
			bottom.addShader(shader);

			var shader = new vfx.shdr.DrawAlt(dispellSource);
			shader.setPriority(50);
			bottom.addShader(shader);

			var shader = new vfx.shdr.DrawMask(maskSource);
			shader.setPriority(50);
			bottom.addShader(shader);
		}
		for (l in painter.topLayers) {
			for (i in 0...wid-1) {
				for (j in 0...hei-1) {
					var at = l[i + j * (wid - 1)];
					var x = at.x * size;
					var y = at.y * size;
					var t = ntilesetSource.sub(x, y, size, size);
					top.add(i*size, j*size, t);
				}
			}
		}

		if (Settings.shaders) {
			var shader = new vfx.shdr.DrawNormals(normalsSource);
			shader.setPriority(50);
			top.addShader(shader);

			var shader = new vfx.shdr.DrawAlt(dispellSource);
			shader.setPriority(50);
			top.addShader(shader);

			var shader = new vfx.shdr.DrawMask(maskSource);
			shader.setPriority(50);
			top.addShader(shader);
		}

		for (m in marks.keys()) {
			switch m {
				case Point(cx, cy):
					drawMark(cx, cy, bottomFx);
					drawMark(cx, cy, topFx);
			}
		}
	}

	function drawMark(cx, cy, layer) {
		var size = Const.GRID;
		var color = 0xaaaaaa;
		var h = new h2d.Graphics(layer);
		h.beginFill(color, .5);
		h.drawRect(-size/2, -size/2, size, size);
		h.endFill();

		h.x = (cx + 0.5) * Const.GRID;
		h.y = (cy + 0.25) * Const.GRID;
	
		if (Settings.shaders) {
			var shader = new vfx.shdr.MarkEntity();
			h.addShader(shader);
		}
	}

	public function hasCollision(cx, cy) : Bool {
		return collisions.exists(Point(cx, cy));
	}

	public function hasAltar(cx, cy) : Bool {
		return altars.exists(Point(cx, cy));
	}

	public function hasHole(cx, cy) : Bool {
		return holes.exists(Point(cx, cy));
	}

	public function hasAntiMagic(cx, cy) : Bool {
		return antiMagic.exists(Point(cx, cy));
	}

	public function hasEndZone(cx, cy) : Bool {
		return endZones.exists(Point(cx, cy));
	}

	public function shouldChangeLevel(cx, cy) : Bool {
		return (cx == 0 || cx == (wid - 1) || cy == 0 || cy == (hei - 1)) && !hasCollision(cx, cy);
	}

	public function hasWarmable(cx, cy) : Bool {
		return hasSolidLava(cx, cy) || hasIce(cx, cy);
	}

	override function update() {
		super.update();

		if (!isSolved && solver.isReachable(game.hero.cx, game.hero.cy)) {
			ChucK.broadcastEvent("transition");
			isSolved = true;
		}	

		var tmp = checkNeighbours(Level.Neighbourhood.Moore, hasAltar, game.hero.cx, game.hero.cy);
		if ((isHeroNextToAltar || tmp) && !(isHeroNextToAltar && tmp)) {
			game.repl.log("Press \"E\" to interact with the altar.");
			isHeroNextToAltar = tmp;
			invalidated = true;
		}
	}

	override function postUpdate() {
		super.postUpdate();

		if (!cd.hasSetS("fireFx", 0.2)) {
			for (l in lava.keys()) {
				switch l {
					case Point (x, y):
						fx.fire(tools.Geometry.Shape.Circle((x + 0.5) * Const.GRID, (y + 0.5) * Const.GRID, (Const.GRID*0.75 + Const.GRID/10)), (x, y) -> hasLava(Math.floor(x/Const.GRID), Math.floor(y/Const.GRID)));
				}
			}
		}

		if( invalidated ) {
			invalidated = false;
			render();
		}
	}

	public function addEndZone(cx, cy) {
		endZones.set(Point(cx, cy), true);
	}

	public function removeEndZone(cx, cy) {
		endZones.remove(Point(cx, cy));
	}

	public function addHole(cx, cy) {
		holes.set(Point(cx, cy), true);
		solver.set(cx, cy, false);
	}

	public function removeHole(cx, cy) {
		holes.remove(Point(cx, cy));
		solver.set(cx, cy, true);
	}

	public function addAntiMagic(cx, cy) {
		antiMagic.set(Point(cx, cy), true);
	}

	public function removeAntiMagic(cx, cy) {
		antiMagic.remove(Point(cx, cy));
	}

	public function addTrigger(t:ent.Steppable) {
		triggers.set(Point(t.cx, t.cy), t);
	}

	public function removeTrigger(cx, cy) {
		triggers.remove(Point(cx, cy));
	}

	public function getTrigger(cx, cy) {
		return triggers.get(Point(cx, cy));
	}

	public function addTriggerable(e:ent.Triggerable) {
		triggerables.set(Point(e.cx, e.cy), e);
	}

	public function removeTriggerable(cx, cy) {
		triggerables.remove(Point(cx, cy));
	}

	public function getTriggerable(cx, cy) {
		return triggerables.get(Point(cx, cy));
	}

	public function addCollision(cx, cy) {
		collisions.set(Point(cx, cy), true);
		solver.set(cx, cy, false);
	}

	public function removeCollision(cx, cy) {
		collisions.remove(Point(cx, cy));
		solver.set(cx, cy, true);
	}

	public function addAltar(cx, cy) {
		altars.set(Point(cx, cy), true);
	}

	public function removeAltar(cx, cy) {
		altars.remove(Point(cx, cy));
	}

	public function addIce(cx, cy) {
		var c = Point(cx, cy);
		var pe = new ProtoEntity("Ice");
		TEMP_TILES.set(c, pe);
		pe.cx = cx;
		pe.cy = cy;
		ices.set(c, true);
		solver.set(cx, cy, true);

		if (null == painter)
			return;

		var b = new LevelPainter.Biome(hasAntiMagic(cx, cy), false, LevelPainter.TA_biome.Ice, LevelPainter.TA_biome.Empty);
		painter.set(cx, cy, b);
	}

	public function removeIce(cx, cy) {
		var c = Point(cx, cy);
		var pe = TEMP_TILES.get(c);
		TEMP_TILES.remove(c);
		pe.destroy();
		ices.remove(c);
	}

	public function hasIce(cx, cy) {
		return ices.exists(Point(cx, cy));
	}

	public function addWater(cx, cy) {
		var c = Point(cx, cy);
		var pe = new ProtoEntity("Water");
		TEMP_TILES.set(c, pe);
		pe.cx = cx;
		pe.cy = cy;
		waters.set(c, true);
		solver.set(cx, cy, false);

		if (null == painter)
			return;

		var b = new LevelPainter.Biome(hasAntiMagic(cx, cy), false, LevelPainter.TA_biome.Water, LevelPainter.TA_biome.Empty);
		painter.set(cx, cy, b);
	}

	public function removeWater(cx, cy) {
		var c = Point(cx, cy);
		var pe = TEMP_TILES.get(c);
		TEMP_TILES.remove(c);
		pe.destroy();
		waters.remove(c);
	}

	public function hasWater(cx, cy) {
		return waters.exists(Point(cx, cy));
	}

	public function addSolidLava(cx, cy) {
		var c = Point(cx, cy);
		var pe = new ProtoEntity("Solid lava");
		TEMP_TILES.set(c, pe);
		pe.cx = cx;
		pe.cy = cy;
		solidLava.set(c, true);
		solver.set(cx, cy, true);
	
		if (null == painter)
			return;

		var b = new LevelPainter.Biome(hasAntiMagic(cx, cy), false, LevelPainter.TA_biome.SolidLava, LevelPainter.TA_biome.Empty);
		painter.set(cx, cy, b);
	}

	public function removeSolidLava(cx, cy) {
		var c = Point(cx, cy);
		var pe = TEMP_TILES.get(c);
		TEMP_TILES.remove(c);
		pe.destroy();
		solidLava.remove(Point(cx, cy));
	}

	public function hasSolidLava(cx, cy) {
		return solidLava.exists(Point(cx, cy));
	}

	public function addLava(cx, cy) {
		var c = Point(cx, cy);
		var pe = new ProtoEntity("Lava");
		TEMP_TILES.set(c, pe);
		pe.cx = cx;
		pe.cy = cy;
		lava.set(c, true);
		systems.Heat.ME.addTile(pe);
		solver.set(cx, cy, false);

		if (null == painter)
			return;

		var b = new LevelPainter.Biome(hasAntiMagic(cx, cy), false, LevelPainter.TA_biome.Lava, LevelPainter.TA_biome.Empty);
		painter.set(cx, cy, b);
	}

	public function removeLava(cx, cy) {
		var c = Point(cx, cy);
		var pe = TEMP_TILES.get(c);
		TEMP_TILES.remove(c);
		pe.destroy();
		lava.remove(Point(cx, cy));
		systems.Heat.ME.removeTile(pe);
	}

	public function hasLava(cx, cy) {
		return lava.exists(Point(cx, cy));
	}

	public function addBonus(s: ent.Bonus) {
		bonuses.set(Point(s.cx, s.cy), s);
	}

	public function removeBonus(cx, cy) {
		bonuses.remove(Point(cx, cy));
	}

	public function getBonus(cx, cy) {
		return bonuses.get(Point(cx, cy));
	}

	public function setSpellableWall(cx, cy) {
		walls.set(Point(cx, cy), true);
	}

	public function getWallTile(cx, cy) {
		return wallTile.get(Point(cx, cy));
	}

	public function warmUp(cx, cy) {
		if (hasIce(cx, cy)) {
			game.repl.log("Ice[" + cx + "," + cy + "] is turned to water.");
			removeIce(cx, cy);
			addWater(cx, cy);

			invalidated = true;
		}
		else if (hasSolidLava(cx, cy)) {
			game.repl.log("Solid Lava[" + cx + "," + cy + "] is now liquid.");
			removeSolidLava(cx, cy);
			addLava(cx, cy);

			var l = lavaLights.get(Point(cx, cy));
			l.visible = true;

			invalidated = true;
		}
	}

	public function coolDown(cx, cy) {
		if (hasWater(cx, cy)) {
			game.repl.log("Water[" + cx + "," + cy + "] is turned to ice.");
			removeWater(cx, cy);
			addIce(cx, cy);

			invalidated = true;
		}
		else if (hasLava(cx, cy)) {
			game.repl.log("Lava[" + cx + "," + cy + "] is now solid.");
			removeLava(cx, cy);
			addSolidLava(cx, cy);

			var l = lavaLights.get(Point(cx, cy));
			l.visible = false;

			invalidated = true;
		}
	}

	public function checkNeighbours(n : Neighbourhood, f : (cx : Int, cy : Int) -> Bool, cx : Int, cy : Int) {
		var res = switch n {
			case Moore: f(cx+1, cy+1) || f(cx-1, cy-1) || f(cx-1, cy+1) || f(cx+1, cy-1);
			case VonNeumann: false;
		};
		return res || f(cx+1, cy) || f(cx-1, cy) || f(cx, cy+1) || f(cx, cy-1);
	}

	override function onResize() {
		super.onResize();

		painter.resize();
		invalidated = true;
	}

	public function addMark(cx, cy) {
		marks.set(Point(cx, cy), true);

		invalidated = true;
	}

	public function removeMark(cx, cy) {
		marks.remove(Point(cx, cy));

		invalidated = true;
	}

	public function isWalkable(cx, cy) {
		var b = getTriggerable(cx, cy);
		return !(hasCollision(cx, cy) || hasLava(cx,cy) || hasWater(cx, cy) || hasHole(cx, cy)) || (hasHole(cx, cy) && (b == null || !b.isActive));
	}
}
