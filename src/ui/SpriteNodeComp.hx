/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ui;

class SpriteElement {
	public var id : spl.Graphics.SubId;
	public var me : h2d.Object;

	public function new(me, id) {
		this.me = me;
		this.id = id;
	}
}

@:uiComp("sprite-node")
class SpriteNodeComp extends h2d.Object implements h2d.domkit.Object {
	static var SRC = <sprite-node></sprite-node>;

	var subAnchor : h2d.Object;
	var color : Int;

	public function new(c : Int, ?parent) {
		super(parent);

		color = c;

		initComponent();

		subAnchor = new h2d.Object(this);

		drawInterface();
	}

	private function drawInterface() {
	}

	public function resize() {
		drawInterface();
	}

	public function addSpriteChild<T>(p : spl.Token<T>, c : SpriteElement) {
		this.subAnchor.addChild(c.me);
		spl.Graphics.placeObject(p, c.id, c.me);
	}
}
