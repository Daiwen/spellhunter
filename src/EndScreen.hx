/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import ui.Components;

@:uiComp("end-screen")
class EndContainer extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <end-screen>
		</end-screen>;

	public function new(align:h2d.Flow.FlowAlign, ?parent) {
		super(parent);
		initComponent();
	}

}

class EndScreen extends dn.Process {
	var img : h2d.Bitmap;

	var center : h2d.Flow;

	public function new() {
		super(Main.ME);

		createRoot(Boot.ME.uiRoot);
		img = new h2d.Bitmap(hxd.Res.title.toTile(), root);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering

		engine.backgroundColor = 0x000;

		var tf = new h2d.Text(Assets.fontLarge, root);
		tf.text = "Thanks for playing !";

		createChildProcess(
				function(c) {
					// Resize dynamically
					tf.setScale( M.imax(1, Math.floor( stageWid*0.35 / tf.textWidth )) );
					tf.x = Std.int( stageWid*0.5 - tf.textWidth*tf.scaleX*0.5 );
					tf.y = Std.int( stageHei*0.1 - tf.textHeight*tf.scaleY*0.5 );

				}, true
				);

		center = new h2d.Flow(root);
		center.horizontalAlign = center.verticalAlign = Middle;
		onResize();

		var root = new EndContainer(Middle, center);

		Main.ME.repl.log("Thanks for playing!");

		dn.Process.resizeAll();
	}

	override function onDispose() {
		super.onDispose();
	}

	override function onResize() {
		super.onResize();

		img.x = ( stageWid/Const.UI_SCALE*0.5 - img.tile.width*0.5 );
	}

	override function postUpdate() {
		super.postUpdate();
	}

	override function update() {
		super.update();
	}
}

