/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2025 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import dn.Process;
import hxd.Key;
import tools.Action;
import haxe.ds.Option;

import ldtk.Level.NeighbourDir.*;

enum NSUid { Id (level : Int, x : Int, y : Int); }

typedef Save = {
	var curLevelIdx : Int;
	var spellBook : Array<spl.Lang.Spell>;
	var slots : Array<Option<spl.Lang.Spell>>;
	var numberOfSlots: Int;
	var newSpells : Map<NSUid, Bool>;
	var spawn : ldtk.Point;
}

class Game extends Process {
	public static var ME : Game;

	public var control : CustomControl;
	public var fx : Fx;
	public var camera : Camera;
	public var scroller : h2d.Layers;
	var needSort = true;
	public var level : Level;
	public var lights : Lights;
	public var dispells : Dispell;
	public var spawn : ldtk.Point;
	public var hud : ui.Hud;
	public var hero : ent.Hero;

	public var spellCaster : Array<spl.Caster>;
	public var spellState : spl.State;

	public var newSpells : Map<NSUid, Bool> = new Map();

	var mouseX : Float;
	var mouseY : Float;

	var isSightlessWorld : Bool;
	public var world : World;
	public var curLevelIdx : Int;
	public var visited : Map<Level.Coord, Bool> = new Map();
	var maxLevelIdx : Int = 0;

	var target : Option<ldtk.Point>;
	var targetRoot : h2d.Object;

	public var marked(default, null) : Array<ent.comp.Spellable> = new Array();
	var markers : h2d.Object;

	public var repl : tools.Hake;
	public var music = false;

	public function new() {
		super(Main.ME);
		ME = this;

		repl = Main.ME.repl;
		repl.addCommand("level", "Opens a menu to explore the level.", logLevel);

		control = new CustomControl();
		var showControls = function () {
			var actions = new Array();
			for (ab in tools.CustomControl.bindings.keyValueIterator()) {
				var bindings = [];
				for (b in ab.value) {
					var s = [ for (m in b.modifiers) tools.CustomControl.getKeyName(m) ];
					s.push(tools.CustomControl.getKeyName(b.key));
					bindings.push(s.join("+"));
				}
				actions.push(control.getActionName(ab.key) + ": " + bindings.join(" "));
			}
			repl.log(actions.join("\n"));
		};
		repl.addCommand("controls", "List the game controls.", showControls);

		createRootInLayers(Main.ME.root, Const.DP_BG);

		engine.backgroundColor = 0xa2a2a2;

		scroller = new h2d.Layers();
		root.add(scroller, Const.DP_BG);


		markers = new h2d.Object();
		scroller.add(markers, Const.DP_TOP);

		target = None;
		targetRoot = new h2d.Object();
		scroller.add(targetRoot, Const.DP_TOP);

		curLevelIdx = 0;

		isSightlessWorld = Settings.sightless;
		var worldName =	hxd.Res.world.world.entry.getText();

		world = new World( worldName );

		fx = new Fx();
		hud = new ui.Hud();
		new systems.Kinetic();
		new systems.Heat();

		startLevel();

		spellState = new spl.State();

		spellCaster = [];
		for (i in 0...4) {
			spellState.slots[i] = None;
			var sc = new spl.Caster(i);
			sc.isLocked = true;
			spellCaster.push(sc);

			var name = "spell " + (i+1);

			hud.makeSlot(name, function () { return spellCaster[i]; });
			repl.addCommand(name, "Displays the spell equipped in slot " + (i+1) + ".", function () { spellCaster[i].log(); });
		}
		spellCaster[0].isLocked = false;

#if( debug )
		ent.NewSpell.addSpellAndWords(Spell (Collider, Link (Caster), Projectile));
		ent.NewSpell.addSpellAndWords(Spell (Collider, Elemental (Fire), Projectile));
		ent.NewSpell.addSpellAndWords(Spell (Collider, Elemental (Fire), Attached (Ground)));
		ent.NewSpell.addSpellAndWords(Spell (Collider, Physical (Push), Projectile));
		ent.NewSpell.addSpellAndWords(Spell (Collider, Physical (Teleport), Projectile));
		ent.NewSpell.addSpellAndWords(Spell (Collider, Meta (Spell (Collider, Elemental (Fire), Projectile)), Projectile));
		ent.NewSpell.addSpellAndWords(Spell (Collider, Elemental (Ice), Projectile));
		ent.NewSpell.addSpellAndWords(Spell (Trigger, Elemental (Ice), Attached (Ground)));
		ent.NewSpell.addSpellAndWords(Spell (Activer, Elemental (Ice), Attached (Entity (Caster))));
		ent.NewSpell.addSpellAndWords(Spell (Collider, Sequence (Elemental (Ice), Physical (Push)), Projectile));
		ent.NewSpell.addSpellAndWords(Spell (Activer, Elemental (Fire), Attached (Entity (Marked))));
		ent.NewSpell.addSpellAndWords(Spell (Collider, Sequence (Mark, Meta (Spell (Trigger, Physical (Teleport), Attached (Entity (Marked))))), Projectile));
		spellState.newWords.clear();
#end
		new spl.Editor();

		Process.resizeAll();
	}

	public function onCdbReload() {
	}

	private function startLevel() {
		systems.Heat.ME.init();
		systems.Kinetic.ME.init();
		camera = new Camera();
		var l = world.all_worlds.Default.levels[curLevelIdx];
		level = new Level(l);
		lights = new Lights();
		dispells = new Dispell();

		ChucK.setInt("seed", curLevelIdx);
		ChucK.broadcastEvent("intro");

		var x = M.round(l.worldX / l.pxWid);
		var y = M.round(l.worldY / l.pxHei);
		var k = Level.Coord.Point(x, y);
		visited.set(k, true);

		hero =
			if (spawn == null) {
				var cy = Std.int(level.hei / 2);
				new ent.Hero(2, cy);
			}
			else
				new ent.Hero(spawn.cx, spawn.cy);
		level.initLevel();
		camera.trackTarget(hero, false);
	}

	public function previousLevel() {
		fx.clear();
		spawn = null;

		updateLevelFromDir(West);

		restartLevel();
	}
	
	public function updateLevel() {
		var dir = South;
		var nx = hero.cx;
		var ny = hero.cy;

		if (hero.cx == 0) {
			dir = West;
			nx = level.wid-2;
		} else if (hero.cx == level.wid-1) {
			dir = East;
			nx = 1;
		} else if (hero.cy == 0) {
			dir = North;
			ny = level.hei-2;
		} else {
			dir = South;
			ny = 1;
		}

		updateLevelFromDir(dir);

		spawn = new ldtk.Point(nx, ny);

		save();

		restartLevel();
	}

	function updateLevelFromDir(dir) {
		var iid = world.all_worlds.Default.levels[curLevelIdx].iid;
		for (n in level.level.neighbours) {
			if ( n.dir == dir ) {
				iid = n.levelIid;
				break;
			}
		}

		for (i in 0...world.all_worlds.Default.levels.length) {
			if (world.all_worlds.Default.levels[i].iid == iid) {
				curLevelIdx = i;
				break;
			}
		}
	}

	public function nextLevel() {
		fx.clear();
		spawn = null;

		updateLevelFromDir(East);

		restartLevel();
	}

	public function restartLevel(){
		fx.clear();
		for(e in Entity.ALL)
			e.destroy();
		hero.destroy();
		level.destroy();
		lights.destroy();
		dispells.destroy();
		camera.destroy();
		all_gc();
		startLevel();
	}

	function e_gc() {
		if( vfx.Effect.GC==null || vfx.Effect.GC.length==0 )
			return;

		for(e in vfx.Effect.GC)
			e.dispose();
		vfx.Effect.GC = [];
	}

	function ec_gc() {
		if( ent.comp.EntityComponent.GC==null || ent.comp.EntityComponent.GC.length==0 )
			return;

		for(e in ent.comp.EntityComponent.GC)
			e.dispose();
		ent.comp.EntityComponent.GC = [];
	}


	function gc() {
		if( Entity.GC==null || Entity.GC.length==0 )
			return;

		for(e in Entity.GC)
			e.dispose();
		Entity.GC = [];
	}

	function s_gc() {
		if( systems.System.GC==null || systems.System.GC.length==0 )
			return;

		for(s in systems.System.GC)
			s.dispose();
		systems.System.GC = [];
	}

	function all_gc() {
		gc();
		ec_gc();
		e_gc();
		s_gc();
	}

	override function onDispose() {
		super.onDispose();

		fx.destroy();
		for(e in Entity.ALL)
			e.destroy();
		all_gc();
	}

	function updateMouse() {
		var gx = hxd.Window.getInstance().mouseX;
		var gy = hxd.Window.getInstance().mouseY;
		mouseX = gx/Const.SCALE - scroller.x;
		mouseY = gy/Const.SCALE - scroller.y;

		hud.invalidate();
	}

	override function preUpdate() {
		super.preUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.preUpdate();
		for(e in ent.comp.EntityComponent.ALL) if( !e.destroyed ) e.preUpdate();
		for(s in systems.System.ALL) if( !s.destroyed ) s.preUpdate();
	}

	function showTarget() {
		targetRoot.removeChildren();

		if (!Settings.assistedAiming)
			return;

		switch(target) {
			case None:
			case Some (t):
				var color = 0xffffff;
				var g = new h2d.Graphics(targetRoot);
				g.beginFill(color, 0.);
				g.lineStyle(2, color);
				g.drawRect(t.cx * Const.GRID, t.cy * Const.GRID, Const.GRID, Const.GRID);
				g.endFill();
		}
	}

	override function postUpdate() {
		super.postUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.postUpdate();
		for(e in ent.comp.EntityComponent.ALL) if( !e.destroyed ) e.postUpdate();
		for(s in systems.System.ALL) if( !s.destroyed ) s.postUpdate();
		all_gc();

		if (level.hasEndZone(hero.cx, hero.cy)) {
			if (isSightlessWorld) {
				isSightlessWorld = false;
				world = new World( hxd.Res.world.world.entry.getText() );
				maxLevelIdx = 0;
				curLevelIdx = 0;
				spawn = null;
				restartLevel();
			}
			else
				Main.ME.end();
		}

		if (level.shouldChangeLevel(hero.cx, hero.cy)) {
			repl.log("Loading level");
			updateLevel();
		}

		showTarget();	

		Boot.ME.postRoot.x = scroller.x;
		Boot.ME.postRoot.y = scroller.y;
	}

	override function fixedUpdate() {
		super.fixedUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.fixedUpdate();
	}

	override function update() {
		super.update();

		if (needSort) {
			needSort = false;
			scroller.ysort(Const.DP_MAIN);
		}

		updateMouse();

		for(e in Entity.ALL) if( !e.destroyed ) e.update();
		for(e in ent.comp.EntityComponent.ALL) if( !e.destroyed ) e.update();
		for(e in vfx.Effect.ALL) if( !e.destroyed ) e.update();
		for(s in systems.System.ALL) if( !s.destroyed ) s.update();

		if( control.probe(Exit) ) {
			new PauseMenu();
		}

		if(!hero.isDead && control.probe(Slot1)) {
			spellCaster[0].trigger(hero.footX, hero.footY);
		}

		if(!hero.isDead && control.probe(Slot2)) {
			spellCaster[1].trigger(hero.footX, hero.footY);
		}

		if(!hero.isDead && control.probe(Slot3)) {
			spellCaster[2].trigger(hero.footX, hero.footY);
		}

		if(!hero.isDead && control.probe(Slot4)) {
			spellCaster[3].trigger(hero.footX, hero.footY);
		}

		if(Key.isPressed(Key.B)) {
			previousLevel();
		}

		if(Key.isPressed(Key.N)) {
			nextLevel();
		}

		if(control.probe(Map)) {
			new MapMenu();
		}

		if(Key.isPressed(Key.P)) {
			Assets.getVolume((v) -> trace("volume: " + v));
		}

#if( debug )
		if(Key.isPressed(Key.I)) {
			openAltar();
		}
#end

		if(control.probe(Interact)) {
			var canEdit = level.checkNeighbours(Level.Neighbourhood.Moore, level.hasAltar, hero.cx, hero.cy);
			if (canEdit) {
				openAltar();
			} else {
				openSpellBook();
			}
		}

		var aimX = getX();
		var aimY = getY();
		switch [aimX, aimY] {
			case [Some (x), Some (y)]: updateTarget(Std.int(x), Std.int(y));
			case [None, Some (y)]: updateTarget(0, Std.int(y));
			case [Some (x), None]: updateTarget(Std.int(x), 0);
			default:
		}

		if(control.probe(SelectAim)) {
			logTarget();
		}
	}

	private function getY() {
		switch [control.cprobe(AimDown), control.cprobe(AimUp)] {
			case [Some (v), _]: return Some (v);
			case [_, Some (v)]: return Some (-v);
			default: return None;
		}
	}

	private function getX() {
		switch [control.cprobe(AimRight), control.cprobe(AimLeft)] {
			case [Some (v), _]:	return Some (v);
			case [_, Some (v)]:	return Some (-v);
			default: return None;
		}
	}

	public function openAltar() {
		for (sc in spellCaster)
			sc.purge();
		all_gc();
		spl.Editor.ME.openAltar(spellState);
	}

	public function openSpellBook() {
		all_gc();
		spl.Editor.ME.openSpellbook(spellState);
	}

	public function mark(es : Array<ent.comp.Spellable>) {
		if (hero != null)
			es.remove(hero.spellable);

		for (e in marked)
			e.unmark();

		marked = es;

		for (e in marked)
			e.mark();
	}

	public function unmark(e : ent.comp.Spellable) {
		marked.remove(e);
	}

	public function updateTarget(x : Int, y : Int) {
		target =
			switch (target) {
				case Some (t) :
					var ncx = t.cx + x;
					var ncy = t.cy + y;

					if (level.isValidX(ncx))
						t.cx = ncx;

					if (level.isValidY(ncy))
						t.cy = ncy;

					if (Settings.assistedAiming) {
						repl.log("Cursor at [" + t.cx + "," + t.cy + "]");

						var entities = ProtoEntity.ALL.filter(function (e) {return e.cx == t.cx && e.cy == t.cy;});

						for (e in entities) {
							repl.log(e.getLog());
						}
					}
					Some (t);
				case None: Some (new ldtk.Point(0, 0));
			}
	}

	public function getTarget() {
		return
			switch (target) {
				case Some (t) if (Settings.assistedAiming):
					new h2d.col.Point((t.cx + 0.5) * Const.GRID, (t.cy + 0.5) * Const.GRID);
				default: new h2d.col.Point(mouseX, mouseY);
			}
	}

	public function logTarget() {
		switch (target) {
			case Some (t):
				var entities = ProtoEntity.ALL.filter(function (e) {return e.cx == t.cx && e.cy == t.cy;});

				var menus = new Array();

				var back = {
					label : "Back",
					select : function () {},
				};

				menus.push(back);

				for (e in entities) {
					var o = {
						label : e.getName(),
						select : e.details,
					}

					menus.push(o);
				}

				repl.menu(menus);
			default:
		}
	}

	public function logLevel() {
		var entities = new Map();

		for (e in ProtoEntity.ALL) {
			var es = entities.get(e.getName());

			if (es == null)
				es = new Array();

			es.push(e);

			entities.set(e.getName(), es);
		}

		var menus = new Array();

		for (i in entities.keyValueIterator()) {
			var locations = [ for (e in i.value) "[" + e.cx + ", " + e.cy + "]" ];
			var o = {
				label : i.key,
				select : function () {
					repl.log(locations.join(" "));
				},
			}

			menus.push(o);
		}

		var exits = new Array();

		for (i in 0...level.wid) {
			if (!level.hasCollision(i, 0)) {
				exits.push(new ldtk.Point(i, 0));
			}

			if (!level.hasCollision(i, level.hei - 1)) {
				exits.push(new ldtk.Point(i, level.hei - 1));
			}
		}

		for (i in 0...level.hei) {
			if (!level.hasCollision(0, i)) {
				exits.push(new ldtk.Point(0, i));
			}

			if (!level.hasCollision(level.wid - 1, i)) {
				exits.push(new ldtk.Point(level.wid - 1, i));
			}
		}

		var exitLocations = [for (e in exits) "[" + e.cx + ", " + e.cy + "]"];
		var o = {
			label : "Exits",
			select : function () {
				repl.log(exitLocations.join(" "));
			},
		}
		menus.push(o);

		if (marked.length > 1) {
			var es = [for (e in marked) e.me.getLocatedName()];
			repl.log(es.join(", ") + " are marked.");
		}
		else if (marked.length == 1) {
			repl.log(marked[0].me.getLocatedName() + " is marked.");
		}

		repl.menu(menus);
	}

	private function getCurrentState() {
		var slots = spellState.slots;
		var nb = 0;
		for ( c in spellCaster ) {
			if (!c.isLocked) {
				nb++;
			} else {
				break;
			}
		}
		var s = {
			curLevelIdx : curLevelIdx
		  , spellBook : [ for (k in spellState.book.keys()) k ]
		  , slots : slots
		  , numberOfSlots : nb
		  , newSpells : newSpells
		  , spawn : spawn
		}

		return s;
	}

	public function save() {
		var s = getCurrentState();

		hxd.Save.save(s);
	}	

	public function load() {
		var cs = getCurrentState();
		var s : Save = hxd.Save.load(cs);

		curLevelIdx = s.curLevelIdx;

		for (s in s.spellBook) {
			ent.NewSpell.addSpellAndWords(s);
		}
		spellState.slots = s.slots;
		newSpells = s.newSpells;
		spawn = s.spawn;

		for ( i in s.numberOfSlots...spellCaster.length ) {
			spellCaster[i].isLocked = true;
		}

		restartLevel();
	}

	override public function resume() {
		super.resume();

		ChucK.broadcastEvent("game");
	}

	public function sortEntities() {
		needSort = true;
	}
}
