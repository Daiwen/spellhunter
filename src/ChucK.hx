extern class ChucK {
	static function init(cont : () -> Void) : Void;
	static function broadcastEvent(v : String) : Void;
	static function setFloat(variable : String, value : Float) : Void;
	static function getFloat(variable : String, f : (Float) -> Void) : Void;
	static function setInt(variable : String, value : Int) : Void;
	static function setString(variable : String, value : String) : Void;
}
