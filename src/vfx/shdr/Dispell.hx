/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class Dispell extends hxsl.Shader {
	static var colors = [ for (i in [0x79acb9, 0x3a562d, 0xa85959, 0x653934]) h3d.Vector4.fromColor(i) ];
    static var SRC = {
		@:import h3d.shader.Base2d;

        @global var noise: Sampler2D;
		@global var mask : Sampler2D;
		@global var dmask : Sampler2D;

		@param var maskColors : Array<Vec4, 4>;

		@global var altColor : Sampler2D;
		@param var period : Float;

		function fragment() {
			var i = int(mod(floor(time/period), 4));
			var color = maskColors[i];
			var maskColor = mask.get(calculatedUV);
			var dmaskColor = dmask.get(calculatedUV);

			if (color.x == maskColor.x && color.y == maskColor.y && color.z == maskColor.z && dmaskColor.x == 1) {
				pixelColor = altColor.get(calculatedUV);
				pixelColor.w = dmaskColor.y;
			}
			else
				pixelColor = vec4(0);
		}
    }


	public function new() {
		super();

		maskColors = colors;

		period = 0.25;
	}
}
