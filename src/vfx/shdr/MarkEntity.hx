/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class MarkEntity extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

		@global var offset : Vec2;
		@global var resolution : Vec2;
		@global var screenSize : Vec2;
        @global var noise : Sampler2D;

		@param var color : Vec4;
		@param var flowSpeed : Float;
		@param var noiseSize : Vec2;

		@const var killAlpha : Bool;


		function toTexture(x : Vec2) : Vec2 {
			var t0 = offset/screenSize;
			var t1 = (offset+resolution)/screenSize;
			var tdiff = t1 - t0;

			return (1/tdiff)*x-(t0/tdiff);
		}


		function fragment() {
			var base = (outputPosition.xy + vec2(1)) * 0.5;
			var offset = floor(noiseSize * flowSpeed * time)/noiseSize;
			var blur1 = noise.get(toTexture(base+offset));
			var blur2 = noise.get(toTexture(base-offset));
			var blur = (blur1.x + blur2.x)/2;


			output.color.xyz = pixelColor.xyz + 0.9 * blur * blur * color.xyz;
			output.color.a = pixelColor.a;
		}
	};


	public function new() {
		super();

		color.setColor(0xfc0fc0);
		color.w = 1;

		noiseSize = new h3d.Vector(Assets.noiseTexture.width, Assets.noiseTexture.height, 0);


		flowSpeed = .02;
	}
}
