/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class DrawDMask extends hxsl.Shader {
	static var SRC = {
		@var var outputPosition : Vec4;

		@param var radius : Float;
		@param var center : Vec2;

		@global var screenSize : Vec2;

		var dmask : Vec4;

        function fragment() {
			var pos = vec4((outputPosition.xy + vec2(1)) * 0.5, 0, 1);

			var c = pos.xy * screenSize - center;
			var r = sqrt(c.x*c.x + c.y*c.y);

			if (radius > r) {
				dmask = vec4(1, 1-(r/radius), 1, 1);
			} else {
				dmask = vec4(0);
			}
        }
	}

	public function new(centerX : Float, centerY : Float) {
		super();

		center.x = centerX;
		center.y = centerY;
	}
}
