/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx;

class Shockwave extends Effect {
	var startTime: Float;
	var radius: Float;
	var shader: vfx.shdr.Shockwave;

	override public function new(o : h2d.Object, x : Float, y : Float, lifeSpan: Float, r: Float, s : String) {
		super(o, x, y, s);

		startTime = tmod;

		cd.setS("life", lifeSpan, destroy);

		radius = r;
		shader = new vfx.shdr.Shockwave(x, y);
		applyEffect();

	}

	override private function applyEffect() {
		root.removeChildren();

		var g = new h2d.Graphics(root);
		g.beginFill(0xffffff, 0.);
		g.lineStyle(5, 0x000000);
		g.drawCircle(0, 0, 2*radius);
		g.endFill();

		if (Settings.shaders) {
			g.addShader(shader);
		}

		updateRadius();
	}

	private function updateRadius() {
		var r = (1 - cd.getRatio("life")) * radius * 2;
	    shader.radius = r;
	}

	override public function update() {
		super.update();

		updateRadius();
	}
}
