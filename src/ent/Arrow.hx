/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;
import haxe.ds.Option;

class Arrow extends Entity {
	var spellable : ent.comp.Spellable;
	var collidable : ent.comp.Collidable;
	var elementable : ent.comp.Elementable;

	public function new(x, y, target, name : String) {
		super(x, y, "Arrow");

		ChucK.broadcastEvent("f_throwArray");

		canCollide = false;

		frict = 1;
		bumpFrict = 1;

		var mX = target.cx;
		var mY = target.cy;
		var hX = x;
		var hY = y;

		var dist = M.dist(mX, mY, hX, hY);
		dx = Settings.speed * 0.5 * (mX-hX)/dist;
		dy = Settings.speed * 0.5 * (mY-hY)/dist;

		var offsetX = dx * Const.SPELL_OFFSET + xr;
		var offsetY = dy * Const.SPELL_OFFSET + yr;

		var icx = Std.int(offsetX/Const.GRID);
		var icy = Std.int(offsetY/Const.GRID);

		cx += icx;
		cy += icy;
		xr = offsetX - icx;
		yr = offsetY - icy;

		spr.set(name);

		spellable = new ent.comp.Spellable(this);
		elementable = new ent.comp.Elementable(spellable);
		elementable.onCool = function(s) {
			switch s {
				case Ice : makeBroken();
				default:
			}
		}
		collidable = new ent.comp.Collidable(this, Soft, true, true);
		collidable.onCheck = shouldBreak;

		collidable.onCollide = function(collided) {
			var solids = collided.filter(function (ec) { return ec.kind == Solid; });

			if (solids.length != 0) {
				game.repl.log( getLocatedName() + " collided with " + solids[0].me.getLocatedName() );
				fx.brokenBits(this.cx, this.cy, -dx, -dy, 0.98, 0x381d1f);
				makeBroken();
			}
		};

		isDead = false;

		systems.Kinetic.ME.addEntity(collidable);
	}

	private function shouldBreak() {
		if (!isDead && !level.isValid(cx, cy)) {
			game.repl.log( getLocatedName() + " exited the level");
			makeBroken();
		}

		if (!isDead && level.hasCollision(cx, cy)) {
			game.repl.log( getLocatedName() + " collided with a Wall at [" + cx + "," + cy + "]");
			makeBroken();
		}
	}

	override function preUpdate() {
		super.preUpdate();

		if (isDead) {
			cancelVelocities();
		}
	}

	override function update() {
		super.update();
	}

	override function postUpdate() {
		super.postUpdate();

		if (!isDead && (!level.isValid(cx, cy) || level.hasCollision(cx,cy) )) {
			game.repl.log( getLocatedName() + " collided with Wall at [" + cx + "," + cy + "]" );
			makeBroken();
		}

		if (!isDead && Math.abs(dxTotal) <= 0.01 && Math.abs(dyTotal) <= 0.01) {
			makeBroken();
		}

		if (isDead)
			spr.alpha *= Math.pow(0.95, tmod);

		if (isDead && !cd.has("broken"))
			destroy();

		if (!isDead) {
			var angle = Math.atan2(dyTotal, dxTotal);
			spr.rotation = angle;
		}
	}

	private function makeBroken() {
		if (isDead)
			return;

		ChucK.broadcastEvent("f_brokenArrow");
		game.repl.log(getLocatedName() + " broke");
		isDead = true;
		cd.setS("broken", 0.5);
	}

	override function dispose() {
		super.dispose();

		systems.Kinetic.ME.removeEntity(collidable);
		elementable.destroy();
		spellable.destroy();
		collidable.destroy();
	}

	override public function getElementable() {
		return Some (elementable);
	}

	override function cancelVelocities() {
		super.cancelVelocities();

		if(!isDead)
			makeBroken();
	}
}
