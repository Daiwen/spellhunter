/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;
import haxe.ds.Option;

class WoodWall extends Entity {

	var elementable : ent.comp.Elementable;
	var spellable : ent.comp.Spellable;
	var repulsor : ent.comp.Repulsor;
	var collidable : ent.comp.Collidable;

	private var markEffect : h2d.Graphics;

	public function new (x, y) {
		super(x, y, "Wood wall");

		isTileEntity = true;
		isFixed = true;

		level.addCollision(x, y);
		level.setSpellableWall(x, y);

		spellable = new ent.comp.Spellable(this);
		elementable = new ent.comp.Elementable(spellable);
		collidable = new ent.comp.Collidable(this, Solid, false, true);
		repulsor = new ent.comp.Repulsor(collidable, 2);

		spr.set(dict.Empty);
	}


	override function update() {
		super.update();
	}

	override function dispose() {
		super.dispose();

		level.removeCollision(this.cx, this.cy);
		elementable.destroy();
		spellable.destroy();
		collidable.destroy();
		repulsor.destroy();
		unmark();
	}

	override public function unmark() {
		level.removeMark(cx, cy);
	}

	override public function mark() {
		level.addMark(cx, cy);
	}

	override public function getElementable() {
		return Some (elementable);
	}
}
