/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;

class Switch extends Entity implements Steppable {
	var targets : Array<ldtk.Point>;

	var switchers : Array<Entity>;
	var isOn : Bool;

	var interactive : h2d.Interactive;

	public function new (x, y, targets) {
		super(x, y, "Pressure switch");
		level.addTrigger(this);

		isTileEntity = true;

		this.targets = targets;

		switchers = new Array();
		isOn = true;

		spr.set(dict.SwitchOn);
		game.scroller.add(spr, Const.DP_BG);

		interactive = new h2d.Interactive(Const.GRID * Const.SCALE, Const.GRID * Const.SCALE, Boot.ME.uiRoot);
		interactive.x = (centerX - Const.GRID/2 + game.scroller.x) * Const.SCALE;
		interactive.y = (centerY - Const.GRID/2 + game.scroller.y) * Const.SCALE;

		interactive.onOver = function(_) {
			for (t in targets) {
				var t = level.getTriggerable(t.cx, t.cy);
				t.highlight();
			}
		};
		interactive.onOut = function(_) {
			for (t in targets) {
				var t = level.getTriggerable(t.cx, t.cy);
				t.downlight();
			}
		};

		proto.details = function () {
			game.repl.log(getLocatedName() + " activates:");
			for (t in targets) {
				var t = level.getTriggerable(t.cx, t.cy);
				t.log();
			}
		};
	}


	public function activate(m:Entity) {
		if (switchers.length == 0) {
			isOn = !isOn;
			if (isOn) {
				game.repl.log(getLocatedName() + " is on");
			} else {
				game.repl.log(getLocatedName() + " is off");
			}

			ChucK.broadcastEvent("f_button");
			spr.set(isOn ? dict.SwitchOn : dict.SwitchOff);
			for (t in targets) {
				var t = level.getTriggerable(t.cx, t.cy);
				if (t != null)
					t.toggle();
			}
		}

		if (!switchers.contains(m))
			switchers.push(m);
	}

	override function update() {
		super.update();

		if (switchers.length != 0) {
			switchers = switchers.filter(function (e) { return e.cx == cx && e.cy == cy; });
		}

	}


	override function dispose() {
		super.dispose();

		level.removeTrigger(this.cx, this.cy);
		interactive.remove();
	}
}
