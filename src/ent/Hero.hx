/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;
import hxd.Key;
import haxe.ds.Option;

enum State {
	Stand;
	Run;
	Cast;
}

enum Dir {
	Left;
	Right;
	Face;
	Back;
}

class Hero extends Entity {
	public static var ME : Null<Hero>;

	final anims = dn.heaps.assets.Aseprite.getDict(hxd.Res.atlas.hero);
	var waterDeath: HSprite;
	var ca : dn.legacy.Controller.ControllerAccess;

	var state: State;
	var dirState: Dir;

	var elementable : ent.comp.Elementable;
	var agent : ent.comp.Agent;
	public var spellable(default, null) : ent.comp.Spellable;
	public var collidable : ent.comp.Collidable;
	var repulsor : ent.comp.Repulsor;
	var displayable : ent.comp.Displayable;

	var lineOfSight : h2d.Graphics = null;

	private function isRunning() {
		var res =
		switch (state) {
			case Run: true;
			default: false;
		};

		return res;	
	}

	private function isStanding() {
		var res =
		switch (state) {
			case Stand: true;
			default: false;
		};

		return res;	
	}

	private function isCasting() {
		var res =
		switch (state) {
			case Cast: true;
			default: false;
		};

		return res;	
	}

	private function isFace() {
		var res =
		switch (dirState) {
			case Face: true;
			default: false;
		};

		return res;	
	}

	private function isLeft() {
		var res =
		switch (dirState) {
			case Left: true;
			default: false;
		};

		return res;	
	}

	private function isRight() {
		var res =
		switch (dirState) {
			case Right: true;
			default: false;
		};

		return res;	
	}

	private function isBack() {
		var res =
		switch (dirState) {
			case Back: true;
			default: false;
		};

		return res;	
	}

	private function updateLineOfSight() {
		if (lineOfSight != null)
			lineOfSight.remove();
		if (isDead)
			return;
		lineOfSight = new h2d.Graphics();
		Game.ME.scroller.add(lineOfSight, Const.DP_BG);

		var sightLength = Const.GRID/2;
		var sightOffset = Const.GRID/3;
		var color = 0x392b52;
		var t = game.getTarget();
		var angle = Math.atan2(t.y - centerY, t.x - centerX);

		var centerOffset = -5;
		var startX = centerX + sightOffset * Math.cos(angle);
		var startY = centerY - centerOffset + sightOffset * Math.sin(angle);

		lineOfSight.lineStyle(1, color);
		lineOfSight.moveTo(startX, startY);
		lineOfSight.lineTo(centerX + (sightOffset + sightLength) * Math.cos(angle), centerY - centerOffset + (sightOffset + sightLength) * Math.sin(angle));
	}

	public function new (x, y) {
		super(x, y, "Hero");

		waterDeath = new HSprite(Assets.hero);
		waterDeath.setCenterRatio(0.5, 0.5);

		xr = 0.5;
		yr = 0.5;

		collisionEShape = Entity.Shape.Segment;

		ME = this;

		state = Stand;
		dirState = Right;
		spellable = new ent.comp.Spellable(this);
		elementable = new ent.comp.Elementable(spellable);
		agent = new ent.comp.Agent(this);
		collidable = new ent.comp.Collidable(this, Solid, false, true);
		repulsor = new ent.comp.Repulsor(collidable, 0);
		displayable = new ent.comp.Displayable(this);

		spr.set(Assets.hero);
		spr.anim.registerStateAnim(anims.IdleFace, 1, () -> isStanding() && isFace());
		spr.anim.registerStateAnim(anims.IdleLeft, 1, () -> isStanding() && isLeft());
		spr.anim.registerStateAnim(anims.IdleBack, 1, () -> isStanding() && isBack());
		spr.anim.registerStateAnim(anims.IdleRight, 1, () -> isStanding() && isRight());

		spr.anim.registerStateAnim(anims.RunFace, 1, () -> isRunning() && isFace());
		spr.anim.registerStateAnim(anims.RunLeft, 1, () -> isRunning() && isLeft());
		spr.anim.registerStateAnim(anims.RunBack, 1, () -> isRunning() && isBack());
		spr.anim.registerStateAnim(anims.RunRight, 1, () -> isRunning() && isRight());

		spr.anim.registerStateAnim(anims.CastProjectileFace, 1, () -> isCasting() && isFace());
		spr.anim.registerStateAnim(anims.CastProjectileLeft, 1, () -> isCasting() && isLeft());
		spr.anim.registerStateAnim(anims.CastProjectileBack, 1, () -> isCasting() && isBack());
		spr.anim.registerStateAnim(anims.CastProjectileRight, 1, () -> isCasting() && isRight());

		spr.anim.registerStateAnim(anims.WaterDeath, 99, () -> this.isDead && level.hasWater(cx, cy) || level.hasLava(cx, cy));
		spr.anim.registerStateAnim(anims.BurningDeath, 99, () -> this.isDead && elementable.state == Fire);
		spr.anim.registerStateAnim(anims.IcedDeath, 99, () -> this.isDead && elementable.state == Ice);
		spr.anim.registerStateAnim(anims.DeathLeft, 99, () -> this.isDead && isLeft());
		spr.anim.registerStateAnim(anims.DeathRight, 98, () -> this.isDead);

		updateLineOfSight();

		isDead = false;

		systems.Kinetic.ME.addEntity(collidable);
	}

	override function dispose() {
		super.dispose();

		systems.Kinetic.ME.removeEntity(collidable);

		elementable.destroy();
		spellable.destroy();
		collidable.destroy();
		repulsor.destroy();
		agent.destroy();
		displayable.destroy();

		lineOfSight.remove();

		waterDeath.remove();

	}

	private function getY() {
		switch [game.control.cprobe(Down), game.control.cprobe(Up)] {
			case [Some (v), _]: return v;
			case [_, Some (v)]: return -v;
			default: return 0.;
		}
	}

	private function getX() {
		switch [game.control.cprobe(Right), game.control.cprobe(Left)] {
			case [Some (v), _]:	return v;
			case [_, Some (v)]:	return -v;
			default: return 0.;
		}
	}

	override function update() {
		super.update();

		updateLineOfSight();

		if (isDead)
			return;

		var dirx = getX();
		var diry = getY();

		dirx /= Math.sqrt(dirx * dirx + diry * diry);
		diry /= Math.sqrt(dirx * dirx + diry * diry);


		state =
			if (M.dist(0, 0, dirx, diry) > 0.05) {

				if (Settings.discrete) {
					if (!cd.hasSetS("move", 0.25)) {
						var ncx = cx + Math.round(dirx);
						var ncy = cy + Math.round(diry);
						if (!level.hasCollision(ncx, ncy)) {
							cx = ncx;
							cy = ncy;
						}
					}
				} else {
					systems.Kinetic.ME.setSpeed(collidable, dirx * Settings.speed * 0.1, diry * Settings.speed * 0.1);
				}
					if (!cd.has("cast"))
						Run;
					else
						state;
			} else {
				if (!cd.has("cast"))
					Stand;
				else
					state;
			}


		if (dirx > 0.3)
			dirState = Right;
		else if (dirx < -0.3)
			dirState = Left;

		if (diry > 0.3)
			dirState = Face;
		else if (diry < -0.3)
			dirState = Back;

		var ns = level.getBonus(cx, cy);
		if (ns != null) {
			ChucK.broadcastEvent("f_pickup");
			ns.capture();
		}

		debug(Std.int(hxd.Timer.fps())+" tmod="+pretty(tmod,2));

	}

	public function makeCollideDead(e : Entity){
		if (!isDead) {
			if (cx + xr < e.cx + e.xr) {
				fx.bleed(centerX, centerY, -1.);
				dirState = Left;
			}
			else {
				fx.bleed(centerX, centerY, 1.);
				dirState = Right;
			}
		}

		game.repl.log( getLocatedName() + " was shot dead." );

		makeDead();
	}

	public function makeDead(){
		if (!isDead) {
			ChucK.broadcastEvent("f_death");
			var b = level.getTriggerable(cx, cy);
			if (level.hasHole(cx, cy) && (b == null || !b.isActive)) {
				xr = 0.5;
				yr = 0.5;
			}
			isDead = true;
			cd.setS("dead", 2);
		}
	}

	override function postUpdate() {
		super.postUpdate();

		if (!isDead && state == Run && !cd.hasSetS("sfxRun", .5))
			ChucK.broadcastEvent("f_walk");

		var b = level.getTriggerable(cx, cy);
		if (!isDead &&
				(!level.isValid(cx, cy) ||
				 level.hasLava(cx, cy) ||
				 level.hasWater(cx, cy) ||
				 elementable.state == Fire ||
				 elementable.state == Ice ||
				 level.hasHole(cx, cy) && (b == null || !b.isActive))) {


			if (level.hasLava(cx, cy) || level.hasWater(cx, cy)) {
				level.dyns.addChild(waterDeath);
				waterDeath.x = cx * Const.GRID + xr;
				waterDeath.y = cy * Const.GRID + yr;
				waterDeath.anim.play(anims.WaterRings);
			}


			if (level.hasLava(cx, cy))
				game.repl.log( getLocatedName() + " fell in lava." );
			else if	(level.hasWater(cx, cy))
				game.repl.log( getLocatedName() + " fell in water." );
			else if (elementable.state == Fire)
				game.repl.log( getLocatedName() + " was burnt to death." );
			else if	(elementable.state == Ice)
				game.repl.log( getLocatedName() + " was frozen to death." );
			else if (level.hasHole(cx, cy) && (b == null || !b.isActive))
				game.repl.log( getLocatedName() + " fell in a hole." );
			else
				game.repl.log( getLocatedName() + " reached an invalid state." );



			makeDead();
		}

		if (isDead && !cd.has("dead"))
			game.restartLevel();

		if (isDead && level.hasHole(cx, cy) && (b == null || !b.isActive)) {
			sprScaleX *= Math.pow(0.95, tmod);
			sprScaleY = sprScaleX;
		}

		if (oldCx != cx || oldCy != cy) {
			game.repl.log("Hero at [" + cx + "," + cy + "]");
		}
	}

	override public function getElementable() {
		return Some (elementable);
	}

	public function castProjectile() {
		state = Cast;
		cd.setS("cast", 0.5);
	}
}
