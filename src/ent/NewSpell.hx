/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;

import spl.Token;

class NewSpell extends Entity implements Bonus {
	var spell : spl.Lang.Spell;
	var nsuid : Game.NSUid;

	public function new (x, y, uid, s : spl.Lang.Spell) {
		super(x, y, "New Spell");
		
		level.addBonus(this);

		isTileEntity = true;

		spell = s;
		this.nsuid = uid;

		spr.anim.registerStateAnim("NewSpell", 1, 0.2);
	}

	public function capture(){
		game.repl.log("Picked up " + getLocatedName());
		destroy();
		level.removeBonus(cx, cy);
		addSpellAndWords(spell);
		game.newSpells.set(nsuid, true);
		game.spellState.slots[0] = Some (spell);
		game.spellCaster[0].refresh();
		game.openSpellBook();
	}

	static public function addSpellAndWords(s) {
		var words = getSpellWords(s);
		words.push(AnyNamedSpell (s));
		for (w in words) {
			var label = Util.getLabel(w);

			if (!Game.ME.spellState.allWords.exists(label)) {
				Game.ME.spellState.newWords.set(label, true);
				Game.ME.spellState.allWords.set(Util.getLabel(w), w);
			}
		}
		Game.ME.spellState.book.set(s, true);

		return;
	}


	static private function getSpellWords(s) {
		var word = AnySpellData (LangToToken.spell(s));
		var acc = [ word ];
		switch s {
			case Spell(k, e, mk):
				acc = acc.concat(getKindWords(k));
				acc = acc.concat(getEffectWords(e));
				acc = acc.concat(getMotionKindWords(mk));
		}
		return acc;
	}

	static private function getKindWords(k) {
		return [ AnyKindData (LangToToken.kind(k)) ];
	}

	static private function getMotionKindWords(k) {
		var word = AnyMotionKindData (LangToToken.motionKind(k));
		var acc = [ word ];
		switch k {
			case Projectile | Attached (Ground):
			case Attached (Entity (ei)):
				acc = acc.concat(getEntityIdWords(ei));
		}
		return acc;
	}

	static private function getEntityIdWords(ei) {
		return [ AnyEntityIdData (LangToToken.entityId(ei)) ];
	}

	static private function getEffectWords(e) {
		var word = AnyEffectData (LangToToken.effect(e));
		var acc = [ word ];
		switch e {
			case Sequence (e1, e2):
				acc = acc.concat(getEffectWords(e1));
				acc = acc.concat(getEffectWords(e2));
			case Meta (s):
				acc = acc.concat(getSpellWords(s));
			case Link (ei):
				acc = acc.concat(getEntityIdWords(ei));
			case Mark | Physical (_) | Elemental (_) :
		}
		return acc;
	}
}
