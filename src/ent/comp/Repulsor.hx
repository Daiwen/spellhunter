/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2023 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent.comp;

class Repulsor extends EntityComponent {
	var mass : Int;
	var collidable : Collidable;

	public function new (c : Collidable, m) {
		super(c.me);
		this.mass = m;
		this.collidable = c;

		systems.Kinetic.ME.addRepulsor(c, m);
	}

	override function dispose() {
		super.dispose();

		systems.Kinetic.ME.removeRepulsor(collidable);
	}
}
