/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent.comp;

import haxe.ds.Option;

class Spellable extends EntityComponent {
    public static var ALL : Array<Spellable> = [];

	public var linkDisplay : h2d.Object;
	var isMarked = false;
	public var linkedTo(default, null) : Option<Spellable> = None;

	public function new (me : Entity) {
		super(me);

		me.proto.details = function () {
			switch (linkedTo) {
				case None:
				case Some (e):
					game.repl.log(me.getLocatedName() + " is linked to " + e.me.getLocatedName() + ".");
			}

			if (game.marked.contains(this)) {
				game.repl.log(me.getLocatedName() + " is marked.");
			}
		}

		linkDisplay = new h2d.Object(me.spr);

		ALL.push(this);
	}

	override function dispose() {
		super.dispose();

		if (isMarked)
			game.unmark(this);

		linkDisplay.removeChildren();

		unlink();

		ALL.remove(this);
	}

	public function mark() {
		isMarked = true;
		me.mark();
	}

	public function unmark() {
		isMarked = true;
		me.unmark();
	}

	public function link(e : Spellable) {
		unlink();
		e.unlink();

		if (e != this) {
			this.linkedTo = Some (e);
			e.linkedTo = Some (this);
		} else
			this.linkedTo = None;
	}

	public function unlink() {
		switch (linkedTo) {
			case None:
			case Some (l): l.linkedTo = None;
		}

		linkedTo = None;
	}

    override public function postUpdate() {
		updateLink();
	}

	private function updateLink() {
		linkDisplay.removeChildren();

		switch (linkedTo) {
			case None:
			case Some (l):
				var color = 0xa85959;
				var g = new h2d.Graphics(linkDisplay);
				g.beginFill(color, 0.);
				g.lineStyle(2, color);
				g.drawCircle(0, 0, Const.GRID/2);
				g.endFill();

				var d = M.dist(l.centerX, l.centerY, centerX, centerY);
				var dx = (l.centerX - centerX) / d;
				var dy = (l.centerY - centerY) / d;

				g.lineStyle(2, color);
				g.moveTo(Const.GRID/2 * dx, Const.GRID/2 * dy);
				g.lineTo(Const.GRID * dx, Const.GRID * dy);
		}
	}
}
