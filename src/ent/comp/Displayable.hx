/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2022 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent.comp;

enum Kind {
	Static;
	Attached;
}

class Displayable extends EntityComponent {
	var display : Null<h2d.Object>;
	var kind : Kind;

	var cnt = 0;

	public function new (e : Entity) {
		super(e);
	}

	override function dispose() {
		super.dispose();

		if (display != null)
			display.remove();
	}

	override function update() {
		super.update();

		if (display != null && kind == Attached) {
			display.x = me.headX;
			display.y = me.headY;
		}
	}

	public function show(s, ?k = Attached) {
		clearDisplay();
		display = new h2d.Object();
		Game.ME.scroller.add(display, Const.DP_UI);
		var tf = new h2d.Text(Settings.gameFont, display);
		tf.text = s;
		tf.x = -Std.int( tf.textWidth );
		tf.setScale(2);

		display.x = me.centerX;
		display.y = me.centerY;
	}

	public function showForS(msg, s, ?k = Attached) {
		show(msg, k);
		cd.setS("" + cnt, s, true, clearDisplay);
		cnt++;
	}

	private function clearDisplay() {
		if (display != null)
			display.remove();

		cd.unset("" + (cnt-1));
	}
}
