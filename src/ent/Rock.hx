/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ent;
import haxe.ds.Option;

class Rock extends Entity {

	var agent : ent.comp.Agent;
	var spellable : ent.comp.Spellable;
	var elementable : ent.comp.Elementable;
	var collidable : ent.comp.Collidable;
	var repulsor : ent.comp.Repulsor;

	var isFalling : Bool;

	public function new (x, y) {
		super(x, y, "Boulder");

		agent = new ent.comp.Agent(this);
		spellable = new ent.comp.Spellable(this);
		elementable = new ent.comp.Elementable(spellable);
		collidable = new ent.comp.Collidable(this, Solid, false, true);
		repulsor = new ent.comp.Repulsor(collidable, 1);

		isFalling = false;

		spr.anim.registerStateAnim("Stone", 1, () -> !isFalling);
		spr.anim.registerStateAnim("FallingRock", 1, 0.20, () -> isFalling);

		systems.Kinetic.ME.addEntity(collidable);
	}


	override function update() {
		super.update();
	}


	override function postUpdate() {
		super.postUpdate();


		if (oldCx != cx || oldCy != cy) {
			game.repl.log(getLocatedName());
		}

		if (elementable.state == Fire)
			elementable.coolDown();

		if (isDead && !cd.has("dead"))
			destroy();

		if (!isDead && (level.hasLava(cx, cy) || level.hasWater(cx, cy))) {
			xr = 0.5;
			yr = 0.5;
			isFalling = true;

			var colorAdjust = 0x201930;
			var colorDrop = 0x79acb9;

			if (level.hasLava(cx, cy)) {
				game.repl.log(getLocatedName() + " fell in Lava");
				colorAdjust = 0x5f324d;
				colorDrop = 0xaf7f66;
			} else {
				game.repl.log(getLocatedName() + " fell in Water");
			}

			fx.plouf(cx, cy, colorDrop);
			ChucK.broadcastEvent("f_plouf");
			spr.adjustColor({ gain : { color : colorAdjust, alpha : 0.5 } });
			makeDead();
		}

		var b = level.getTriggerable(cx, cy);
		if (!isDead && level.hasHole(cx, cy) && (b == null || !b.isActive)) {
			game.repl.log(getLocatedName() + " fell in Hole");
			xr = 0.5;
			yr = 0.5;
			isFalling = true;
			ChucK.broadcastEvent("f_fall");
			makeDead();
		}
	}

	override function dispose() {
		super.dispose();

		systems.Kinetic.ME.removeEntity(collidable);

		level.removeCollision(this.cx, this.cy);
		agent.destroy();
		spellable.destroy();
		collidable.destroy();
		elementable.destroy();
		repulsor.destroy();
	}

	private function makeDead() {
			isDead = true;
			cd.setS("dead", 0.5);
	}

	override public function getElementable() {
		return Some (elementable);
	}
}
