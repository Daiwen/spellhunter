/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import tools.Action;
import hxd.Key;

class CustomControl extends tools.CustomControl {
	public function getActionName(a : Action) {
		return
			switch a {
				case Up : "Up";
				case Down : "Down";
				case Left : "Left";
				case Right : "Right";
				case Select : "Select";
				case Exit : "Exit";
				case Interact : "Interact";
				case Slot1 : "Spell 1";
				case Slot2 : "Spell 2";
				case Slot3 : "Spell 3";
				case Slot4 : "Spell 4";
				case Map : "Map";
				case AimUp : "Aim Up";
				case AimDown : "Aim Down";
				case AimLeft : "Aim Left";
				case AimRight : "Aim Right";
				case SelectAim : "Select aim";
			}
	}

	public function getSuspendDuration() {
		return Settings.inputSuspend;
	}

	static public function init() {
		tools.CustomControl.init();
		tools.CustomControl.addBinding(Interact, { modifiers : new Array(), key : tools.CustomControl.key(Key.E), protected : false });

		tools.CustomControl.addBinding(Slot1, { modifiers : new Array(), key : tools.CustomControl.key(Key.NUMBER_1), protected : false });
		tools.CustomControl.addBinding(Slot1, { modifiers : new Array(), key : tools.CustomControl.key(Key.MOUSE_LEFT), protected : false });

		tools.CustomControl.addBinding(Slot2, { modifiers : new Array(), key : tools.CustomControl.key(Key.NUMBER_2), protected : false });
		tools.CustomControl.addBinding(Slot2, { modifiers : new Array(), key : tools.CustomControl.key(Key.MOUSE_RIGHT), protected : false });

		tools.CustomControl.addBinding(Slot3, { modifiers : new Array(), key : tools.CustomControl.key(Key.NUMBER_3), protected : false });
		tools.CustomControl.addBinding(Slot3, { modifiers : new Array(), key : tools.CustomControl.key(Key.MOUSE_MIDDLE), protected : false });

		tools.CustomControl.addBinding(Slot4, { modifiers : new Array(), key : tools.CustomControl.key(Key.NUMBER_4), protected : false });

		tools.CustomControl.addBinding(Map, { modifiers : new Array(), key : tools.CustomControl.key(Key.M), protected : false });

		tools.CustomControl.addBinding(AimUp, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.Z), protected : false });
		tools.CustomControl.addBinding(AimUp, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.W), protected : false });
		tools.CustomControl.addBinding(AimUp, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.UP), protected : false });

		tools.CustomControl.addBinding(AimDown, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.S), protected : false });
		tools.CustomControl.addBinding(AimDown, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.DOWN), protected : false });

		tools.CustomControl.addBinding(AimLeft, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.A), protected : false });
		tools.CustomControl.addBinding(AimLeft, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.Q), protected : false });
		tools.CustomControl.addBinding(AimLeft, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.LEFT), protected : false });

		tools.CustomControl.addBinding(AimRight, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.D), protected : false });
		tools.CustomControl.addBinding(AimRight, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.RIGHT), protected : false });

		tools.CustomControl.addBinding(SelectAim, { modifiers : [tools.CustomControl.key(Key.CTRL)], key : tools.CustomControl.key(Key.ENTER), protected : false });
	}
}
