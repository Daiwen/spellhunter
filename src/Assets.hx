/*
    Spellhunter, a create your own spell puzzle game.
    Copyright (C) 2021-2024 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import dn.heaps.slib.*;

class Assets {
	public static var noiseTexture : h3d.mat.Texture;

	public static var fontPixel : h2d.Font;
	public static var fontTiny : h2d.Font;
	public static var fontSmall : h2d.Font;
	public static var fontMedium : h2d.Font;
	public static var fontLarge : h2d.Font;
	public static var tiles : SpriteLib;
	public static var normals : SpriteLib;
	public static var sprites : SpriteLib;
	public static var hero : SpriteLib;
	public static var spell : SpriteLib;
	public static var lang : SpriteLib;
	public static var map : SpriteLib;
	public static var buttons : SpriteLib;

	public static var tilesDict = dn.heaps.assets.Aseprite.getDict(hxd.Res.world.ntileset);
	public static var spritesDict = dn.heaps.assets.Aseprite.getDict(hxd.Res.atlas.sprites);
	public static var langDict = dn.heaps.assets.Aseprite.getDict(hxd.Res.atlas.lang);
	public static var mapDict = dn.heaps.assets.Aseprite.getDict(hxd.Res.atlas.map);
	public static var buttonsDict = dn.heaps.assets.Aseprite.getDict(hxd.Res.atlas.buttons);

	static var initDone = false;
	public static function init() {
		if( initDone )
			return;
		initDone = true;

		fontPixel = hxd.Res.fonts.minecraftiaOutline.toFont();
		fontTiny = hxd.Res.fonts.barlow_condensed_medium_regular_9.toFont();
		fontSmall = hxd.Res.fonts.barlow_condensed_medium_regular_11.toFont();
		fontMedium = hxd.Res.fonts.barlow_condensed_medium_regular_17.toFont();
		fontLarge = hxd.Res.fonts.barlow_condensed_medium_regular_32.toFont();

		tiles = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.world.ntileset.toAseprite());
		normals = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.world.normals.toAseprite());
		sprites = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.atlas.sprites.toAseprite());
		hero = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.atlas.hero.toAseprite());
		spell = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.atlas.spell.toAseprite());
		lang = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.atlas.lang.toAseprite());
		map = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.atlas.map.toAseprite());
		buttons = dn.heaps.assets.Aseprite.convertToSLib(Const.FPS, hxd.Res.atlas.buttons.toAseprite());

		sprites.defineAnim("NewSpell", "0-3");
		sprites.defineAnim("NewSlot", "0-3");
		sprites.defineAnim("FallingRock", "1-4, 5(9999)");
		sprites.defineAnim("HorizontalDoor", "0-6");
		sprites.defineAnim("HorizontalDoorOpened", "0");
		sprites.defineAnim("HorizontalDoorClosed", "0");
		sprites.defineAnim("VerticalDoor", "1-7");
		sprites.defineAnim("VerticalDoorOpened", "0");
		sprites.defineAnim("VerticalDoorClosed", "0");
		sprites.defineAnim("Stone", "0");

		noiseTexture = hxd.Res.noise2d.toTile().getTexture();
		noiseTexture.wrap = h3d.mat.Data.Wrap.Repeat;
		noiseTexture.filter = Nearest;
	}

	public static function getVolume( f ) {
		return ChucK.getFloat("volume", f);
	}

	public static function setVolume(v) {
		ChucK.setFloat("volume", v);
		ChucK.broadcastEvent("volumeChange");
	}

	public static function getMusicVolume( f ) {
		return ChucK.getFloat("mVolume", f);
	}

	public static function setMusicVolume(v) {
		ChucK.setFloat("mVolume", v);
		ChucK.broadcastEvent("volumeChange");
	}

	public static function getSoundEffectsVolume( f ) {
		return ChucK.getFloat("fVolume", f);
	}

	public static function setSoundEffectsVolume(v) {
		ChucK.setFloat("fVolume", v);
		ChucK.broadcastEvent("volumeChange");
	}

	public static function toggleMusicPause() {
	}
}
